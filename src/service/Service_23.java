package service;

import dao.Dao_23;
import java.sql.SQLException;
import java.util.List;
import model.Sach;

public class Service_23 {

    Dao_23 sachDao = null;

    public Service_23() {
        sachDao = new Dao_23();
    }

    public List<Sach> getDSSach() {
        return sachDao.getDSSach();
    }

    public List<Sach> searchSach(String tenSach) throws SQLException {
        return sachDao.searchSach(tenSach);
    }

    public String getTacGiaByMaTacGia(int mtg) {
        return sachDao.getTacGiaByMaTacGia(mtg);
    }

    public String getTtheLoaiByMaTheLoai(int mtl) {
        return sachDao.getTtheLoaiByMaTheLoai(mtl);
    }

    public String getNXBByMaNXB(int mnxb) {
        return sachDao.getNXBByMaNXB(mnxb);
    }

    public List<String> getAllNXB() {
        return sachDao.getAllNXB();
    }

    public List<Sach> getTheLoaiSach(String type) {
        return sachDao.getTheLoaiSach(type);
    }

    public int addBook(Sach sach) {
        return sachDao.addBook(sach);
    }

    public int updateBook(Sach sach) {
        return sachDao.updateBook(sach);
    }

    public int deleteBook(int id) {
        return sachDao.deleteBook(id);
    }

    public int getMaTacGia(String tacGia) {
        return sachDao.getTacGia(tacGia);
    }

    public int getMaTheLoai(String theLoai) {
        return sachDao.getTheLoai(theLoai);
    }

    public int getMaNXB(String NXB) {
        return sachDao.getMaNXB(NXB);
    }

    public int getMaTrangThai(String trangThai) {
        return sachDao.getMaTrangThai(trangThai);
    }
}
