package service;

import dao.Dao_16;
import java.util.List;
import model.ChiTietDonHang;
import model.DonHang;
import model.Sach;

public class Service_16 {

    Dao_16 dao16 = null;

    public Service_16() {
        dao16 = new Dao_16();
    }

    public List<String> getAllTheLoai() {
        return dao16.getAllTheLoai();
    }

    public String getTrangThaiByMaTrangThai(int mtt) {
        return dao16.getTrangThaiByMaTrangThai(mtt);
    }

    public List<String> getAllTrangThai() {
        return dao16.getAllTrangThai();
    }

    public List<String> getAllTacGia() {
        return dao16.getAllTacGia();
    }

    public List<DonHang> getAllDonHang() {
        return dao16.getAllDonHang();
    }

    public String getTenNhanVien(int maNV) {
        return dao16.getTenNhanVien(maNV);
    }

    public List<ChiTietDonHang> getChiTietDonHang(int maDH) {
        return dao16.getChiTietDonHang(maDH);
    }

    public Double getTienCocByID(int idSach) {
        return dao16.getTienCocByID(idSach);
    }

    public Double getTienMuonByID(int idSach) {
        return dao16.getTienMuonByID(idSach);
    }

    public List<Sach> filterSach(String tThai, String tGia, String tLoai) {
        return dao16.filterSach(tThai, tGia, tLoai);
    }

    public int checkThe(String maThe) {
        return dao16.checkThe(maThe);
    }

    public int addDonHang(DonHang donHang) {
        return dao16.addDonHang(donHang);
    }

    public int addChiTietDonHang(ChiTietDonHang ctdh) {
        return dao16.addChiTietDonHang(ctdh);
    }

    public int checkChiTietDonHang() {
        return dao16.checkChiTietDonHang();
    }

    public int updateTrangThaiToDaThanhToan(ChiTietDonHang c) {
        return dao16.updateTrangThaiToDaThanhToan(c);
    }

    public List<String> getAllYearDonHanng() {
        return dao16.getAllYearDonHanng();
    }

    public List<String> getMonthDonHangByYear(String year) {
        return dao16.getMonthDonHangByYear(year);
    }

    public List<DonHang> filterDonHang(String trangThaiDH, String nam, String thang) {
        return dao16.filterDonHang(trangThaiDH, nam, thang);
    }
    
    public List<Sach> getSachMuonNhieu() {
        return dao16.getSachMuonNhieu();
    }
    
    public List<DonHang> searchDonHang(String text) {
        return dao16.searchDonHang(text);
    }
}
