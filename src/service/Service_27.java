package service;

import dao.Dao_27;
import java.sql.SQLException;
import java.util.List;
import model.NguoiDung;

public class Service_27 {

    private Dao_27 dao;

    public Service_27() {
        dao = new Dao_27();
    }

    public List<NguoiDung> getUserByTitle(String tenNguoiDung) throws SQLException {
        return dao.getUserByTitle(tenNguoiDung);
    }

    public int changepass(int maND16, String passnew16) {
        return dao.changepass(maND16, passnew16);
    }

    public List<NguoiDung> getAllND() {
        return dao.getAllND();
    }

    public String editString(String s) {
        String[] str = s.toLowerCase().replaceAll("\\s\\s+", " ").trim().split(" ");
        s = "";
        for (String i : str) {
            s = s + " " + i.substring(0, 1).toUpperCase() + i.substring(1, i.length());
        }
        return s.trim();
    }

    public int addUser(NguoiDung nguoidung) {
        return dao.addUser(nguoidung);
    }

    public int updateUser(NguoiDung nguoidung) {
        return dao.updateUser(nguoidung);
    }

    public int deleteUser(int ma_nd) {
        return dao.deleteUser(ma_nd);
    }
}
