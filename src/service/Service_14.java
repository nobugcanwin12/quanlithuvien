package service;

import dao.Dao_14;
import java.util.List;
import model.BieuDoTron;
import model.DoanhThu;
import model.KhachHang;
import model.TacGia;
import model.The;
import model.TheLoai;

public class Service_14 {

    private Dao_14 dao;

    public Service_14() {
        dao = new Dao_14();
    }

    public int addKH(KhachHang kh) {
        return dao.addKH(kh);
    }

    public List<KhachHang> getAllKH() {
        return dao.getAllKH();
    }

    public int deleteKH(int maKH) {
        return dao.deleteKH(maKH);
    }

    public int editKH(KhachHang kh) {
        return dao.editKH(kh);
    }

    public List<KhachHang> getKhSdt(String sdt) {
        return dao.getKhSdt(sdt);
    }

    //Sử lý in hoa và xóa khoảng trắng của 1 chuỗi
    public String editString(String s) {
        String[] str = s.toLowerCase().replaceAll("\\s\\s+", " ").trim().split(" ");
        s = "";
        for (String i : str) {
            s = s + " " + i.substring(0, 1).toUpperCase() + i.substring(1, i.length());
        }
        return s.trim();
    }

    public int addThe(The the) {
        return dao.addThe(the);
    }

    public List<The> getAllThe() {       
        return dao.getAllThe();
    }

    public int khoaThe(String maThe) {
        return dao.khoaThe(maThe);
    }

    public int moThe(String maThe) {
        return dao.moThe(maThe);
    }

    public String getTenByMa(int maThe) {
        return dao.getTenByMa(maThe);
    }

    public List<The> getTheByTen(String ten) {
        return dao.getTheByTen(ten);
    }

    public int addTacGia(String tenTacGia) {
        return dao.addTacGia(tenTacGia);
    }

    public List<TacGia> getAllTacGia() {
        return dao.getAllTacGia();
    }

    public int editTacGia(TacGia tg) {
        return dao.editTacGia(tg);
    }

    public List<TacGia> timKiemTacGia(String tk) {
        return dao.timKiemTacGia(tk);
    }

    public List<TheLoai> getAllTheLoai() {
        return dao.getAllTheLoai();
    }

    public List<TheLoai> timKiemTheLoai(String tk) {
        return dao.timKiemTheLoai(tk);
    }

    public int addTheLoai(String tenTheLoai) {
        return dao.addTheLoai(tenTheLoai);
    }

    public int editTheLoai(TheLoai tl) {
        return dao.editTheLoai(tl);
    }

    public int setTrangThaiHetHan() {
        return dao.setTrangThaiHetHan();
    }

    public List<DoanhThu> doanhThu() {
        return dao.doanhThu();
    }

    public List<BieuDoTron> bieuDoTron() {
        return dao.BieuDotron();
    }
    
    public void inHoaDon(int maDH) {
        dao.inHoaDon(maDH);
    }
}
