package view.tacGia;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.Sach;
import model.TacGia;
import service.Service_14;
import service.Service_16;
import service.Service_23;

public class TacGiaPN extends javax.swing.JPanel {

    DefaultTableModel qlTacGiaModel = null;
    Sach sach;
    Service_23 TGService_23 = null;
    Service_16 TGService_16 = null;
    Service_14 service_14 = null;
    List<TacGia> tacGiaS;

    public TacGiaPN() {
        TGService_23 = new Service_23();
        TGService_16 = new Service_16();
        service_14 = new Service_14();
        initComponents();
        qlTacGiaModel = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblQLTacGia.setModel(qlTacGiaModel);
        //them cot cho TacGiaTB
        qlTacGiaModel.addColumn("Mã tác giả");
        qlTacGiaModel.addColumn("Tên Tác giả");

        //them du lieu vao bookTB
        tacGiaS = service_14.getAllTacGia();
        setDataTblQLTacGia(tacGiaS);
        widthColumnTB_Muon();
        tblQLTacGia.setComponentPopupMenu(tbPopupMenu_14);
    }

    public void setDataTblQLTacGia(List<TacGia> tacGiaS) {
        qlTacGiaModel.setRowCount(0);
        for (TacGia tg : tacGiaS) {
            qlTacGiaModel.addRow(new Object[]{tg.getMaTacGia(), tg.getTenTacGia()});
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    public void widthColumnTB_Muon() {
        TableColumnModel columnModel = tblQLTacGia.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(200);
        columnModel.getColumn(0).setMaxWidth(200);
        columnModel.getColumn(1).setPreferredWidth(500);
        columnModel.getColumn(1).setMaxWidth(500);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbPopupMenu_14 = new javax.swing.JPopupMenu();
        editMenuItem_14 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblQLTacGia = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtIdTG = new javax.swing.JTextField();
        txtTenTG = new javax.swing.JTextField();
        btnSaveTG = new javax.swing.JButton();
        btnAddTG = new javax.swing.JButton();
        btnEditTG = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        timKiem = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        editMenuItem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        editMenuItem_14.setText("Sửa");
        editMenuItem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMenuItem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu_14.add(editMenuItem_14);

        setPreferredSize(new java.awt.Dimension(1225, 720));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel1.setText("Mã tác giả:");

        tblQLTacGia.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tblQLTacGia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblQLTacGia.setRowHeight(30);
        tblQLTacGia.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tblQLTacGia.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tblQLTacGia);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel2.setText("Tên tác giả:");

        txtIdTG.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtIdTG.setEnabled(false);

        txtTenTG.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        btnSaveTG.setBackground(new java.awt.Color(236, 139, 94));
        btnSaveTG.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnSaveTG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        btnSaveTG.setText("Lưu");
        btnSaveTG.setEnabled(false);
        btnSaveTG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveTGActionPerformed(evt);
            }
        });

        btnAddTG.setBackground(new java.awt.Color(138, 170, 229));
        btnAddTG.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnAddTG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/add_48px.png"))); // NOI18N
        btnAddTG.setText("Thêm");
        btnAddTG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTGActionPerformed(evt);
            }
        });

        btnEditTG.setBackground(new java.awt.Color(239, 246, 124));
        btnEditTG.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnEditTG.setForeground(new java.awt.Color(41, 95, 45));
        btnEditTG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        btnEditTG.setText("Sửa");
        btnEditTG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditTGActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 153, 102));
        jLabel3.setText("THÔNG TIN TÁC GIẢ");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/author.jpg"))); // NOI18N
        jLabel4.setToolTipText("");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/search-b-icon.png"))); // NOI18N
        jLabel5.setText("Tìm kiếm :");
        jLabel5.setToolTipText("");

        timKiem.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                timKiemPropertyChange(evt);
            }
        });
        timKiem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timKiemKeyPressed(evt);
            }
        });

        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(41, 40, 38));
        btnCancel.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(249, 211, 66));
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/exit.png"))); // NOI18N
        btnCancel.setText("Hủy");
        btnCancel.setEnabled(false);
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(64, 64, 64)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel1))
                            .addGap(30, 30, 30)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtIdTG, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTenTG, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(79, 79, 79)
                            .addComponent(jLabel4))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnCancel)
                            .addGap(18, 18, 18)
                            .addComponent(btnAddTG)
                            .addGap(18, 18, 18)
                            .addComponent(btnEditTG)
                            .addGap(18, 18, 18)
                            .addComponent(btnSaveTG)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(timKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(btnSearch)))
                .addGap(63, 63, 63))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(66, 66, 66)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(69, 69, 69)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(timKiem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(50, 50, 50))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(42, 42, 42)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdTG, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTenTG, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSaveTG, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddTG, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEditTG, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(20, 20, 20))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveTGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveTGActionPerformed
        if (txtIdTG.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Chọn tác giả sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (txtTenTG.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Nhập tên tác giả", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            TacGia tg = new TacGia(Integer.parseInt(txtIdTG.getText()), txtTenTG.getText());
            int check = service_14.editTacGia(tg);
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Sửa thất bại", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Sửa thành công");
                txtIdTG.setText("");
                txtTenTG.setText("");

                btnSaveTG.setEnabled(false);

                tacGiaS = service_14.getAllTacGia();
                setDataTblQLTacGia(tacGiaS);
            }
        }

    }//GEN-LAST:event_btnSaveTGActionPerformed

    private void btnEditTGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditTGActionPerformed
        int row = tblQLTacGia.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Chọn tác giả sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            txtIdTG.setText(tblQLTacGia.getValueAt(row, 0).toString());
            txtTenTG.setText(tblQLTacGia.getValueAt(row, 1).toString());

            btnSaveTG.setEnabled(true);
            btnCancel.setEnabled(true);

            btnAddTG.setEnabled(false);
            btnEditTG.setEnabled(false);

        }
    }//GEN-LAST:event_btnEditTGActionPerformed

    private void btnAddTGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTGActionPerformed
        // TODO add your handling code here:
        if (txtTenTG.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập tác giả sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int check = service_14.addTacGia(txtTenTG.getText());
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Thêm không thành công", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Thêm thành công");
                tacGiaS = service_14.getAllTacGia();
                setDataTblQLTacGia(tacGiaS);
            }
        }
    }//GEN-LAST:event_btnAddTGActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        if (timKiem.getText().equals("")) {
            tacGiaS = service_14.getAllTacGia();
            setDataTblQLTacGia(tacGiaS);
        } else {
            tacGiaS = service_14.timKiemTacGia(timKiem.getText());
            setDataTblQLTacGia(tacGiaS);
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void timKiemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timKiemKeyPressed
        // TODO add your handling code here:
        if (timKiem.getText().equals("")) {
            tacGiaS = service_14.getAllTacGia();
            setDataTblQLTacGia(tacGiaS);
        } else {
            tacGiaS = service_14.timKiemTacGia(timKiem.getText());
            setDataTblQLTacGia(tacGiaS);
        }
    }//GEN-LAST:event_timKiemKeyPressed

    private void timKiemPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_timKiemPropertyChange
        // TODO add your handling code here:
        if (timKiem.getText().equals("")) {
            tacGiaS = service_14.getAllTacGia();
            setDataTblQLTacGia(tacGiaS);
        } else {
            tacGiaS = service_14.timKiemTacGia(timKiem.getText());
            setDataTblQLTacGia(tacGiaS);
        }
    }//GEN-LAST:event_timKiemPropertyChange

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        txtIdTG.setText("");
        txtTenTG.setText("");

        btnSaveTG.setEnabled(false);
        btnCancel.setEnabled(false);

        btnAddTG.setEnabled(true);
        btnEditTG.setEnabled(true);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void editMenuItem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editMenuItem_14ActionPerformed
        // TODO add your handling code here:
        int row = tblQLTacGia.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Chọn tác giả sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            txtIdTG.setText(tblQLTacGia.getValueAt(row, 0).toString());
            txtTenTG.setText(tblQLTacGia.getValueAt(row, 1).toString());

            btnSaveTG.setEnabled(true);
            btnCancel.setEnabled(true);

            btnAddTG.setEnabled(false);
            btnEditTG.setEnabled(false);

        }
    }//GEN-LAST:event_editMenuItem_14ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddTG;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEditTG;
    private javax.swing.JButton btnSaveTG;
    private javax.swing.JButton btnSearch;
    private javax.swing.JMenuItem editMenuItem_14;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu tbPopupMenu_14;
    private javax.swing.JTable tblQLTacGia;
    private javax.swing.JTextField timKiem;
    private javax.swing.JTextField txtIdTG;
    private javax.swing.JTextField txtTenTG;
    // End of variables declaration//GEN-END:variables
}
