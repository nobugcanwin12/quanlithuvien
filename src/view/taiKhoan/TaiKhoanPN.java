package view.taiKhoan;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import view.Login;
import model.NguoiDung;
import service.*;

public class TaiKhoanPN extends javax.swing.JPanel {

    private Service_27 nguoidungService_27;
    private NguoiDung dao_27;
    private String nd;
    private NguoiDung nguoiDung16 = null;
    JFrame home;

    public TaiKhoanPN(JFrame home, NguoiDung nguoiDung16) {

        this.home = home;
        this.nguoiDung16 = nguoiDung16;
        nguoidungService_27 = new Service_27();

        initComponents();

        this.hidepass1_327.setVisible(false);
        this.hidepass2_327.setVisible(false);
        this.hidepass3_327.setVisible(false);

        //set ten nguoi dung
        tenNDLB16.setText("XIN CHÀO " + nguoiDung16.getTenND().toUpperCase());
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tenNDLB16 = new javax.swing.JLabel();
        hidepass2_327 = new javax.swing.JLabel();
        showpass3_327 = new javax.swing.JLabel();
        doipass_327 = new javax.swing.JButton();
        logout_327 = new javax.swing.JButton();
        changenewpass_txt = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        newpass_txt = new javax.swing.JPasswordField();
        oldpass_txt = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        hidepass3_327 = new javax.swing.JLabel();
        showpass2_327 = new javax.swing.JLabel();
        showpass1_327 = new javax.swing.JLabel();
        hidepass1_327 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        tenNDLB16.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        tenNDLB16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tenNDLB16.setText("XIN CHÀO ");

        hidepass2_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_hide_32.png"))); // NOI18N
        hidepass2_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                hidepass2_327MousePressed(evt);
            }
        });

        showpass3_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_eye_32.png"))); // NOI18N
        showpass3_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                showpass3_327MousePressed(evt);
            }
        });

        doipass_327.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        doipass_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_good_pincode_32.png"))); // NOI18N
        doipass_327.setText("Đổi mật khẩu");
        doipass_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doipass_327ActionPerformed(evt);
            }
        });

        logout_327.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        logout_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/choice.png"))); // NOI18N
        logout_327.setText("Đăng xuất");
        logout_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logout_327ActionPerformed(evt);
            }
        });

        changenewpass_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Nhập mật khẩu mới: ");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nhập mật khẩu cũ: ");

        newpass_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        oldpass_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Xác nhận mật khẩu mới: ");

        hidepass3_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_hide_32.png"))); // NOI18N
        hidepass3_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                hidepass3_327MousePressed(evt);
            }
        });

        showpass2_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_eye_32.png"))); // NOI18N
        showpass2_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                showpass2_327MousePressed(evt);
            }
        });

        showpass1_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_eye_32.png"))); // NOI18N
        showpass1_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                showpass1_327MousePressed(evt);
            }
        });

        hidepass1_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/icons8_hide_32.png"))); // NOI18N
        hidepass1_327.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                hidepass1_327MousePressed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/LIBRARY.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tenNDLB16, javax.swing.GroupLayout.DEFAULT_SIZE, 1225, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doipass_327))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(newpass_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(oldpass_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(showpass1_327)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(hidepass1_327))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(showpass2_327)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(hidepass2_327))))
                        .addComponent(logout_327, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(changenewpass_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(showpass3_327)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(hidepass3_327)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tenNDLB16, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(showpass1_327, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hidepass1_327, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(oldpass_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(newpass_txt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showpass2_327, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hidepass2_327, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(changenewpass_txt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(hidepass3_327, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(showpass3_327, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(77, 77, 77)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logout_327, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(doipass_327, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        tenNDLB16.getAccessibleContext().setAccessibleName("Account_327");
        doipass_327.getAccessibleContext().setAccessibleName("logout_327");
    }// </editor-fold>//GEN-END:initComponents

    private void logout_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logout_327ActionPerformed
        new Login().setVisible(true);
        home.dispose();
    }//GEN-LAST:event_logout_327ActionPerformed

    private void showpass1_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showpass1_327MousePressed
        hidepass1_327.setVisible(true);
        showpass1_327.setVisible(false);
        oldpass_txt.setEchoChar((char) 0);
    }//GEN-LAST:event_showpass1_327MousePressed

    private void hidepass1_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hidepass1_327MousePressed
        showpass1_327.setVisible(true);
        hidepass1_327.setVisible(false);
        oldpass_txt.setEchoChar('*');
    }//GEN-LAST:event_hidepass1_327MousePressed

    private void showpass2_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showpass2_327MousePressed
        hidepass2_327.setVisible(true);
        showpass2_327.setVisible(false);
        newpass_txt.setEchoChar((char) 0);
    }//GEN-LAST:event_showpass2_327MousePressed

    private void hidepass2_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hidepass2_327MousePressed
        showpass2_327.setVisible(true);
        hidepass2_327.setVisible(false);
        newpass_txt.setEchoChar('*');
    }//GEN-LAST:event_hidepass2_327MousePressed

    private void hidepass3_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hidepass3_327MousePressed
        showpass3_327.setVisible(true);
        hidepass3_327.setVisible(false);
        changenewpass_txt.setEchoChar('*');
    }//GEN-LAST:event_hidepass3_327MousePressed

    private void showpass3_327MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showpass3_327MousePressed
        hidepass3_327.setVisible(true);
        showpass3_327.setVisible(false);
        changenewpass_txt.setEchoChar((char) 0);
        // String pass = nguoidungService_27.getMatKhau();
    }//GEN-LAST:event_showpass3_327MousePressed

    private void doipass_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doipass_327ActionPerformed
        String oldpass = new String(oldpass_txt.getPassword());
        String newpass = new String(newpass_txt.getPassword());
        String changenewpass = new String(changenewpass_txt.getPassword());

        String pass = nguoiDung16.getMatKhau();

        if (oldpass.length() == 0 || newpass.length() == 0 || changenewpass.length() == 0) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập đầy đủ", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (!pass.equals(oldpass)) {
            JOptionPane.showMessageDialog(this, "Mật khẩu cũ không chính xác", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (!newpass.equals(changenewpass)) {
            JOptionPane.showMessageDialog(this, "Xác nhận mật khẩu không chính xác", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int check = nguoidungService_27.changepass(nguoiDung16.getMaND(), newpass);
            if (check == 1) {
                JOptionPane.showConfirmDialog(this, "Thay đổi mật khẩu thành công", "Thông báo", JOptionPane.CLOSED_OPTION);
                oldpass_txt.setText("");
                newpass_txt.setText("");
                changenewpass_txt.setText("");
            } else {
                JOptionPane.showConfirmDialog(this, "Có lỗi trong lúc thay đổi, vui lòng thử lại", "Thông báo", JOptionPane.CLOSED_OPTION);
            }
        }
    }//GEN-LAST:event_doipass_327ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField changenewpass_txt;
    private javax.swing.JButton doipass_327;
    private javax.swing.JLabel hidepass1_327;
    private javax.swing.JLabel hidepass2_327;
    private javax.swing.JLabel hidepass3_327;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton logout_327;
    private javax.swing.JPasswordField newpass_txt;
    private javax.swing.JPasswordField oldpass_txt;
    private javax.swing.JLabel showpass1_327;
    private javax.swing.JLabel showpass2_327;
    private javax.swing.JLabel showpass3_327;
    private javax.swing.JLabel tenNDLB16;
    // End of variables declaration//GEN-END:variables

}
