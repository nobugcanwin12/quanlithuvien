package view.muonSach;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.*;
import service.*;

public class MuonSachPN extends javax.swing.JPanel {

    DefaultTableModel dsSachTVModel = null;
    Service_23 sachService_23 = null;
    Service_16 service16 = null;
    Service_14 service14 = null;
    List<Sach> sach = null;
    List<SachMuonPN> listSachMuon = null;
    NguoiDung nv = null;
    Double tienCoc = 0.0, tienMuon = 0.0;

    public MuonSachPN(NguoiDung nv) {

        sachService_23 = new Service_23();
        service16 = new Service_16();
        service14 = new Service_14();
        this.nv = nv;
        sach = new ArrayList<Sach>();
        listSachMuon = new ArrayList<SachMuonPN>();

        initComponents();

        //sach co trong thu vien
        dsSachTVModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblSachTV.setModel(dsSachTVModel);
        //them cot cho bookTB
        dsSachTVModel.addColumn("Mã sách");
        dsSachTVModel.addColumn("Tên sách");
        dsSachTVModel.addColumn("Trạng thái");
        //them du lieu vao bookTB
        addRowShowSach(sachService_23.getDSSach());

        //load bo loc
        loadmaTheLoai();
        loadTrangThai();
        loadTacGia();

        //set do rong cho cac cot
        widthColumnTB();

        //them panel title vao sach muon
        dsSachMuonPanel.add(new TitleMuonSachPN());
        revalidate();
        repaint();
    }

    final void addRowShowSach(List<Sach> sachs) {
        dsSachTVModel.setRowCount(0);
        for (Sach sach : sachs) {
            String trangThai = service16.getTrangThaiByMaTrangThai(sach.getTrangThai());
            dsSachTVModel.addRow(new Object[]{sach.getId(), sach.getTen(), trangThai});
        }
    }

    public void loadmaTheLoai() {
        List<String> theLoais = service16.getAllTheLoai();
        for (String theLoai : theLoais) {
            cbbTheLoai.addItem(theLoai);
        }
    }

    public void loadTrangThai() {
        List<String> trangThais = service16.getAllTrangThai();
        for (String trangThai : trangThais) {
            cbbTrangThai.addItem(trangThai);
        }
    }

    public void loadTacGia() {
        List<String> tacGias = service16.getAllTacGia();
        for (String tacGia : tacGias) {
            cbbTacGia.addItem(tacGia);
        }
    }

    public void widthColumnTB() {
        TableColumnModel columnModel = tblSachTV.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(70);
        columnModel.getColumn(0).setMaxWidth(70);
        columnModel.getColumn(1).setPreferredWidth(280);
        columnModel.getColumn(1).setMaxWidth(280);
        columnModel.getColumn(2).setPreferredWidth(140);
        columnModel.getColumn(2).setMaxWidth(140);
    }

    public void setDSSachMuon() {
        xoaSachMuonCBB.removeAllItems();
        xoaSachMuonCBB.addItem("Xóa tất cả");
        for (SachMuonPN s1 : listSachMuon) {
            xoaSachMuonCBB.addItem(String.valueOf(s1.getID()));
        }
    }

    public void setTienCocVaMuon() {
        tienCoc = 0.0;
        tienMuon = 0.0;
        for (SachMuonPN s1 : listSachMuon) {
            int id = s1.getID();
            tienCoc += s1.getTienCoc();
            tienMuon += s1.getTienMuon() * s1.getNgayMuon();
        }
        NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
        tienMuonLB.setText(nf.format(tienMuon));
        tienCocLB.setText(nf.format(tienCoc));
    }

    public void searchSach() {
        try {
            addRowShowSach(sachService_23.searchSach(txtSearch.getText()));
            cbbTrangThai.setSelectedItem("Tất cả sách");
            cbbTheLoai.setSelectedItem("Tất cả thể loại");
            cbbTacGia.setSelectedItem("Tất cả tác giả");
        } catch (SQLException ex) {
            Logger.getLogger(MuonSachPN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void themDonHang() {
        //them don hang
        DonHang dh = new DonHang();
        dh.setMaNV(nv.getMaND());
        dh.setSoThe(theTF.getText().toUpperCase());
        int maDH = service16.addDonHang(dh);

        if (maDH != 0) {
            //them chi tiet don hang
            int soSachDaThem = 0;
            for (SachMuonPN sachMuon : listSachMuon) {
                ChiTietDonHang ctdh = new ChiTietDonHang();
                ctdh.setMaDH(maDH);
                ctdh.setIdSach(sachMuon.getID());
                ctdh.setThoiGianMuon(sachMuon.getNgayMuon());
                ctdh.setTienCoc(sachMuon.getTienCoc());
                ctdh.setTienMuon(sachMuon.getTienMuon());

                soSachDaThem += service16.addChiTietDonHang(ctdh);
            }
            if (soSachDaThem == listSachMuon.size()) {
                //set default                
                dsSachMuonPanel.removeAll();
                dsSachMuonPanel.add(new TitleMuonSachPN());
                revalidate();
                repaint();

                listSachMuon = new ArrayList<SachMuonPN>();

                addRowShowSach(sachService_23.getDSSach());

                setDSSachMuon();
                setTienCocVaMuon();
                lblGioiHan.setText(String.valueOf(listSachMuon.size()));

                theTF.setText("");
                
                //in hoa don
                int ys = JOptionPane.showConfirmDialog(this, "Thêm thàn công, Bạn có muốn in hóa đơn không?", "Hỏi đáp", JOptionPane.YES_NO_OPTION);
                if(ys == 0) {
                    service14.inHoaDon(maDH);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Có lỗi trong lúc thêm, Vui lòng kiểm tra lại", "Lỗi", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Có lỗi trong lúc thêm, Vui lòng kiểm tra lại", "Lỗi", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbbTrangThai = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        cbbTheLoai = new javax.swing.JComboBox<>();
        btnSearch2 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tblSachTV = new javax.swing.JTable();
        btnThemSachMuon = new javax.swing.JButton();
        btnThanhToan = new javax.swing.JButton();
        cbbTacGia = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        filterBT16 = new javax.swing.JButton();
        dsSachMuonPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        tienMuonLB = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        tienCocLB = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblGioiHan = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        xoaSachMuonCBB = new javax.swing.JComboBox<>();
        xoaSachMuonBT = new javax.swing.JButton();
        theTF = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(1225, 720));

        cbbTrangThai.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cbbTrangThai.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả sách" }));
        cbbTrangThai.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbTrangThaiItemStateChanged(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel10.setText("Tìm kiếm");

        txtSearch.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
        });

        cbbTheLoai.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cbbTheLoai.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả thể loại" }));
        cbbTheLoai.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbTheLoaiItemStateChanged(evt);
            }
        });

        btnSearch2.setBackground(new java.awt.Color(204, 204, 255));
        btnSearch2.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnSearch2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch2ActionPerformed(evt);
            }
        });

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Sách thư viện", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 15))); // NOI18N

        tblSachTV.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tblSachTV.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblSachTV.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tblSachTV.setRowHeight(30);
        tblSachTV.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tblSachTV.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblSachTV.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSachTVMouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(tblSachTV);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnThemSachMuon.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnThemSachMuon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/addbook-icon.png"))); // NOI18N
        btnThemSachMuon.setText("Mượn");
        btnThemSachMuon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemSachMuonActionPerformed(evt);
            }
        });

        btnThanhToan.setBackground(new java.awt.Color(204, 204, 255));
        btnThanhToan.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        btnThanhToan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Pay-icon.png"))); // NOI18N
        btnThanhToan.setText("Thanh Toán");
        btnThanhToan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThanhToanActionPerformed(evt);
            }
        });

        cbbTacGia.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cbbTacGia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả tác giả" }));
        cbbTacGia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbTacGiaItemStateChanged(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel15.setText("Lọc:");

        filterBT16.setBackground(new java.awt.Color(204, 204, 255));
        filterBT16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        filterBT16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/filter16.png"))); // NOI18N
        filterBT16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterBT16ActionPerformed(evt);
            }
        });

        dsSachMuonPanel.setBackground(new java.awt.Color(255, 255, 255));
        dsSachMuonPanel.setOpaque(false);
        dsSachMuonPanel.setLayout(new java.awt.GridLayout(0, 1, 0, 5));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        jLabel1.setText("Số thẻ:");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setOpaque(false);

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel16.setText("Tiền mượn:");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel17.setText("Tiền cọc:");

        tienMuonLB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        tienMuonLB.setForeground(new java.awt.Color(255, 51, 51));
        tienMuonLB.setText("0");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel19.setText("VNĐ");

        tienCocLB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        tienCocLB.setForeground(new java.awt.Color(255, 51, 51));
        tienCocLB.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        tienCocLB.setText("0");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel21.setText("VNĐ");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel14.setText("3");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("/");

        lblGioiHan.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblGioiHan.setForeground(new java.awt.Color(255, 51, 51));
        lblGioiHan.setText("0");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel11.setText("Số sách mượn:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(60, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel17)
                    .addComponent(jLabel16))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(tienCocLB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(tienMuonLB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel19))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblGioiHan)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel14)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblGioiHan, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienMuonLB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienCocLB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        xoaSachMuonCBB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        xoaSachMuonCBB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Xóa tất cả" }));

        xoaSachMuonBT.setBackground(new java.awt.Color(204, 204, 255));
        xoaSachMuonBT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/xoa.png"))); // NOI18N
        xoaSachMuonBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xoaSachMuonBTActionPerformed(evt);
            }
        });

        theTF.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/LIBRARY.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel15))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbbTrangThai, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cbbTacGia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(filterBT16, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSearch)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnThemSachMuon))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel2)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(theTF, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(xoaSachMuonCBB, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(xoaSachMuonBT))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 176, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnThanhToan)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(dsSachMuonPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbbTrangThai, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbbTacGia, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(filterBT16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearch2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(169, 169, 169)
                        .addComponent(btnThemSachMuon))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xoaSachMuonCBB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xoaSachMuonBT, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(theTF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(dsSachMuonPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                        .addComponent(btnThanhToan, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 27, Short.MAX_VALUE)
                        .addComponent(jLabel2)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            searchSach();
        }
    }//GEN-LAST:event_txtSearchKeyPressed

    private void cbbTheLoaiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbTheLoaiItemStateChanged

    }//GEN-LAST:event_cbbTheLoaiItemStateChanged

    private void btnSearch2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch2ActionPerformed
        searchSach();
    }//GEN-LAST:event_btnSearch2ActionPerformed

    private void tblSachTVMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSachTVMouseClicked

    }//GEN-LAST:event_tblSachTVMouseClicked

    private void btnThemSachMuonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemSachMuonActionPerformed
        if (listSachMuon.size() >= 3) {
            JOptionPane.showMessageDialog(this, "Vượt quá số sách cho phép mượn!", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int row = tblSachTV.getSelectedRow();
            if (row != -1) {
                String trangThai = tblSachTV.getValueAt(row, 2).toString();
                if (trangThai.equals("Đang rảnh")) {

                    int id = Integer.parseInt(tblSachTV.getValueAt(row, 0).toString());

                    //kiem tra xem sach da co ben sach muon hay chua
                    for (SachMuonPN s1 : listSachMuon) {
                        if (s1.getID() == id) {
                            JOptionPane.showMessageDialog(this, "Sách này bạn đã chọn rồi", "Lỗi", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }

                    //them vao list sach muon
                    SachMuonPN sachMuon = new SachMuonPN(this);
                    sachMuon.setID(id);
                    sachMuon.setTienCoc(service16.getTienCocByID(id));
                    sachMuon.setTienMuon(service16.getTienMuonByID(id));
                    listSachMuon.add(sachMuon);

                    //them vao list sach muon giao dien
                    dsSachMuonPanel.add(sachMuon);
                    revalidate();
                    repaint();

                    //đặt số lượng sách đã chọn
                    lblGioiHan.setText(String.valueOf(listSachMuon.size()));

                    //set ds sach muon de xoa
                    setDSSachMuon();

                    //hien thi tien coc va tien muon
                    setTienCocVaMuon();
                } else {
                    JOptionPane.showMessageDialog(this, "Sách này hiện tại không thể cho mượn!", "Lỗi", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Vui lòng chọn sách", "Lỗi", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnThemSachMuonActionPerformed

    private void cbbTrangThaiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbTrangThaiItemStateChanged

    }//GEN-LAST:event_cbbTrangThaiItemStateChanged

    private void cbbTacGiaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbTacGiaItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbbTacGiaItemStateChanged

    private void filterBT16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterBT16ActionPerformed
        String tt = cbbTrangThai.getSelectedItem().toString();
        String tl = cbbTheLoai.getSelectedItem().toString();
        String tg = cbbTacGia.getSelectedItem().toString();
        addRowShowSach(service16.filterSach(tt, tg, tl));
    }//GEN-LAST:event_filterBT16ActionPerformed

    private void xoaSachMuonBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xoaSachMuonBTActionPerformed
        int sachXoa = xoaSachMuonCBB.getSelectedIndex();
        if (sachXoa == 0) {
            for (int i = 1; i <= listSachMuon.size(); i++) {
                dsSachMuonPanel.remove(1);
            }
            listSachMuon = new ArrayList<SachMuonPN>();
        } else {
            dsSachMuonPanel.remove(sachXoa);
            listSachMuon.remove(sachXoa - 1);
        }
        revalidate();
        repaint();

        //đặt số lượng sách đã chọn
        lblGioiHan.setText(String.valueOf(listSachMuon.size()));

        //set ds sach muon de xoa
        setDSSachMuon();

        //hien thi tien coc va tien muon
        setTienCocVaMuon();
    }//GEN-LAST:event_xoaSachMuonBTActionPerformed

    private void btnThanhToanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThanhToanActionPerformed
        if (theTF.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập mã thẻ", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (listSachMuon.size() < 1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn sách", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int kt = service16.checkThe(theTF.getText());
            switch (kt) {
                case 0:
                    JOptionPane.showMessageDialog(this, "Mã thẻ không đúng, vui lòng nhập lại", "Lỗi", JOptionPane.ERROR_MESSAGE);
                    break;
                case 1:
                    JOptionPane.showMessageDialog(this, "Thẻ \"" + theTF.getText().toUpperCase() + "\" đang mượn sách", "Lỗi", JOptionPane.ERROR_MESSAGE);
                    break;
                case 2:
                    JOptionPane.showMessageDialog(this, "Thẻ \"" + theTF.getText().toUpperCase() + "\" đã bị khóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
                    break;
                case 3:
                    JOptionPane.showMessageDialog(this, "Thẻ \"" + theTF.getText().toUpperCase() + "\" đã hết hạn", "Lỗi", JOptionPane.ERROR_MESSAGE);
                    break;
                case 4:
                    themDonHang();
                    break;
            }
        }
    }//GEN-LAST:event_btnThanhToanActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch2;
    private javax.swing.JButton btnThanhToan;
    private javax.swing.JButton btnThemSachMuon;
    private javax.swing.JComboBox<String> cbbTacGia;
    private javax.swing.JComboBox<String> cbbTheLoai;
    private javax.swing.JComboBox<String> cbbTrangThai;
    private javax.swing.JPanel dsSachMuonPanel;
    private javax.swing.JButton filterBT16;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblGioiHan;
    private javax.swing.JTable tblSachTV;
    private javax.swing.JTextField theTF;
    private javax.swing.JLabel tienCocLB;
    private javax.swing.JLabel tienMuonLB;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JButton xoaSachMuonBT;
    private javax.swing.JComboBox<String> xoaSachMuonCBB;
    // End of variables declaration//GEN-END:variables
}
