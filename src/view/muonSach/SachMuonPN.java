package view.muonSach;

import java.text.NumberFormat;
import java.util.Locale;

public class SachMuonPN extends javax.swing.JPanel {

    private double tienCoc;
    private double tienMuon;
    MuonSachPN pn;

    public SachMuonPN(MuonSachPN pn) {

        this.pn = pn;

        initComponents();
    }

    public void setID(int id) {
        idSachLB.setText(String.valueOf(id));
    }

    public int getID() {
        return Integer.parseInt(idSachLB.getText());
    }

    public void setTienCoc(double tienCoc) {
        this.tienCoc = tienCoc;

        NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
        tienCocLB.setText(nf.format(tienCoc));
    }

    public double getTienCoc() {
        return tienCoc;
    }

    public void setTienMuon(double tienMuon) {
        this.tienMuon = tienMuon;

        NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
        tienMuonLB.setText(nf.format(tienMuon));
    }

    public double getTienMuon() {
        return tienMuon;
    }

    public int getNgayMuon() {
        return Integer.parseInt(ngayMuonCBB.getSelectedItem().toString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ngayMuonCBB = new javax.swing.JComboBox<>();
        tienCocLB = new javax.swing.JLabel();
        tienMuonLB = new javax.swing.JLabel();
        idSachLB = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setPreferredSize(new java.awt.Dimension(502, 52));

        ngayMuonCBB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        ngayMuonCBB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "7", "14", "21", "30" }));
        ngayMuonCBB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ngayMuonCBBItemStateChanged(evt);
            }
        });
        ngayMuonCBB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ngayMuonCBBActionPerformed(evt);
            }
        });

        tienCocLB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tienCocLB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tienCocLB.setText("1000");

        tienMuonLB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tienMuonLB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tienMuonLB.setText("1000");

        idSachLB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        idSachLB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        idSachLB.setText("1000");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(idSachLB, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(tienMuonLB, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(tienCocLB, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ngayMuonCBB, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ngayMuonCBB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienCocLB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienMuonLB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idSachLB, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ngayMuonCBBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ngayMuonCBBItemStateChanged
        pn.setTienCocVaMuon();
    }//GEN-LAST:event_ngayMuonCBBItemStateChanged

    private void ngayMuonCBBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ngayMuonCBBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ngayMuonCBBActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel idSachLB;
    private javax.swing.JComboBox<String> ngayMuonCBB;
    private javax.swing.JLabel tienCocLB;
    private javax.swing.JLabel tienMuonLB;
    // End of variables declaration//GEN-END:variables
}
