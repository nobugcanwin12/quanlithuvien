package view.khachHang;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.KhachHang;
import model.The;
import service.Service_14;

public class KhachHangPN extends javax.swing.JPanel {

    KhachHang kh;
    Service_14 service;
    DefaultTableModel defaultTableModel;
    int trang_14 = 1;
    int sLg_14 = 7;
    List<KhachHang> khS_14;

    public KhachHangPN() {
        initComponents();
        service = new Service_14();
        defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableKH_14.setModel(defaultTableModel);
        defaultTableModel.addColumn("Mã KH");
        defaultTableModel.addColumn("Tên KH");
        defaultTableModel.addColumn("Địa Chỉ");
        defaultTableModel.addColumn("SĐT");

        khS_14 = service.getAllKH();

        setDataTable(khS_14);
        kiemTraNut();
        tableKH_14.setComponentPopupMenu(tbPopupMenu);
    }

    private void setDataTable(List<KhachHang> khachHangs) {
        if (trang_14 != 1 && (trang_14 - 1) * sLg_14 == khS_14.size()) {
            trang_14--;

        }
        kiemTraNut();
//        if (sLg > khachHangs.size()) {
//                sLg = khachHangs.size()+1;
//            }

        defaultTableModel.setRowCount(0);
        for (int i = (trang_14 - 1) * sLg_14; i <= (trang_14 * sLg_14 - 1); i++) {
            if (i > khS_14.size() - 1) {
                break;
            }
            defaultTableModel.addRow(new Object[]{khachHangs.get(i).getMaDocGia(), khachHangs.get(i).getTenDocGia(), khachHangs.get(i).getDiaChi(), khachHangs.get(i).getSdt()});
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbPopupMenu = new javax.swing.JPopupMenu();
        deleteMenuItem_14 = new javax.swing.JMenuItem();
        editMenuitem_14 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        save_BT_14 = new javax.swing.JButton();
        delete_BT_14 = new javax.swing.JButton();
        edit_BT_14 = new javax.swing.JButton();
        add_BT_14 = new javax.swing.JButton();
        add_The_14 = new javax.swing.JButton();
        huy_BT_14 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        nguoidung_ma_PN = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        addId_14 = new javax.swing.JTextField();
        NguoiDung_Ten_PN = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        addTen_14 = new javax.swing.JTextField();
        NguoiDung_DiaChi_PN = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        addDiaChi_14 = new javax.swing.JTextField();
        NguoiDung_sdt_PN = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        addSdt_14 = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        sdtTK_14 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableKH_14 = new javax.swing.JTable();
        ve = new javax.swing.JButton();
        qua = new javax.swing.JButton();
        jButton1_14 = new javax.swing.JButton();

        deleteMenuItem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        deleteMenuItem_14.setText("Xóa");
        deleteMenuItem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteMenuItem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu.add(deleteMenuItem_14);

        editMenuitem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        editMenuitem_14.setText("Sửa");
        editMenuitem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMenuitem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu.add(editMenuitem_14);

        setBackground(new java.awt.Color(208, 230, 165));
        setToolTipText("");
        setPreferredSize(new java.awt.Dimension(1225, 720));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 102));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Quản Lí Khách Hàng");

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.lightGray, java.awt.Color.lightGray), "Xử lý", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N

        save_BT_14.setBackground(new java.awt.Color(236, 139, 94));
        save_BT_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        save_BT_14.setForeground(new java.awt.Color(20, 26, 70));
        save_BT_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        save_BT_14.setText("Lưu");
        save_BT_14.setEnabled(false);
        save_BT_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_BT_14ActionPerformed(evt);
            }
        });

        delete_BT_14.setBackground(new java.awt.Color(41, 40, 38));
        delete_BT_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        delete_BT_14.setForeground(new java.awt.Color(249, 211, 66));
        delete_BT_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/2_004.png"))); // NOI18N
        delete_BT_14.setText("Xóa");
        delete_BT_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_BT_14ActionPerformed(evt);
            }
        });

        edit_BT_14.setBackground(new java.awt.Color(239, 246, 124));
        edit_BT_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        edit_BT_14.setForeground(new java.awt.Color(41, 95, 45));
        edit_BT_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        edit_BT_14.setText("Sửa");
        edit_BT_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edit_BT_14ActionPerformed(evt);
            }
        });

        add_BT_14.setBackground(new java.awt.Color(138, 170, 229));
        add_BT_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        add_BT_14.setForeground(new java.awt.Color(255, 255, 255));
        add_BT_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Add-Male-User.png"))); // NOI18N
        add_BT_14.setText("Thêm");
        add_BT_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_BT_14ActionPerformed(evt);
            }
        });

        add_The_14.setBackground(new java.awt.Color(36, 54, 101));
        add_The_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        add_The_14.setForeground(new java.awt.Color(139, 216, 189));
        add_The_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/add_The.png"))); // NOI18N
        add_The_14.setText("Thêm Thẻ");
        add_The_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_The_14ActionPerformed(evt);
            }
        });

        huy_BT_14.setBackground(new java.awt.Color(29, 27, 27));
        huy_BT_14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        huy_BT_14.setForeground(new java.awt.Color(236, 77, 55));
        huy_BT_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/huy_Edit.png"))); // NOI18N
        huy_BT_14.setText("Hủy");
        huy_BT_14.setEnabled(false);
        huy_BT_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                huy_BT_14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(huy_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(save_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(edit_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(delete_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(add_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(add_The_14)
                .addContainerGap(204, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(delete_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edit_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_The_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(save_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(huy_BT_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 59, Short.MAX_VALUE))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin người dùng", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N
        jPanel9.setLayout(new java.awt.GridLayout(0, 1));

        jPanel10.setLayout(new java.awt.GridLayout(0, 1, 10, 30));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Mã Đọc Giả :");

        addId_14.setEditable(false);
        addId_14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout nguoidung_ma_PNLayout = new javax.swing.GroupLayout(nguoidung_ma_PN);
        nguoidung_ma_PN.setLayout(nguoidung_ma_PNLayout);
        nguoidung_ma_PNLayout.setHorizontalGroup(
            nguoidung_ma_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nguoidung_ma_PNLayout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addId_14, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        nguoidung_ma_PNLayout.setVerticalGroup(
            nguoidung_ma_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(addId_14, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel10.add(nguoidung_ma_PN);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Tên :");

        addTen_14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_Ten_PNLayout = new javax.swing.GroupLayout(NguoiDung_Ten_PN);
        NguoiDung_Ten_PN.setLayout(NguoiDung_Ten_PNLayout);
        NguoiDung_Ten_PNLayout.setHorizontalGroup(
            NguoiDung_Ten_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_Ten_PNLayout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addTen_14, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        NguoiDung_Ten_PNLayout.setVerticalGroup(
            NguoiDung_Ten_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(addTen_14, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_Ten_PN);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Địa Chỉ : ");

        addDiaChi_14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_DiaChi_PNLayout = new javax.swing.GroupLayout(NguoiDung_DiaChi_PN);
        NguoiDung_DiaChi_PN.setLayout(NguoiDung_DiaChi_PNLayout);
        NguoiDung_DiaChi_PNLayout.setHorizontalGroup(
            NguoiDung_DiaChi_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_DiaChi_PNLayout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addDiaChi_14, javax.swing.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
        );
        NguoiDung_DiaChi_PNLayout.setVerticalGroup(
            NguoiDung_DiaChi_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_DiaChi_PNLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(addDiaChi_14, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_DiaChi_PN);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Số điện thoại :");

        addSdt_14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N

        javax.swing.GroupLayout NguoiDung_sdt_PNLayout = new javax.swing.GroupLayout(NguoiDung_sdt_PN);
        NguoiDung_sdt_PN.setLayout(NguoiDung_sdt_PNLayout);
        NguoiDung_sdt_PNLayout.setHorizontalGroup(
            NguoiDung_sdt_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NguoiDung_sdt_PNLayout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addSdt_14, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        NguoiDung_sdt_PNLayout.setVerticalGroup(
            NguoiDung_sdt_PNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(addSdt_14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
        );

        jPanel10.add(NguoiDung_sdt_PN);

        jPanel9.add(jPanel10);

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Bảng thông tin khách hàng", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Tìm kiếm :");

        sdtTK_14.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        sdtTK_14.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                sdtTK_14InputMethodTextChanged(evt);
            }
        });
        sdtTK_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sdtTK_14ActionPerformed(evt);
            }
        });
        sdtTK_14.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                sdtTK_14PropertyChange(evt);
            }
        });
        sdtTK_14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sdtTK_14KeyPressed(evt);
            }
        });

        tableKH_14.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        tableKH_14.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Mã KH", "Tên KH", "Địa Chỉ", "SDT"
            }
        ));
        tableKH_14.setRowHeight(30);
        tableKH_14.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tableKH_14.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tableKH_14);

        ve.setText("<");
        ve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                veActionPerformed(evt);
            }
        });

        qua.setText(">");
        qua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quaActionPerformed(evt);
            }
        });

        jButton1_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        jButton1_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1_14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(sdtTK_14, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(jButton1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 635, Short.MAX_VALUE)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(244, 244, 244)
                .addComponent(ve)
                .addGap(18, 18, 18)
                .addComponent(qua)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sdtTK_14, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(qua, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ve, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1225, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1205, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
                        .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    //ADD khách hàng
    private void add_BT_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_BT_14ActionPerformed
        if (addTen_14.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập tên Khách Hàng", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (addDiaChi_14.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập địa chỉ Khách Hàng", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (addSdt_14.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng nhập SDT khách ", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                Integer.parseInt(addSdt_14.getText());
                kh = new KhachHang(service.editString(addTen_14.getText()), service.editString(addDiaChi_14.getText()), addSdt_14.getText());
                int check = service.addKH(kh);
                if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Thêm thất bại! Vui lòng thử lại. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else if (check == -1) {
                    JOptionPane.showMessageDialog(this, "Số điện thoại đã có. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Thêm thành công. ");
                    khS_14 = service.getAllKH();

                    setDataTable(khS_14);
                    addId_14.setText("");
                    addTen_14.setText("");
                    addDiaChi_14.setText("");
                    addSdt_14.setText("");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "SDT Không hợp lệ ", "Lỗi", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_add_BT_14ActionPerformed

    private void delete_BT_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_BT_14ActionPerformed
        // TODO add your handling code here:
        int row = tableKH_14.getSelectedRow();
        if (row == -1) {

            JOptionPane.showMessageDialog(this, "Vui lòng chọn tài liệu xóa", "Lỗi", JOptionPane.ERROR_MESSAGE);

        } else {
            int maKH = Integer.valueOf(String.valueOf(tableKH_14.getValueAt(row, 0)));
            int confirm = JOptionPane.showConfirmDialog(this, " Bạn chắc chắn muốn xóa ");
            if (confirm == JOptionPane.YES_OPTION) {
                if (service.deleteKH(maKH) == 0) {
                    JOptionPane.showMessageDialog(this, "Xóa thất bại! Vui lòng thử lại. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Xóa thành công. ");
                    khS_14 = service.getAllKH();

                    setDataTable(khS_14);
                    kiemTraNut();
                }
            }
        }
    }//GEN-LAST:event_delete_BT_14ActionPerformed

    //Chuyền tt khách hàng qua bên
    private void edit_BT_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edit_BT_14ActionPerformed
        // TODO add your handling code here:
        int row = tableKH_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn tài liệu sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            addId_14.setText(String.valueOf(tableKH_14.getValueAt(row, 0)));
            addTen_14.setText(String.valueOf(tableKH_14.getValueAt(row, 1)));
            addDiaChi_14.setText(String.valueOf(tableKH_14.getValueAt(row, 2)));
            addSdt_14.setText(String.valueOf(tableKH_14.getValueAt(row, 3)));

            huy_BT_14.setEnabled(true);
            save_BT_14.setEnabled(true);

            edit_BT_14.setEnabled(false);
            add_BT_14.setEnabled(false);
            delete_BT_14.setEnabled(false);
            add_The_14.setEnabled(false);
        }
    }//GEN-LAST:event_edit_BT_14ActionPerformed

    //sửa thông tin KH
    private void save_BT_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_BT_14ActionPerformed
        // TODO add your handling code here:
        if (addId_14.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn tài liệu sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (service.editKH(kh = new KhachHang(Integer.parseInt(addId_14.getText()), service.editString(addTen_14.getText()), service.editString(addDiaChi_14.getText()), addSdt_14.getText())) == 0) {
            JOptionPane.showMessageDialog(this, "Sửa thất bại", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Sửa thành công");
            khS_14 = service.getAllKH();

            setDataTable(khS_14);
            kiemTraNut();

            addId_14.setText("");
            addTen_14.setText("");
            addDiaChi_14.setText("");
            addSdt_14.setText("");

            huy_BT_14.setEnabled(false);
            save_BT_14.setEnabled(false);

            edit_BT_14.setEnabled(true);
            add_BT_14.setEnabled(true);
            delete_BT_14.setEnabled(true);
            add_The_14.setEnabled(true);
        }
    }//GEN-LAST:event_save_BT_14ActionPerformed

    private void sdtTK_14InputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_sdtTK_14InputMethodTextChanged

    }//GEN-LAST:event_sdtTK_14InputMethodTextChanged

    private void sdtTK_14PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_sdtTK_14PropertyChange

    }//GEN-LAST:event_sdtTK_14PropertyChange

    private void sdtTK_14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sdtTK_14KeyPressed
        // TODO add your handling code here:
        if (!sdtTK_14.getText().equals("")) {

            khS_14 = service.getKhSdt(sdtTK_14.getText());

            setDataTable(khS_14);
            kiemTraNut();

        } else {
            khS_14 = service.getAllKH();

            setDataTable(khS_14);
            kiemTraNut();
        }

    }//GEN-LAST:event_sdtTK_14KeyPressed
    private void kiemTraNut() {
//        if(trang==1){
//            ve.setEnabled(false);
//        }else{
//             ve.setEnabled(true);
//        }
//        if(sLg*(trang+1)>khS.size()){
//            qua.setEnabled(false);
//        }else{
//            qua.setEnabled(true);
//        }

        ve.setEnabled(true);
        qua.setEnabled(true);
        if (trang_14 == 1) {
            ve.setEnabled(false);

        }
        if (sLg_14 * (trang_14) >= khS_14.size()) {
            qua.setEnabled(false);

        }

    }
    private void quaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quaActionPerformed
        // TODO add your handling code here:
        trang_14++;
        setDataTable(khS_14);
        kiemTraNut();


    }//GEN-LAST:event_quaActionPerformed

    private void veActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_veActionPerformed
        // TODO add your handling code here:

        trang_14--;
        setDataTable(khS_14);
        kiemTraNut();
    }//GEN-LAST:event_veActionPerformed

    private void add_The_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_The_14ActionPerformed
        int row = tableKH_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng Khách hàng để thêm thẻ", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int confirm = JOptionPane.showConfirmDialog(this, " Thêm thẻ cho khách hàng " + tableKH_14.getValueAt(row, 1));
            if (confirm == JOptionPane.YES_OPTION) {
                The t = new The();
                t.setMaKhachHang(Integer.parseInt(tableKH_14.getValueAt(row, 0).toString()));
                String ht = String.valueOf(java.time.LocalDate.now());
                t.setNgayBatDau(ht);
                int check = service.addThe(t);
                if (check == -1) {
                    JOptionPane.showMessageDialog(this, "Người dùng này đã có Thẻ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Thêm thất bại", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {

                }
            }
        }
    }//GEN-LAST:event_add_The_14ActionPerformed

    private void sdtTK_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sdtTK_14ActionPerformed
        // TODO add your handling code here:
        if (!sdtTK_14.getText().equals("")) {

            khS_14 = service.getKhSdt(sdtTK_14.getText());

            setDataTable(khS_14);
            kiemTraNut();

        } else {
            khS_14 = service.getAllKH();

            setDataTable(khS_14);
            kiemTraNut();
        }
    }//GEN-LAST:event_sdtTK_14ActionPerformed

    private void jButton1_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1_14ActionPerformed
        // TODO add your handling code here:
        if (!sdtTK_14.getText().equals("")) {

            khS_14 = service.getKhSdt(sdtTK_14.getText());

            setDataTable(khS_14);
            kiemTraNut();

        } else {
            khS_14 = service.getAllKH();

            setDataTable(khS_14);
            kiemTraNut();
        }
    }//GEN-LAST:event_jButton1_14ActionPerformed

    private void huy_BT_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_huy_BT_14ActionPerformed
        // TODO add your handling code here:
        addId_14.setText("");
        addTen_14.setText("");
        addDiaChi_14.setText("");
        addSdt_14.setText("");

        huy_BT_14.setEnabled(false);
        save_BT_14.setEnabled(false);

        edit_BT_14.setEnabled(true);
        add_BT_14.setEnabled(true);
        delete_BT_14.setEnabled(true);
        add_The_14.setEnabled(true);
    }//GEN-LAST:event_huy_BT_14ActionPerformed

    private void deleteMenuItem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteMenuItem_14ActionPerformed
        // TODO add your handling code here:
        int row = tableKH_14.getSelectedRow();
        if (row == -1) {

            JOptionPane.showMessageDialog(this, "Vui lòng chọn tài liệu xóa", "Lỗi", JOptionPane.ERROR_MESSAGE);

        } else {
            int maKH = Integer.valueOf(String.valueOf(tableKH_14.getValueAt(row, 0)));
            int confirm = JOptionPane.showConfirmDialog(this, " Bạn chắc chắn muốn xóa ");
            if (confirm == JOptionPane.YES_OPTION) {
                if (service.deleteKH(maKH) == 0) {
                    JOptionPane.showMessageDialog(this, "Xóa thất bại! Vui lòng thử lại. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "Xóa thành công. ");
                    khS_14 = service.getAllKH();

                    setDataTable(khS_14);
                    kiemTraNut();
                }
            }
        }
    }//GEN-LAST:event_deleteMenuItem_14ActionPerformed

    private void editMenuitem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editMenuitem_14ActionPerformed
        // TODO add your handling code here:
        int row = tableKH_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn tài liệu sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            addId_14.setText(String.valueOf(tableKH_14.getValueAt(row, 0)));
            addTen_14.setText(String.valueOf(tableKH_14.getValueAt(row, 1)));
            addDiaChi_14.setText(String.valueOf(tableKH_14.getValueAt(row, 2)));
            addSdt_14.setText(String.valueOf(tableKH_14.getValueAt(row, 3)));

            huy_BT_14.setEnabled(true);
            save_BT_14.setEnabled(true);

            edit_BT_14.setEnabled(false);
            add_BT_14.setEnabled(false);
            delete_BT_14.setEnabled(false);
            add_The_14.setEnabled(false);
        }
    }//GEN-LAST:event_editMenuitem_14ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel NguoiDung_DiaChi_PN;
    private javax.swing.JPanel NguoiDung_Ten_PN;
    private javax.swing.JPanel NguoiDung_sdt_PN;
    private javax.swing.JTextField addDiaChi_14;
    private javax.swing.JTextField addId_14;
    private javax.swing.JTextField addSdt_14;
    private javax.swing.JTextField addTen_14;
    private javax.swing.JButton add_BT_14;
    private javax.swing.JButton add_The_14;
    private javax.swing.JMenuItem deleteMenuItem_14;
    private javax.swing.JButton delete_BT_14;
    private javax.swing.JMenuItem editMenuitem_14;
    private javax.swing.JButton edit_BT_14;
    private javax.swing.JButton huy_BT_14;
    private javax.swing.JButton jButton1_14;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel nguoidung_ma_PN;
    private javax.swing.JButton qua;
    private javax.swing.JButton save_BT_14;
    private javax.swing.JTextField sdtTK_14;
    private javax.swing.JTable tableKH_14;
    private javax.swing.JPopupMenu tbPopupMenu;
    private javax.swing.JButton ve;
    // End of variables declaration//GEN-END:variables
}
