package view.theLoai;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.TheLoai;
import service.Service_14;

public class TheLoaiPN extends javax.swing.JPanel {

    DefaultTableModel qlTheLoaiModel = null;
    Service_14 service_14 = null;
    List<TheLoai> theLoaiS_14 = null;

    public TheLoaiPN() {
        service_14 = new Service_14();
        initComponents();
        qlTheLoaiModel = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblQLTacGia.setModel(qlTheLoaiModel);
        //them cot cho TacGiaTB
        qlTheLoaiModel.addColumn("Mã thể loại");
        qlTheLoaiModel.addColumn("Tên thể loại");

        //them du lieu vao bookTB
        theLoaiS_14 = service_14.getAllTheLoai();
        setDataTblQLTacGia(theLoaiS_14);
        tblQLTacGia.setComponentPopupMenu(tbPopupMenu_14);
    }

    public void setDataTblQLTacGia(List<TheLoai> tls) {
        qlTheLoaiModel.setRowCount(0);
        for (TheLoai tl : tls) {
            qlTheLoaiModel.addRow(new Object[]{tl.getMaTheLoai(), tl.getTenTheLoai()});
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbPopupMenu_14 = new javax.swing.JPopupMenu();
        editMenuItem_14 = new javax.swing.JMenuItem();
        btnAddTL = new javax.swing.JButton();
        btnEditTL = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        timKiemTL = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblQLTacGia = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtIdTL = new javax.swing.JTextField();
        txtTenTL = new javax.swing.JTextField();
        btnSaveTL = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        editMenuItem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        editMenuItem_14.setText("Sửa");
        editMenuItem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMenuItem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu_14.add(editMenuItem_14);

        setPreferredSize(new java.awt.Dimension(1225, 720));

        btnAddTL.setBackground(new java.awt.Color(138, 170, 229));
        btnAddTL.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnAddTL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/add_48px.png"))); // NOI18N
        btnAddTL.setText("Thêm");
        btnAddTL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTLActionPerformed(evt);
            }
        });

        btnEditTL.setBackground(new java.awt.Color(239, 246, 124));
        btnEditTL.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnEditTL.setForeground(new java.awt.Color(41, 95, 45));
        btnEditTL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        btnEditTL.setText("Sửa");
        btnEditTL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditTLActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 30)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("THÔNG TIN THỂ LOẠI");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/search-b-icon.png"))); // NOI18N
        jLabel5.setText("Tìm kiếm :");
        jLabel5.setToolTipText("");

        timKiemTL.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                timKiemTLPropertyChange(evt);
            }
        });
        timKiemTL.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timKiemTLKeyPressed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(41, 40, 38));
        btnCancel.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(249, 211, 66));
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/exit.png"))); // NOI18N
        btnCancel.setText("Hủy");
        btnCancel.setEnabled(false);
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel1.setText("Mã thể loại:");

        tblQLTacGia.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        tblQLTacGia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblQLTacGia.setRowHeight(30);
        tblQLTacGia.setSelectionBackground(new java.awt.Color(58, 175, 169));
        tblQLTacGia.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tblQLTacGia);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel2.setText("Tên thể loại:");

        txtIdTL.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtIdTL.setEnabled(false);

        txtTenTL.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        btnSaveTL.setBackground(new java.awt.Color(236, 139, 94));
        btnSaveTL.setFont(new java.awt.Font("Tahoma", 3, 15)); // NOI18N
        btnSaveTL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        btnSaveTL.setText("Lưu");
        btnSaveTL.setEnabled(false);
        btnSaveTL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveTLActionPerformed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/LIBRARY.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(64, 64, 64)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel1))
                            .addGap(30, 30, 30)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtIdTL, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTenTL, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnCancel)
                            .addGap(18, 18, 18)
                            .addComponent(btnAddTL)
                            .addGap(18, 18, 18)
                            .addComponent(btnEditTL)
                            .addGap(18, 18, 18)
                            .addComponent(btnSaveTL)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(timKiemTL, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addGap(63, 63, 63))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(66, 66, 66)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(69, 69, 69)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(timKiemTL, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(50, 50, 50))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(35, 35, 35)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdTL, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTenTL, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSaveTL, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddTL, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEditTL, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 153, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddTLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTLActionPerformed

        if (txtTenTL.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Chọn thể loại sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int check = service_14.addTheLoai(txtTenTL.getText());
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Thêm thất bại", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Thêm thành công");
                theLoaiS_14 = service_14.getAllTheLoai();
                setDataTblQLTacGia(theLoaiS_14);
            }
        }
    }//GEN-LAST:event_btnAddTLActionPerformed

    private void btnEditTLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditTLActionPerformed
        int row = tblQLTacGia.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Chọn thể loại sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            txtIdTL.setText(tblQLTacGia.getValueAt(row, 0).toString());
            txtTenTL.setText(tblQLTacGia.getValueAt(row, 1).toString());

            btnSaveTL.setEnabled(true);
            btnCancel.setEnabled(true);

            btnAddTL.setEnabled(false);
            btnEditTL.setEnabled(false);

        }
    }//GEN-LAST:event_btnEditTLActionPerformed

    private void timKiemTLPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_timKiemTLPropertyChange
        if (timKiemTL.getText().equals("")) {
            theLoaiS_14 = service_14.getAllTheLoai();
            setDataTblQLTacGia(theLoaiS_14);
        } else {
            theLoaiS_14 = service_14.timKiemTheLoai(timKiemTL.getText());
            setDataTblQLTacGia(theLoaiS_14);
        }

    }//GEN-LAST:event_timKiemTLPropertyChange

    private void timKiemTLKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timKiemTLKeyPressed
        if (timKiemTL.getText().equals("")) {
            theLoaiS_14 = service_14.getAllTheLoai();
            setDataTblQLTacGia(theLoaiS_14);
        } else {
            theLoaiS_14 = service_14.timKiemTheLoai(timKiemTL.getText());
            setDataTblQLTacGia(theLoaiS_14);
        }

    }//GEN-LAST:event_timKiemTLKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (timKiemTL.getText().equals("")) {
            theLoaiS_14 = service_14.getAllTheLoai();
            setDataTblQLTacGia(theLoaiS_14);
        } else {
            theLoaiS_14 = service_14.timKiemTheLoai(timKiemTL.getText());
            setDataTblQLTacGia(theLoaiS_14);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed

        txtIdTL.setText("");
        txtTenTL.setText("");

        btnSaveTL.setEnabled(false);
        btnCancel.setEnabled(false);

        btnAddTL.setEnabled(true);
        btnEditTL.setEnabled(true);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveTLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveTLActionPerformed
        if (txtTenTL.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Nhập tên thể loại", "Lỗi", ERROR);
        } else {
            TheLoai tl = new TheLoai(Integer.parseInt(txtIdTL.getText()), txtTenTL.getText());
            int check = service_14.editTheLoai(tl);
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Thêm thất bại", "Lỗi", ERROR);
            } else {
                JOptionPane.showMessageDialog(this, "Thêm thành công");
                theLoaiS_14 = service_14.getAllTheLoai();
                setDataTblQLTacGia(theLoaiS_14);

                txtIdTL.setText("");
                txtTenTL.setText("");

                btnSaveTL.setEnabled(false);
                btnCancel.setEnabled(false);

                btnAddTL.setEnabled(true);
                btnEditTL.setEnabled(true);
            }
        }
    }//GEN-LAST:event_btnSaveTLActionPerformed

    private void editMenuItem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editMenuItem_14ActionPerformed
        // TODO add your handling code here:
        int row = tblQLTacGia.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Chọn thể loại sửa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            txtIdTL.setText(tblQLTacGia.getValueAt(row, 0).toString());
            txtTenTL.setText(tblQLTacGia.getValueAt(row, 1).toString());

            btnSaveTL.setEnabled(true);
            btnCancel.setEnabled(true);

            btnAddTL.setEnabled(false);
            btnEditTL.setEnabled(false);

        }
    }//GEN-LAST:event_editMenuItem_14ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddTL;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEditTL;
    private javax.swing.JButton btnSaveTL;
    private javax.swing.JMenuItem editMenuItem_14;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu tbPopupMenu_14;
    private javax.swing.JTable tblQLTacGia;
    private javax.swing.JTextField timKiemTL;
    private javax.swing.JTextField txtIdTL;
    private javax.swing.JTextField txtTenTL;
    // End of variables declaration//GEN-END:variables
}
