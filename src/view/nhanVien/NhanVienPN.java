package view.nhanVien;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import model.NguoiDung;
import service.*;

public class NhanVienPN extends javax.swing.JPanel {

    DefaultTableModel dsNVmodel = null;
    NguoiDung nguoidung;
    List<NguoiDung> nd = new ArrayList<NguoiDung>();
    Service_27 nguoidungService_27 = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public NhanVienPN() {
        nguoidungService_27 = new Service_27();
        initComponents();
        dsNVmodel = new DefaultTableModel() {
            public boolean isCellEdittable(int row, int column) {
                return false;
            }
        };
        table_nv.setModel(dsNVmodel);
        dsNVmodel.addColumn("Mã NV");
        dsNVmodel.addColumn("Tên NV");
        dsNVmodel.addColumn("Tên tài khoản");
        dsNVmodel.addColumn("Mật khẩu");
        dsNVmodel.addColumn("Giới tính");
        dsNVmodel.addColumn("Ngày sinh");
        dsNVmodel.addColumn("Số điện thoại");
        dsNVmodel.addColumn("Trạng thái");

        showNV(nguoidungService_27.getAllND());
    }

    final void showNV(List<NguoiDung> nd) {
        dsNVmodel.setRowCount(0);
        for (NguoiDung nguoidung : nd) {
            dsNVmodel.addRow(new Object[]{nguoidung.getMaND(), nguoidung.getTenND(), nguoidung.getTaiKhoan(), nguoidung.getMatKhau(), nguoidung.getGioiTinh(), nguoidung.getNgaySinh(), nguoidung.getSdt(), nguoidung.getTrangThai()});
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        sdt_txt = new javax.swing.JTextField();
        manv_txt = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        trangthai_txt = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        gioitinhnam = new javax.swing.JRadioButton();
        gioitinhnu = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        add_327 = new javax.swing.JButton();
        fix_327 = new javax.swing.JButton();
        delete_327 = new javax.swing.JButton();
        save_327 = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        search_327 = new javax.swing.JButton();
        birthday = new com.toedter.calendar.JDateChooser();
        tennv_txt = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        pass_txt = new javax.swing.JPasswordField();
        jLabel16 = new javax.swing.JLabel();
        taikhoan_txt = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_nv = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(1225, 720));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("QUẢN LÝ NHÂN VIÊN");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Tên tài khoản:");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Số điện thoại:");

        sdt_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        manv_txt.setEditable(false);
        manv_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Tên NV:");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Trạng thái:");

        trangthai_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Giới tính:");

        gioitinhnam.setBackground(new java.awt.Color(207, 244, 210));
        buttonGroup1.add(gioitinhnam);
        gioitinhnam.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        gioitinhnam.setSelected(true);
        gioitinhnam.setText("Nam");

        gioitinhnu.setBackground(new java.awt.Color(207, 244, 210));
        buttonGroup1.add(gioitinhnu);
        gioitinhnu.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        gioitinhnu.setText("Nữ");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Ngày sinh:");

        add_327.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        add_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/add_48px.png"))); // NOI18N
        add_327.setText("Thêm ");
        add_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_327ActionPerformed(evt);
            }
        });

        fix_327.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        fix_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        fix_327.setText("Sửa");
        fix_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fix_327ActionPerformed(evt);
            }
        });

        delete_327.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        delete_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/dongcuoi.png"))); // NOI18N
        delete_327.setText("Xóa");
        delete_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_327ActionPerformed(evt);
            }
        });

        save_327.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        save_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        save_327.setText("Lưu");
        save_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_327ActionPerformed(evt);
            }
        });

        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
        });

        search_327.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        search_327.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        search_327.setText("Tìm kiếm");
        search_327.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                search_327ActionPerformed(evt);
            }
        });

        birthday.setDateFormatString(" yyyy-MM-dd");
        birthday.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        tennv_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Mật khẩu:");

        pass_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Mã NV:");
        jLabel16.setMaximumSize(new java.awt.Dimension(75, 22));
        jLabel16.setMinimumSize(new java.awt.Dimension(75, 22));

        taikhoan_txt.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N

        table_nv.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        table_nv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_nv.setRowHeight(30);
        table_nv.setSelectionBackground(new java.awt.Color(58, 175, 169));
        table_nv.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(table_nv);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(manv_txt, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                                            .addComponent(tennv_txt)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(taikhoan_txt, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                                            .addComponent(pass_txt))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(gioitinhnam)
                                        .addGap(74, 74, 74)
                                        .addComponent(gioitinhnu))
                                    .addComponent(sdt_txt)
                                    .addComponent(trangthai_txt)
                                    .addComponent(birthday, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(add_327, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(fix_327, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(43, 43, 43)
                                .addComponent(delete_327, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33)
                                .addComponent(save_327, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(search_327, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1))
                        .addGap(30, 30, 30)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(manv_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(gioitinhnam, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(gioitinhnu, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tennv_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(birthday, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sdt_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(trangthai_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(taikhoan_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pass_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(delete_327, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add_327, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(save_327, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fix_327, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(search_327, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    public void searchUser() {
        try {
            showNV(nguoidungService_27.getUserByTitle(txtSearch.getText()));

        } catch (SQLException ex) {
            Logger.getLogger(NhanVienPN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void add_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add_327ActionPerformed
        if (tennv_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên nhân viên", "Lỗi", ERROR_MESSAGE);
        } else if (taikhoan_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên tài khoản", "Lỗi", ERROR_MESSAGE);
        } else if (pass_txt.getPassword().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập mật khẩu", "Lỗi", ERROR_MESSAGE);
        } else if (sdt_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập số điện thoại", "Lỗi", ERROR_MESSAGE);

        } else {
            nguoidung = new NguoiDung();
            nguoidung.setTenND(tennv_txt.getText());
            nguoidung.setTaiKhoan(taikhoan_txt.getText());
            nguoidung.setMatKhau(String.valueOf(pass_txt.getPassword()));
            if (gioitinhnam.isSelected()) {
                nguoidung.setGioiTinh(gioitinhnam.getText());
            } else if (gioitinhnu.isSelected()) {
                nguoidung.setGioiTinh(gioitinhnu.getText());
            }
            nguoidung.setNgaySinh(sdf.format(birthday.getDate()));
            nguoidung.setSdt(String.valueOf(sdt_txt.getText()));
            nguoidung.setTrangThai(Integer.valueOf(trangthai_txt.getText()));
            if (nguoidungService_27.addUser(nguoidung) == 0) {
                JOptionPane.showMessageDialog(this, "Thêm thất bại", "Lỗi", ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Thêm thành công. ");
            }
        }
        
        showNV(nguoidungService_27.getAllND());

    }//GEN-LAST:event_add_327ActionPerformed

    private void fix_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fix_327ActionPerformed
        int row = table_nv.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn nhân viên để sửa!", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                manv_txt.setText(String.valueOf(table_nv.getValueAt(row, 0)));
                tennv_txt.setText(String.valueOf(table_nv.getValueAt(row, 1)));
                taikhoan_txt.setText(String.valueOf(table_nv.getValueAt(row, 2)));
                pass_txt.setText(String.valueOf(table_nv.getValueAt(row, 3)));
                if (table_nv.getValueAt(row, 4).equals("Nam")) {
                    gioitinhnam.setSelected(true);
                } else {
                    gioitinhnu.setSelected(true);
                }
                birthday.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(table_nv.getValueAt(row, 5).toString()));
                sdt_txt.setText(String.valueOf(table_nv.getValueAt(row, 6)));
                trangthai_txt.setText(String.valueOf(table_nv.getValueAt(row, 7)));
            } catch (ParseException ex) {
                Logger.getLogger(NhanVienPN.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_fix_327ActionPerformed

    private void delete_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delete_327ActionPerformed
        int row = table_nv.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn nhân viên để xóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int ma_nd = Integer.valueOf(String.valueOf(table_nv.getValueAt(row, 0)));
            int confirm = JOptionPane.showConfirmDialog(this, "Bạn chắc chắn muốn xóa ?");
            if (confirm == JOptionPane.YES_OPTION) {
                if (nguoidungService_27.deleteUser(ma_nd) == 0) {
                    JOptionPane.showMessageDialog(this, "Xóa thất bại! Vui lòng thử lại. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    showNV(nguoidungService_27.getAllND());
                    JOptionPane.showMessageDialog(this, "Xóa thành công. ");
                }
            }
        }
    }//GEN-LAST:event_delete_327ActionPerformed

    private void save_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_327ActionPerformed
        if (manv_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập mã nhân viên", "Lỗi", ERROR_MESSAGE);
        } else if (tennv_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên nhân viên", "Lỗi", ERROR_MESSAGE);
        } else if (taikhoan_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên tài khoản", "Lỗi", ERROR_MESSAGE);
        } else if (pass_txt.getPassword().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập mật khẩu", "Lỗi", ERROR_MESSAGE);
        } else if (sdt_txt.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập số điện thoại", "Lỗi", ERROR_MESSAGE);
        } else {
            nguoidung = new NguoiDung();
            nguoidung.setMaND(Integer.valueOf(manv_txt.getText()));
            nguoidung.setTenND(tennv_txt.getText());
            nguoidung.setTaiKhoan(taikhoan_txt.getText());
            nguoidung.setMatKhau(String.valueOf(pass_txt.getPassword()));
            nguoidung.setGioiTinh(gioitinhnam.isSelected() ? "Nam" : "Nữ");
            nguoidung.setNgaySinh(new SimpleDateFormat("yyyy-MM-dd").format(birthday.getDate()));
            nguoidung.setSdt(String.valueOf(sdt_txt.getText()));
            nguoidung.setTrangThai(Integer.valueOf(trangthai_txt.getText()));

            int check = nguoidungService_27.updateUser(nguoidung);
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Sửa thất bại", "Lỗi", ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Sửa thành công.");
                showNV(nguoidungService_27.getAllND());
            }
        }


    }//GEN-LAST:event_save_327ActionPerformed

    private void search_327ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_search_327ActionPerformed
        searchUser();
    }//GEN-LAST:event_search_327ActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            searchUser();
        }
    }//GEN-LAST:event_txtSearchKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add_327;
    private com.toedter.calendar.JDateChooser birthday;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton delete_327;
    private javax.swing.JButton fix_327;
    private javax.swing.JRadioButton gioitinhnam;
    private javax.swing.JRadioButton gioitinhnu;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField manv_txt;
    private javax.swing.JPasswordField pass_txt;
    private javax.swing.JButton save_327;
    private javax.swing.JTextField sdt_txt;
    private javax.swing.JButton search_327;
    private javax.swing.JTable table_nv;
    private javax.swing.JTextField taikhoan_txt;
    private javax.swing.JTextField tennv_txt;
    private javax.swing.JTextField trangthai_txt;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
