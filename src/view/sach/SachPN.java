package view.sach;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.Sach;
import service.*;

public class SachPN extends javax.swing.JPanel {

    DefaultTableModel qlSachModel = null;
    Sach sach;
    List<Sach> s = new ArrayList<Sach>();
    Service_23 sachService_23 = null;
    Service_16 sachService_16 = null;

    public SachPN() {
        sachService_23 = new Service_23();
        sachService_16 = new Service_16();
        initComponents();
        //sach co trong thu vien
        qlSachModel = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblQLSach.setModel(qlSachModel);
        //them cot cho bookTB
        qlSachModel.addColumn("Id sách");
        qlSachModel.addColumn("Tên sách");
        qlSachModel.addColumn("Tác giả");
        qlSachModel.addColumn("Thể loại");
        qlSachModel.addColumn("Nhà xuất bản");
        qlSachModel.addColumn("Năm xuất bản");
        qlSachModel.addColumn("Tiền mượn");
        qlSachModel.addColumn("Tiền cọc");
        qlSachModel.addColumn("Trạng thái");
        //them du lieu vao bookTB
        addRowShowSach(sachService_23.getDSSach());
        loadTrangThai();
        loadMaTheLoai();
        loadTacGia();
        loadNXB();

//        widthColumnTB_Muon();
    }

    final void addRowShowSach(List<Sach> sachs) {
        qlSachModel.setRowCount(0);
        for (Sach sach : sachs) {
            String trangThai = sachService_16.getTrangThaiByMaTrangThai(sach.getTrangThai());
            String tacGia = sachService_23.getTacGiaByMaTacGia(sach.getMaTacGia());
            String theLoai = sachService_23.getTtheLoaiByMaTheLoai(sach.getMaTheLoai());
            String NXB = sachService_23.getNXBByMaNXB(sach.getMaNXB());
            qlSachModel.addRow(new Object[]{sach.getId(), sach.getTen(), tacGia, theLoai, NXB,
                sach.getNamXB(), sach.getTienMuon(), sach.getTienCoc(), trangThai});
        }
    }

    public void loadMaTheLoai() {
        List<String> theLoais = sachService_16.getAllTheLoai();
        for (String theLoai : theLoais) {
            cbbTheLoai.addItem(theLoai);
            cbbTheLoaiEdit.addItem(theLoai);
        }
    }

    public void loadTrangThai() {
        List<String> trangThais = sachService_16.getAllTrangThai();
        for (String trangThai : trangThais) {
            cbbTrangThai.addItem(trangThai);
            cbbTrangThaiEdit.addItem(trangThai);
        }
    }

    public void loadTacGia() {
        List<String> tacGias = sachService_16.getAllTacGia();
        for (String tacGia : tacGias) {
            cbbTacGia.addItem(tacGia);
            cbbIdTacGia.addItem(tacGia);
        }
    }

    public void loadNXB() {
        List<String> NXBs = sachService_23.getAllNXB();
        for (String NXB : NXBs) {
            cbbNXB.addItem(NXB);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    public void widthColumnTB_Muon() {
        TableColumnModel columnModel = tblQLSach.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(80);
        columnModel.getColumn(0).setMaxWidth(80);
        columnModel.getColumn(1).setPreferredWidth(800);
        columnModel.getColumn(1).setMaxWidth(800);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(2).setMaxWidth(100);
        columnModel.getColumn(3).setPreferredWidth(300);
        columnModel.getColumn(3).setMaxWidth(300);
    }

    public void searchSach() {
        try {
            addRowShowSach(sachService_23.searchSach(txtSearch.getText()));
            cbbTrangThai.setSelectedItem("Tất cả sách");
            cbbTheLoai.setSelectedItem("Tất cả thể loại");
            cbbTacGia.setSelectedItem("Tất cả tác giả");
        } catch (SQLException ex) {
            Logger.getLogger(SachPN.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSearch = new javax.swing.JButton();
        cbbTrangThai = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        cbbTheLoai = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblQLSach = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtTenSach = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtidSach = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNamXuatBan = new javax.swing.JTextField();
        txtTienMuon = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnXoa = new javax.swing.JButton();
        btnThem = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        cbbNXB = new javax.swing.JComboBox<>();
        cbbTheLoaiEdit = new javax.swing.JComboBox<>();
        cbbIdTacGia = new javax.swing.JComboBox<>();
        btnSave = new javax.swing.JButton();
        cbbTrangThaiEdit = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtTienCoc = new javax.swing.JTextField();
        cbbTacGia = new javax.swing.JComboBox<>();
        btnLoc = new javax.swing.JButton();

        btnSearch.setBackground(new java.awt.Color(251, 85, 51));
        btnSearch.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        cbbTrangThai.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        cbbTrangThai.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả sách" }));

        jLabel10.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel10.setText("Tìm kiếm:");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
        });

        cbbTheLoai.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        cbbTheLoai.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả thể loại" }));
        cbbTheLoai.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbTheLoaiItemStateChanged(evt);
            }
        });

        tblQLSach.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        tblQLSach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblQLSach.setRowHeight(30);
        tblQLSach.setSelectionBackground(new java.awt.Color(58, 175, 169));
        jScrollPane1.setViewportView(tblQLSach);

        jPanel2.setBackground(new java.awt.Color(211, 227, 252));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Tên sách:");

        txtTenSach.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Id sách:");

        txtidSach.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtidSach.setEnabled(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Tác giả:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel4.setText("Thể loại:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel5.setText("Mã NXB:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel6.setText("Năm xuất bản:");

        txtNamXuatBan.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        txtTienMuon.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel7.setText("Trạng thái:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel8.setText("Tiền mượn:");

        btnXoa.setBackground(new java.awt.Color(41, 40, 38));
        btnXoa.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnXoa.setForeground(new java.awt.Color(249, 211, 66));
        btnXoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/xoa.png"))); // NOI18N
        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        btnThem.setBackground(new java.awt.Color(138, 170, 229));
        btnThem.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnThem.setForeground(new java.awt.Color(255, 255, 255));
        btnThem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/addbook-icon.png"))); // NOI18N
        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnSua.setBackground(new java.awt.Color(239, 246, 124));
        btnSua.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnSua.setForeground(new java.awt.Color(41, 95, 45));
        btnSua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/edit-icon.png"))); // NOI18N
        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        cbbNXB.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        cbbTheLoaiEdit.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        cbbIdTacGia.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        btnSave.setBackground(new java.awt.Color(236, 139, 94));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnSave.setForeground(new java.awt.Color(20, 26, 70));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        btnSave.setText("Lưu");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        cbbTrangThaiEdit.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel9.setText("Tiền cọc:");

        txtTienCoc.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cbbIdTacGia, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTenSach, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtidSach, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(250, 250, 250)
                        .addComponent(btnThem)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtNamXuatBan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbbNXB, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbbTheLoaiEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(65, 65, 65)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(txtTienMuon, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel9)
                                        .addGap(18, 18, 18)))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbbTrangThaiEdit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTienCoc, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))))
                        .addContainerGap(36, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addComponent(btnSua, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnXoa, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(250, 250, 250))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTienMuon, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbbTheLoaiEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTienCoc, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cbbNXB, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cbbTrangThaiEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtNamXuatBan, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtidSach, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTenSach, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbbIdTacGia, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnXoa, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnThem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSua, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        cbbTacGia.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        cbbTacGia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả tác giả" }));

        btnLoc.setBackground(new java.awt.Color(13, 92, 182));
        btnLoc.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        btnLoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/filter16.png"))); // NOI18N
        btnLoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLocActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbbTrangThai, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55)
                                .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(66, 66, 66)
                                .addComponent(cbbTacGia, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                            .addComponent(btnLoc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1141, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(42, 42, 42))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(cbbTrangThai, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(cbbTheLoai, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnLoc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbTacGia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        searchSach();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            searchSach();
        }
    }//GEN-LAST:event_txtSearchKeyPressed

    private void cbbTheLoaiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbTheLoaiItemStateChanged
//        String tlSelected = cbbTheLoai.getSelectedItem().toString();
//        if (tlSelected.equals("Tất cả thể loại") == true) {
//            addRowShowSach(sachService_23.getDSSach());
//        } else {
//            addRowShowSach(sachService_23.getTheLoaiSach(tlSelected));
//        }
    }//GEN-LAST:event_cbbTheLoaiItemStateChanged

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed

        if (txtTenSach.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên sách", "Lỗi", ERROR_MESSAGE);
        } else if (txtNamXuatBan.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập năm xuất bản", "Lỗi", ERROR_MESSAGE);
        } else if (txtTienMuon.getText().equals("")) { //??
            JOptionPane.showMessageDialog(this, "Cần nhập mã tiền mượn", "Lỗi", ERROR_MESSAGE);
        } else if (txtTienCoc.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tiền cọc", "Lỗi", ERROR_MESSAGE);
        } else {
            sach = new Sach();
            sach.setTen(txtTenSach.getText());
            sach.setMaTacGia(sachService_23.getMaTacGia(cbbIdTacGia.getSelectedItem().toString()));
            sach.setMaTheLoai(sachService_23.getMaTheLoai(cbbTheLoaiEdit.getSelectedItem().toString()));
            sach.setMaNXB(sachService_23.getMaNXB(cbbNXB.getSelectedItem().toString()));
            sach.setNamXB(Integer.valueOf(txtNamXuatBan.getText()));
            sach.setTienMuon(Double.parseDouble(txtTienMuon.getText()));
            sach.setTienCoc(Double.parseDouble(txtTienCoc.getText()));
            sach.setTrangThai(sachService_23.getMaTrangThai(cbbTrangThaiEdit.getSelectedItem().toString()));
            if (sachService_23.addBook(sach) == 0) {
                JOptionPane.showMessageDialog(this, "Thêm thất bại", "Lỗi", ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Thêm thành công. ");
                addRowShowSach(sachService_23.getDSSach());
                txtTenSach.setText("");
                txtNamXuatBan.setText("");
                txtTienMuon.setText("");
                txtTienCoc.setText("");
            }
        }
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        int row = tblQLSach.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn sách để sửa!", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            txtidSach.setText(String.valueOf(tblQLSach.getValueAt(row, 0)));
            txtTenSach.setText(String.valueOf(tblQLSach.getValueAt(row, 1)));
            cbbIdTacGia.setSelectedItem(String.valueOf(tblQLSach.getValueAt(row, 2)));
            cbbTheLoaiEdit.setSelectedItem(String.valueOf(tblQLSach.getValueAt(row, 3)));
            cbbNXB.setSelectedItem(String.valueOf(tblQLSach.getValueAt(row, 4)));
            txtNamXuatBan.setText(String.valueOf(tblQLSach.getValueAt(row, 5)));
            txtTienMuon.setText(String.valueOf(tblQLSach.getValueAt(row, 6)));
            txtTienCoc.setText(String.valueOf(tblQLSach.getValueAt(row, 7)));
            cbbTrangThaiEdit.setSelectedItem(String.valueOf(tblQLSach.getValueAt(row, 8)));
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        int row = tblQLSach.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn sách để xóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            int maSach = Integer.valueOf(String.valueOf(tblQLSach.getValueAt(row, 0)));
            int confirm = JOptionPane.showConfirmDialog(this, "Bạn chắc chắn muốn xóa ?");
            if (confirm == JOptionPane.YES_OPTION) {
                if (sachService_23.deleteBook(maSach) == 0) {
                    JOptionPane.showMessageDialog(this, "Xóa thất bại! Vui lòng thử lại. ", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    addRowShowSach(sachService_23.getDSSach());
                }
            }
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (txtTenSach.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tên sách", "Lỗi", ERROR_MESSAGE);
        } else if (txtNamXuatBan.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập năm xuất bản", "Lỗi", ERROR_MESSAGE);
        } else if (txtTienMuon.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập mã tiền mượn", "Lỗi", ERROR_MESSAGE);
        } else if (txtTienCoc.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Cần nhập tiền cọc", "Lỗi", ERROR_MESSAGE);
        } else {
            sach = new Sach();
            sach.setId(Integer.parseInt(txtidSach.getText()));
            sach.setTen(txtTenSach.getText());
            sach.setMaTacGia(sachService_23.getMaTacGia(cbbIdTacGia.getSelectedItem().toString()));
            sach.setMaTheLoai(sachService_23.getMaTheLoai(cbbTheLoaiEdit.getSelectedItem().toString()));
            sach.setMaNXB(sachService_23.getMaNXB(cbbNXB.getSelectedItem().toString()));
            sach.setNamXB(Integer.valueOf(txtNamXuatBan.getText()));
            sach.setTienMuon(Double.parseDouble(txtTienMuon.getText()));
            sach.setTienCoc(Double.parseDouble(txtTienCoc.getText()));
            sach.setTrangThai(sachService_23.getMaTrangThai(cbbTrangThaiEdit.getSelectedItem().toString()));
            int check = sachService_23.updateBook(sach);
            if (check == 0) {
                JOptionPane.showMessageDialog(this, "Sửa thất bại", "Lỗi", ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Sửa thành công.");
                addRowShowSach(sachService_23.getDSSach());
                txtidSach.setText("");
                txtTenSach.setText("");
                txtNamXuatBan.setText("");
                txtTienMuon.setText("");
                txtTienCoc.setText("");
            }
        }

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnLocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLocActionPerformed
        String tt = cbbTrangThai.getSelectedItem().toString();
        String tl = cbbTheLoai.getSelectedItem().toString();
        String tg = cbbTacGia.getSelectedItem().toString();
        addRowShowSach(sachService_16.filterSach(tt, tg, tl));
    }//GEN-LAST:event_btnLocActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLoc;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnXoa;
    private javax.swing.JComboBox<String> cbbIdTacGia;
    private javax.swing.JComboBox<String> cbbNXB;
    private javax.swing.JComboBox<String> cbbTacGia;
    private javax.swing.JComboBox<String> cbbTheLoai;
    private javax.swing.JComboBox<String> cbbTheLoaiEdit;
    private javax.swing.JComboBox<String> cbbTrangThai;
    private javax.swing.JComboBox<String> cbbTrangThaiEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblQLSach;
    private javax.swing.JTextField txtNamXuatBan;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtTenSach;
    private javax.swing.JTextField txtTienCoc;
    private javax.swing.JTextField txtTienMuon;
    private javax.swing.JTextField txtidSach;
    // End of variables declaration//GEN-END:variables
}
