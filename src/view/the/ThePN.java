package view.the;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.The;
import service.Service_14;

public class ThePN extends javax.swing.JPanel {

    Service_14 service;
    DefaultTableModel defaultTableModel;
    List<The> cards_14=null;
    int trangThai_14;
    int biKhoa = 0, hd = 1, hetHan = -1;

    public ThePN() {
        initComponents();

        service = new Service_14();
        service.setTrangThaiHetHan();
        defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dataTable_14.setModel(defaultTableModel);
        defaultTableModel.addColumn("Mã Thẻ");
        defaultTableModel.addColumn("Tên KH");
        defaultTableModel.addColumn("Ngày tạo");
        defaultTableModel.addColumn("Ngày hết hạn");
        defaultTableModel.addColumn("Trạng thái");
        defaultTableModel.setRowCount(0);
        cards_14 = service.getAllThe();
        trangThai_14 = -3;
        setDataTable(cards_14, trangThai_14);
        dataTable_14.setComponentPopupMenu(tbPopupMenu);
    }

    private void setDataTable(List<The> theS, int trangThai) {
        defaultTableModel.setRowCount(0);
        String tt = "";
        switch (trangThai) {
            case -3:
                
                for (The the : theS) {
                    if (the.getTrangThai() == hd) {
                        tt = "Hoạt Động";
                    } else if (the.getTrangThai() == biKhoa) {
                        tt = "Bị khóa";
                    } else {
                        tt = "Hết hạn";
                    }
                    defaultTableModel.addRow(new Object[]{the.getSoThe(), service.getTenByMa(the.getMaKhachHang()), the.getNgayBatDau(), the.getNgayKetThuc(), tt});
                }
                break;
            case 1:
                for (The the : theS) {
                    if (the.getTrangThai() == 1) {
                        tt = "Hoạt Động";
                        defaultTableModel.addRow(new Object[]{the.getSoThe(), service.getTenByMa(the.getMaKhachHang()), the.getNgayBatDau(), the.getNgayKetThuc(), tt});
                    }
                }
                break;
            case 0:
                for (The the : theS) {
                    if (the.getTrangThai() == 0) {
                        tt = "bị khóa";
                        defaultTableModel.addRow(new Object[]{the.getSoThe(), service.getTenByMa(the.getMaKhachHang()), the.getNgayBatDau(), the.getNgayKetThuc(), tt});
                    }
                }
                break;
            case -1:
                for (The the : theS) {
                    if (the.getTrangThai() == -1) {
                        tt = "Hết hạn";
                        defaultTableModel.addRow(new Object[]{the.getSoThe(), service.getTenByMa(the.getMaKhachHang()), the.getNgayBatDau(), the.getNgayKetThuc(), tt});
                    }
                }
                break;
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbPopupMenu = new javax.swing.JPopupMenu();
        openMenuItem_14 = new javax.swing.JMenuItem();
        closeMenuItem_14 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        mo_The_14 = new javax.swing.JButton();
        khoa_The_14 = new javax.swing.JButton();
        loc_14 = new javax.swing.JComboBox<>();
        timKiem_14 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        dataTable_14 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();

        openMenuItem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        openMenuItem_14.setText("Mở Thẻ");
        openMenuItem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu.add(openMenuItem_14);

        closeMenuItem_14.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        closeMenuItem_14.setText("Khóa thẻ");
        closeMenuItem_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeMenuItem_14ActionPerformed(evt);
            }
        });
        tbPopupMenu.add(closeMenuItem_14);

        setBackground(new java.awt.Color(208, 230, 165));
        setPreferredSize(new java.awt.Dimension(1225, 720));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(154, 23, 80));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Quản Lí Thẻ");

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.lightGray, java.awt.Color.lightGray), "Xử lý", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 0, 18))); // NOI18N

        mo_The_14.setBackground(new java.awt.Color(138, 170, 229));
        mo_The_14.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        mo_The_14.setForeground(new java.awt.Color(255, 255, 255));
        mo_The_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/open_the.png"))); // NOI18N
        mo_The_14.setText("Mở");
        mo_The_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mo_The_14ActionPerformed(evt);
            }
        });

        khoa_The_14.setBackground(new java.awt.Color(41, 40, 38));
        khoa_The_14.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        khoa_The_14.setForeground(new java.awt.Color(249, 211, 66));
        khoa_The_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/close_the.png"))); // NOI18N
        khoa_The_14.setText("Khóa");
        khoa_The_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                khoa_The_14ActionPerformed(evt);
            }
        });

        loc_14.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        loc_14.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Hoạt Động", "Bị Khóa", "Hết hạn" }));
        loc_14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loc_14ActionPerformed(evt);
            }
        });

        timKiem_14.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        timKiem_14.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                timKiem_14PropertyChange(evt);
            }
        });
        timKiem_14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timKiem_14KeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(khoa_The_14)
                .addGap(89, 89, 89)
                .addComponent(mo_The_14, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(106, 106, 106)
                .addComponent(loc_14, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(95, 95, 95)
                .addComponent(timKiem_14, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(timKiem_14, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(khoa_The_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mo_The_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(loc_14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 58, Short.MAX_VALUE))
        );

        dataTable_14.setBackground(new java.awt.Color(242, 242, 242));
        dataTable_14.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        dataTable_14.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        dataTable_14.setRowHeight(30);
        dataTable_14.setSelectionBackground(new java.awt.Color(58, 175, 169));
        dataTable_14.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(dataTable_14);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/card-icon.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1225, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 671, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void mo_The_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mo_The_14ActionPerformed
        int row = dataTable_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn thẻ mở", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dataTable_14.getValueAt(row, 4).equals("Hết hạn")) {
                JOptionPane.showMessageDialog(this, "Thẻ này không mở khóa vì hết hạn", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                int check = service.moThe(dataTable_14.getValueAt(row, 0).toString());
                if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Mở thẻ không thành công", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    cards_14 = service.getAllThe();
                    setDataTable(cards_14, trangThai_14);
                }
            }

        }
    }//GEN-LAST:event_mo_The_14ActionPerformed

    private void khoa_The_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_khoa_The_14ActionPerformed
        int row = dataTable_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn thẻ khóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dataTable_14.getValueAt(row, 4).equals("Hết hạn")) {
                JOptionPane.showMessageDialog(this, "Thẻ này không được khóa vì hết hạn", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                int check = service.khoaThe(dataTable_14.getValueAt(row, 0).toString());
                if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Khóa thẻ không thành công", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    cards_14 = service.getAllThe();
                    setDataTable(cards_14, trangThai_14);
                }
            }

        }
    }//GEN-LAST:event_khoa_The_14ActionPerformed

    private void loc_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loc_14ActionPerformed
        // TODO add your handling code here:
        switch (loc_14.getSelectedItem().toString()) {
            case "Hoạt Động":
                trangThai_14 = hd;
                setDataTable(cards_14, trangThai_14);
                break;
            case "Bị Khóa":
                trangThai_14 = biKhoa;
                setDataTable(cards_14, trangThai_14);
                break;
            case "Hết hạn":
                trangThai_14 = hetHan;
                setDataTable(cards_14, trangThai_14);
                break;
            case "Tất cả":
                trangThai_14 = -3;
                setDataTable(cards_14, trangThai_14);
                break;
            default:
                trangThai_14 = -3;
                setDataTable(cards_14, trangThai_14);
                break;
        }
    }//GEN-LAST:event_loc_14ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        System.out.println("1");
        cards_14 = service.getTheByTen(timKiem_14.getText());
        setDataTable(cards_14, trangThai_14);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void timKiem_14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timKiem_14KeyPressed
        // TODO add your handling code here:
       
        cards_14 = service.getTheByTen(timKiem_14.getText());
        setDataTable(cards_14, trangThai_14);
    }//GEN-LAST:event_timKiem_14KeyPressed

    private void timKiem_14PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_timKiem_14PropertyChange

    }//GEN-LAST:event_timKiem_14PropertyChange

    private void openMenuItem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItem_14ActionPerformed
        // TODO add your handling code here:
        int row = dataTable_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn thẻ mở", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dataTable_14.getValueAt(row, 4).equals("Hết hạn")) {
                JOptionPane.showMessageDialog(this, "Thẻ này không mở khóa vì hết hạn", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                int check = service.moThe(dataTable_14.getValueAt(row, 0).toString());
                if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Mở thẻ không thành công", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    cards_14 = service.getAllThe();
                    setDataTable(cards_14, trangThai_14);
                }
            }

        }

    }//GEN-LAST:event_openMenuItem_14ActionPerformed

    private void closeMenuItem_14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeMenuItem_14ActionPerformed
        // TODO add your handling code here
        int row = dataTable_14.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn thẻ khóa", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dataTable_14.getValueAt(row, 4).equals("Hết hạn")) {
                JOptionPane.showMessageDialog(this, "Thẻ này không được khóa vì hết hạn", "Lỗi", JOptionPane.ERROR_MESSAGE);
            } else {
                int check = service.khoaThe(dataTable_14.getValueAt(row, 0).toString());
                if (check == 0) {
                    JOptionPane.showMessageDialog(this, "Khóa thẻ không thành công", "Lỗi", JOptionPane.ERROR_MESSAGE);
                } else {
                    cards_14 = service.getAllThe();
                    setDataTable(cards_14, trangThai_14);
                }
            }

        }

    }//GEN-LAST:event_closeMenuItem_14ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem closeMenuItem_14;
    private javax.swing.JTable dataTable_14;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton khoa_The_14;
    private javax.swing.JComboBox<String> loc_14;
    private javax.swing.JButton mo_The_14;
    private javax.swing.JMenuItem openMenuItem_14;
    private javax.swing.JPopupMenu tbPopupMenu;
    private javax.swing.JTextField timKiem_14;
    // End of variables declaration//GEN-END:variables
}
