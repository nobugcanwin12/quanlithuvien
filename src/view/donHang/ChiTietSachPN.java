package view.donHang;

import java.awt.Color;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ChiTietDonHang;

public class ChiTietSachPN extends javax.swing.JPanel {

    private ChiTietDonHang ctdhGoc16 = null, ctdhThayDoi16 = null;
    boolean trangThaiThayDoi = false;

    public ChiTietSachPN(ChiTietDonHang ctdh) {

        ctdhGoc16 = ctdh;

        initComponents();

        setAll(ctdhGoc16);
    }

    public void setAll(ChiTietDonHang ctdhView16) {
        //do du lieu
        maSachTF16.setText(String.valueOf(ctdhView16.getIdSach()));

        if (ctdhView16.getNgayTra() != null) {
            ngayTraTF16.setText(ctdhView16.getNgayTra());
        }

        NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
        tienMuonTF16.setText(nf.format(ctdhView16.getTienMuon()));
        tienCocTF16.setText(nf.format(ctdhView16.getTienCoc()));
        if (ctdhView16.getTienThanhToan() != 0) {
            tienThanhToanTF16.setText(nf.format(ctdhView16.getTienThanhToan()));
        }

        thoiGianMuonTF16.setText(String.valueOf(ctdhView16.getThoiGianMuon()));

        //set mau cho panel theo trang thai
        //mau xanh: 0 136 122
        //mau vang: 243 210 80
        trangThaiBT16.setVisible(false);
        if (ctdhView16.getNgayTra() == null) {
            contentPanel16.setBackground(new Color(243, 210, 80));
            trangThaiBT16.setVisible(true);
            trangThaiLB16.setText("Đang mượn");
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date ngayHetHan16 = null, ngayTra = null;
            try {
                //set ngay het han
                Calendar c = Calendar.getInstance();
                c.setTime(formatter.parse(ctdhView16.getNgayMuon()));
                c.add(Calendar.DATE, ctdhView16.getThoiGianMuon());
                ngayHetHan16 = c.getTime();

                //set ngay tra
                ngayTra = formatter.parse(ctdhView16.getNgayTra());
            } catch (ParseException ex) {
                Logger.getLogger(ChiTietSachPN.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (ngayHetHan16.compareTo(ngayTra) >= 0) {
                contentPanel16.setBackground(new Color(0, 136, 122));
                trangThaiLB16.setText("Đã trả");
            } else {
                contentPanel16.setBackground(new Color(252, 68, 69));
                trangThaiLB16.setText("Hết hạn");
            }
        }
    }

    public boolean getTrangThaiThayDoi() {
        return trangThaiThayDoi;
    }

    public int getIDSach() {
        return Integer.parseInt(maSachTF16.getText());
    }

    public String getNgayTra() {
        return ngayTraTF16.getText();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        contentPanel16 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        trangThaiBT16 = new javax.swing.JButton();
        thoiGianMuonTF16 = new javax.swing.JLabel();
        tienThanhToanTF16 = new javax.swing.JLabel();
        tienCocTF16 = new javax.swing.JLabel();
        tienMuonTF16 = new javax.swing.JLabel();
        maSachTF16 = new javax.swing.JLabel();
        ngayTraTF16 = new javax.swing.JLabel();
        trangThaiLB16 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(395, 274));

        contentPanel16.setBackground(new java.awt.Color(0, 136, 122));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Ngày trả:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Tiền thanh toán:");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Thời gian mượn:");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Tiền mượn:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Tiền cọc:");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("ID sách:");

        trangThaiBT16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        trangThaiBT16.setText("Chuyển trạng thái thành đã trả");
        trangThaiBT16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trangThaiBT16ActionPerformed(evt);
            }
        });

        thoiGianMuonTF16.setBackground(new java.awt.Color(255, 255, 255));
        thoiGianMuonTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        thoiGianMuonTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        thoiGianMuonTF16.setText("jLabel1");
        thoiGianMuonTF16.setToolTipText("");
        thoiGianMuonTF16.setOpaque(true);

        tienThanhToanTF16.setBackground(new java.awt.Color(255, 255, 255));
        tienThanhToanTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tienThanhToanTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tienThanhToanTF16.setToolTipText("");
        tienThanhToanTF16.setOpaque(true);

        tienCocTF16.setBackground(new java.awt.Color(255, 255, 255));
        tienCocTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tienCocTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tienCocTF16.setText("jLabel1");
        tienCocTF16.setToolTipText("");
        tienCocTF16.setOpaque(true);

        tienMuonTF16.setBackground(new java.awt.Color(255, 255, 255));
        tienMuonTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        tienMuonTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tienMuonTF16.setText("jLabel1");
        tienMuonTF16.setToolTipText("");
        tienMuonTF16.setOpaque(true);

        maSachTF16.setBackground(new java.awt.Color(255, 255, 255));
        maSachTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        maSachTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        maSachTF16.setText("jLabel1");
        maSachTF16.setToolTipText("");
        maSachTF16.setOpaque(true);

        ngayTraTF16.setBackground(new java.awt.Color(255, 255, 255));
        ngayTraTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        ngayTraTF16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ngayTraTF16.setToolTipText("");
        ngayTraTF16.setOpaque(true);

        trangThaiLB16.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        trangThaiLB16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        trangThaiLB16.setText("jLabel1");

        javax.swing.GroupLayout contentPanel16Layout = new javax.swing.GroupLayout(contentPanel16);
        contentPanel16.setLayout(contentPanel16Layout);
        contentPanel16Layout.setHorizontalGroup(
            contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contentPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(contentPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(tienThanhToanTF16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(contentPanel16Layout.createSequentialGroup()
                        .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addGroup(contentPanel16Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel16))))
                        .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(contentPanel16Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(thoiGianMuonTF16, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                                    .addComponent(maSachTF16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(contentPanel16Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ngayTraTF16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tienMuonTF16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tienCocTF16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(contentPanel16Layout.createSequentialGroup()
                        .addComponent(trangThaiBT16)
                        .addGap(18, 18, 18)
                        .addComponent(trangThaiLB16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        contentPanel16Layout.setVerticalGroup(
            contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, contentPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(maSachTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ngayTraTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienMuonTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienCocTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tienThanhToanTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(thoiGianMuonTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(contentPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trangThaiBT16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trangThaiLB16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contentPanel16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contentPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void trangThaiBT16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trangThaiBT16ActionPerformed
        trangThaiThayDoi = true;
        ctdhThayDoi16 = ctdhGoc16;
        ctdhThayDoi16.setNgayTra(String.valueOf(java.time.LocalDate.now()));
        ctdhThayDoi16.setTienThanhToan(ctdhThayDoi16.getTienMuon() * ctdhThayDoi16.getThoiGianMuon());
        setAll(ctdhThayDoi16);
    }//GEN-LAST:event_trangThaiBT16ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contentPanel16;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel maSachTF16;
    private javax.swing.JLabel ngayTraTF16;
    private javax.swing.JLabel thoiGianMuonTF16;
    private javax.swing.JLabel tienCocTF16;
    private javax.swing.JLabel tienMuonTF16;
    private javax.swing.JLabel tienThanhToanTF16;
    private javax.swing.JButton trangThaiBT16;
    private javax.swing.JLabel trangThaiLB16;
    // End of variables declaration//GEN-END:variables
}
