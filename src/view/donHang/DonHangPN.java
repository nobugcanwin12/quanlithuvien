package view.donHang;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.*;
import service.Service_14;
import service.Service_16;

public class DonHangPN extends javax.swing.JPanel {

    Service_16 service16 = null;
    Service_14 service14 = null;
    DefaultTableModel defaultTableModel = null;
    List<ChiTietSachPN> sachTrongDonHangs = null;
    int maDH = 0;

    public DonHangPN() {

        service16 = new Service_16();
        service14 = new Service_14();

        initComponents();

        defaultTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        donHangTB.setModel(defaultTableModel);

        //them cot cho bang don hang
        defaultTableModel.addColumn("Mã đơn hàng");
        defaultTableModel.addColumn("Số thẻ");
        defaultTableModel.addColumn("Nhân viên");
        defaultTableModel.addColumn("Ngày mượn");

        //them du lieu vao bookTB
        showDonHang(service16.getAllDonHang());

        //dong nut luu thay doi
        saveBT16.setVisible(false);

        //load
        loadYearDonHang();

        //check trang thai het han
        int check = service16.checkChiTietDonHang();
        if (check == -1) {
            JOptionPane.showMessageDialog(this, "Có lỗi trong lúc thay đổi trạng thái thành HẾT HẠN, vui lòng kiểm tra và thử lại", "Lỗi", JOptionPane.ERROR_MESSAGE);
        } else if (check == 1) {
            JOptionPane.showMessageDialog(this, "Những đơn hàng HẾT HẠN đã được tự động chuyển trạng thái", "Thông báo", JOptionPane.CLOSED_OPTION);
        }
    }

    final void showDonHang(List<DonHang> donHangs) {
        defaultTableModel.setRowCount(0);
        for (DonHang donHang : donHangs) {
            String nhanVien = service16.getTenNhanVien(donHang.getMaNV());
            defaultTableModel.addRow(new Object[]{donHang.getMaDH(), donHang.getSoThe(),
                nhanVien, donHang.getNgayMuon()});
        }
    }

    public void showSachMuon(int maDH) {
        sachTrongDonHangs = new ArrayList<ChiTietSachPN>();

        List<ChiTietDonHang> chiTietDonHangs = service16.getChiTietDonHang(maDH);

        for (ChiTietDonHang chiTietDonHang : chiTietDonHangs) {
            ChiTietSachPN sach = new ChiTietSachPN(chiTietDonHang);

            sachMuonPN16.add(sach);

            sachTrongDonHangs.add(sach);
        }
        revalidate();
        repaint();
    }

    public void reset() {
        sachMuonPN16.removeAll();
        revalidate();
        repaint();

        saveBT16.setVisible(false);
        jPanel2.setVisible(true);
    }

    public void loadYearDonHang() {
        for (String y : service16.getAllYearDonHanng()) {
            namCBB16.addItem(y);
        }
    }

    public void loadMonthDonHang(String year) {
        thangCBB16.removeAllItems();
        thangCBB16.addItem("Tất cả các tháng");
        for (String m : service16.getMonthDonHangByYear(year)) {
            thangCBB16.addItem(m);
        }
    }
    
    public void search() {
        showDonHang(service16.searchDonHang(searchTF16.getText()));
        reset();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel11 = new javax.swing.JLabel();
        trangThaiCBB16 = new javax.swing.JComboBox<>();
        searchTF16 = new javax.swing.JTextField();
        btnSearch1 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        donHangTB = new javax.swing.JTable();
        sachMuonPN16 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        saveBT16 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        xemSachMuonBT16 = new javax.swing.JButton();
        xemHoaDon16 = new javax.swing.JButton();
        filterBT16 = new javax.swing.JButton();
        namCBB16 = new javax.swing.JComboBox<>();
        thangCBB16 = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(211, 231, 238));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel11.setText("Tìm kiếm:");

        trangThaiCBB16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        trangThaiCBB16.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả", "Đã trả hết sách", "Có sách đang mượn", "Có sách hết hạn" }));

        searchTF16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        searchTF16.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchTF16KeyPressed(evt);
            }
        });

        btnSearch1.setBackground(new java.awt.Color(204, 204, 255));
        btnSearch1.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        btnSearch1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Search-icon.png"))); // NOI18N
        btnSearch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearch1ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel14.setText("Lọc:");

        donHangTB.setBackground(new java.awt.Color(242, 242, 242));
        donHangTB.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        donHangTB.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "2", "3", "4"},
                {"2", "2", "3", "4"}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        donHangTB.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        donHangTB.setRowHeight(30);
        donHangTB.setSelectionBackground(new java.awt.Color(58, 175, 169));
        donHangTB.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        donHangTB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                donHangTBMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(donHangTB);

        sachMuonPN16.setBackground(new java.awt.Color(222, 242, 241));
        sachMuonPN16.setOpaque(false);
        sachMuonPN16.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 0));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(154, 23, 80));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Sách Đã Mượn");

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.CardLayout());

        saveBT16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        saveBT16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Save-as.png"))); // NOI18N
        saveBT16.setText("Lưu thay đổi");
        saveBT16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBT16ActionPerformed(evt);
            }
        });
        jPanel1.add(saveBT16, "card3");

        jPanel2.setOpaque(false);
        jPanel2.setLayout(new java.awt.GridLayout());

        xemSachMuonBT16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        xemSachMuonBT16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/view-detail.png"))); // NOI18N
        xemSachMuonBT16.setText("Xem sách đã mượn");
        xemSachMuonBT16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xemSachMuonBT16ActionPerformed(evt);
            }
        });
        jPanel2.add(xemSachMuonBT16);

        xemHoaDon16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        xemHoaDon16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Receipt.png"))); // NOI18N
        xemHoaDon16.setText("Xem hóa đơn");
        xemHoaDon16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xemHoaDon16ActionPerformed(evt);
            }
        });
        jPanel2.add(xemHoaDon16);

        jPanel1.add(jPanel2, "card4");

        filterBT16.setBackground(new java.awt.Color(204, 204, 255));
        filterBT16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        filterBT16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/filter16.png"))); // NOI18N
        filterBT16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterBT16ActionPerformed(evt);
            }
        });

        namCBB16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        namCBB16.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả các năm" }));
        namCBB16.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                namCBB16ItemStateChanged(evt);
            }
        });

        thangCBB16.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        thangCBB16.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tất cả các tháng" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sachMuonPN16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 1065, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(searchTF16, javax.swing.GroupLayout.DEFAULT_SIZE, 859, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(trangThaiCBB16, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(namCBB16, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(thangCBB16, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(filterBT16, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(80, 80, 80))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(filterBT16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(btnSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(trangThaiCBB16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(namCBB16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(thangCBB16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(searchTF16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(sachMuonPN16, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchTF16KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTF16KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            search();
        }
    }//GEN-LAST:event_searchTF16KeyPressed

    private void btnSearch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearch1ActionPerformed
        search();
    }//GEN-LAST:event_btnSearch1ActionPerformed

    private void saveBT16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBT16ActionPerformed
        int kq = JOptionPane.showConfirmDialog(this, "Bạn có muốn lưu thay đổi không", "Hỏi đáp", JOptionPane.YES_NO_OPTION);
        if (kq == 0) {
            int canUpdate = 0, daUpdate = 0;
            for (ChiTietSachPN cts : sachTrongDonHangs) {
                if (cts.getTrangThaiThayDoi()) {
                    ChiTietDonHang ctdh = new ChiTietDonHang();
                    ctdh.setMaDH(maDH);
                    ctdh.setIdSach(cts.getIDSach());
                    ctdh.setNgayTra(cts.getNgayTra());

                    canUpdate += 1;
                    daUpdate += service16.updateTrangThaiToDaThanhToan(ctdh);
                }
            }
            if (canUpdate * 2 != daUpdate) {
                JOptionPane.showMessageDialog(this, "Có lỗi trong lúc chuyển đổi, vui lòng thử lại", "Lỗi", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_saveBT16ActionPerformed

    private void donHangTBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_donHangTBMouseClicked
        reset();
    }//GEN-LAST:event_donHangTBMouseClicked

    private void xemSachMuonBT16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xemSachMuonBT16ActionPerformed
        sachMuonPN16.removeAll();
        int row = donHangTB.getSelectedRow();
        if (row != -1) {
            maDH = Integer.parseInt(String.valueOf(donHangTB.getValueAt(row, 0)));
            showSachMuon(maDH);
            saveBT16.setVisible(true);
            jPanel2.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn đơn hàng muốn xem", "Lỗi", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_xemSachMuonBT16ActionPerformed

    private void filterBT16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterBT16ActionPerformed
        String ttDH = trangThaiCBB16.getSelectedItem().toString();
        String n = namCBB16.getSelectedItem().toString();
        String t = thangCBB16.getSelectedItem().toString();

        showDonHang(service16.filterDonHang(ttDH, n, t));

        reset();
    }//GEN-LAST:event_filterBT16ActionPerformed

    private void namCBB16ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_namCBB16ItemStateChanged
        if (!namCBB16.getSelectedItem().toString().equals("Tất cả các năm")) {
            loadMonthDonHang(namCBB16.getSelectedItem().toString());
        } else {
            loadMonthDonHang("");
        }
    }//GEN-LAST:event_namCBB16ItemStateChanged

    private void xemHoaDon16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xemHoaDon16ActionPerformed
        int row = donHangTB.getSelectedRow();
        if (row != -1) {
            maDH = Integer.parseInt(String.valueOf(donHangTB.getValueAt(row, 0)));
            service14.inHoaDon(maDH);
        } else {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn đơn hàng muốn xem", "Lỗi", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_xemHoaDon16ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch1;
    private javax.swing.JTable donHangTB;
    private javax.swing.JButton filterBT16;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JComboBox<String> namCBB16;
    private javax.swing.JPanel sachMuonPN16;
    private javax.swing.JButton saveBT16;
    private javax.swing.JTextField searchTF16;
    private javax.swing.JComboBox<String> thangCBB16;
    private javax.swing.JComboBox<String> trangThaiCBB16;
    private javax.swing.JButton xemHoaDon16;
    private javax.swing.JButton xemSachMuonBT16;
    // End of variables declaration//GEN-END:variables
}
