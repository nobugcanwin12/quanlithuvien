package view.thongKe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.BieuDoTron;
import model.DoanhThu;
import model.Sach;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import service.Service_14;
import service.Service_16;

public class ThongKePN extends javax.swing.JPanel {

    Service_14 service_14 = null;
    Service_16 service16 = null;

    public ThongKePN() {

        service_14 = new Service_14();
        service16 = new Service_16();

        initComponents();
        showPieChart1(service_14.bieuDoTron());
        showBarChart(service_14.doanhThu());
        showSachMuonNhieu16();
        widthColumnTableSachMuonNhieu();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        int width = getWidth();
        int height = getHeight();
        Color color1 = new Color(168, 222, 224);
        Color color2 = new Color(249, 226, 174);
        GradientPaint gp = new GradientPaint(0, 0, color1, 180, height, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, width, height);
    }

    public void showSachMuonNhieu16() {
        DefaultTableModel sachMuonNhieu16 = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        sachMuonNhieuTB16.setModel(sachMuonNhieu16);

        //them cot cho sach muon nhieu tb
        sachMuonNhieu16.addColumn("Id");
        sachMuonNhieu16.addColumn("Tên sách");
        sachMuonNhieu16.addColumn("Số lần được mượn");

        //them du lieu cho sach muon nhieu tb
        sachMuonNhieu16.setRowCount(0);
        for (Sach sach : service16.getSachMuonNhieu()) {
            sachMuonNhieu16.addRow(new Object[]{sach.getId(), sach.getTen(), sach.getSoLuongMuon()});
        }
    }
    
    //set do rong cot 
    public void widthColumnTableSachMuonNhieu() {
        TableColumnModel columnModel = sachMuonNhieuTB16.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(80);
        columnModel.getColumn(0).setMaxWidth(80);
        columnModel.getColumn(1).setPreferredWidth(800);
        columnModel.getColumn(1).setMaxWidth(800);
        columnModel.getColumn(2).setPreferredWidth(230);
        columnModel.getColumn(2).setMaxWidth(230);
    }

    public void showPieChart1(List<BieuDoTron> ds) {
        DefaultPieDataset barDataser = new DefaultPieDataset();
        for (BieuDoTron bd_14 : ds) {
            barDataser.setValue(bd_14.getTen(), bd_14.getSoLuong());
        }
//        barDataser.setValue("Sách đang rảnh", (20));
//        barDataser.setValue("Sách đang mượn", (30));
//        barDataser.setValue("Sách đang bào trì", (40));

        JFreeChart piechart = ChartFactory.createPieChart("Biểu đồ thống kê sách ", barDataser, true, true, true);
        PiePlot piePlot = (PiePlot) piechart.getPlot();

//        piePlot.setSectionPaint("IP5", new Color(255,255,102));
//        piePlot.setSectionPaint("IP1", new Color(102,255,102));
//        piePlot.setSectionPaint("IP3", new Color(255,102,153));
        piePlot.setBackgroundPaint(Color.white);

        ChartPanel barChartPanel = new ChartPanel(piechart);
        
        bieuDoTron_14.removeAll();
        barChartPanel.setEnabled(false);
        bieuDoTron_14.add(barChartPanel, BorderLayout.CENTER);
        bieuDoTron_14.validate();
    }

    public void showBarChart(List<DoanhThu> doanhThu) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//        doanhThu.remove(doanhThu.get(0));
        for (DoanhThu dt : doanhThu) {
            dataset.setValue(dt.getDoanhThu(), "Doanh thu", dt.getThang());
        }

//        dataset.setValue(200, "Amount", "1/2");
//        dataset.setValue(200, "Amount", "thang 2");
//        dataset.setValue(800, "Amount", "thang 3");
//        dataset.setValue(200, "Amount", "thang 4");
//        dataset.setValue(200, "Amount", "thang 5");
//        dataset.setValue(200, "Amount", "thang 6");
//        dataset.setValue(200, "Amount", "thang 8");
//        dataset.setValue(200, "Amount", "thang 9");
//        dataset.setValue(200, "Amount", "thang 10");
//        dataset.setValue(200, "Amount", "thang 11");
//        dataset.setValue(200, "Amount", "thang 12");
        JFreeChart chart = ChartFactory.createBarChart("Biểu đồ doanh thu theo từng tháng", "Tháng", "Doanh thu", dataset, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot categoryPlot = chart.getCategoryPlot();
        categoryPlot.setBackgroundPaint(Color.white);
        BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
        Color clr3 = new Color(204, 0, 51);
        renderer.setSeriesPaint(0, clr3);

        ChartPanel barpChartPanel1 = new ChartPanel(chart);
        barpChartPanel1.setOpaque(false);
        bieuDoDoanhThu_14.removeAll();

        bieuDoDoanhThu_14.add(barpChartPanel1, BorderLayout.CENTER);
        bieuDoDoanhThu_14.validate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        sachMuonNhieuTB16 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        bieuDoDoanhThu_14 = new javax.swing.JPanel();
        bieuDoTron_14 = new javax.swing.JPanel();

        setBackground(new java.awt.Color(74, 222, 222));
        setPreferredSize(new java.awt.Dimension(1225, 720));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Segoe UI", 3, 20)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 102));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DANH SÁCH CÁC SÁCH MƯỢN NHIỀU");
        jLabel1.setOpaque(true);

        sachMuonNhieuTB16.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        sachMuonNhieuTB16.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        sachMuonNhieuTB16.setRowHeight(30);
        sachMuonNhieuTB16.setSelectionBackground(new java.awt.Color(58, 175, 169));
        sachMuonNhieuTB16.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(sachMuonNhieuTB16);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setOpaque(false);
        jPanel2.setLayout(new java.awt.GridLayout(0, 1, 0, 5));

        bieuDoDoanhThu_14.setBackground(new java.awt.Color(102, 255, 102));
        bieuDoDoanhThu_14.setLayout(new java.awt.BorderLayout());
        jPanel2.add(bieuDoDoanhThu_14);

        bieuDoTron_14.setBackground(new java.awt.Color(102, 255, 102));
        bieuDoTron_14.setFocusable(false);
        bieuDoTron_14.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        bieuDoTron_14.setLayout(new java.awt.BorderLayout());
        jPanel2.add(bieuDoTron_14);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 645, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bieuDoDoanhThu_14;
    private javax.swing.JPanel bieuDoTron_14;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable sachMuonNhieuTB16;
    // End of variables declaration//GEN-END:variables
}
