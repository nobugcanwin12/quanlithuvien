package view;

import java.awt.Color;
import javax.swing.JPanel;
import model.NguoiDung;
import service.Service_14;
import view.donHang.*;
import view.muonSach.*;
import view.khachHang.*;
import view.nhanVien.*;
import view.sach.*;
import view.tacGia.*;
import view.taiKhoan.*;
import view.the.*;
import view.theLoai.*;
import view.thongKe.*;

public class Home extends javax.swing.JFrame {

    JPanel contentPN;
    NguoiDung user = null;
    Service_14 service_14 = null;

    public Home(NguoiDung user, int page) {
        service_14 = new Service_14();
        this.user = user;

        initComponents();

        service_14.setTrangThaiHetHan();

        setLocationRelativeTo(null);

        showPage(page);

        if (user.getMaCV() == 1) {
            sideBar.remove(lblQLSach);
            sideBar.remove(lblQLNhanVien);
            sideBar.remove(lblThongKe);
            sideBar.remove(lblTacGia);
            sideBar.remove(lblTheLoai);
        }
    }

    public void showPage(int page) {
        contentPanel.removeAll();
        hiddenPN();
        switch (page) {
            case 1:
                lblmuonSach.setBackground(new Color(202, 250, 254));
                contentPN = new MuonSachPN(user);
                break;
            case 2:
                lblKhachHang.setBackground(new Color(202, 250, 254));
                contentPN = new KhachHangPN();
                break;
            case 3:
                lblThe1.setBackground(new Color(202, 250, 254));
                contentPN = new ThePN();
                break;
            case 4:
                lblQLSach.setBackground(new Color(202, 250, 254));
                contentPN = new SachPN();
                break;
            case 5:
                lblTacGia.setBackground(new Color(202, 250, 254));
                contentPN = new TacGiaPN();
                break;
            case 6:
                lblTheLoai.setBackground(new Color(202, 250, 254));
                contentPN = new TheLoaiPN();
                break;
            case 7:
                lblQLDonHang.setBackground(new Color(202, 250, 254));
                contentPN = new DonHangPN();
                break;
            case 8:
                lblQLNhanVien.setBackground(new Color(202, 250, 254));
                contentPN = new NhanVienPN();
                break;
            case 9:
                lblThongKe.setBackground(new Color(202, 250, 254));
                contentPN = new ThongKePN();
                break;
            case 10:
                lblTK.setBackground(new Color(202, 250, 254));
                contentPN = new TaiKhoanPN(this, user);
                break;
        }
        contentPanel.add(contentPN);
        pack();
    }

    public void hiddenPN() {
        lblmuonSach.setBackground(new Color(255, 255, 255));
        lblThe1.setBackground(new Color(255, 255, 255));
        lblKhachHang.setBackground(new Color(255, 255, 255));
        lblQLSach.setBackground(new Color(255, 255, 255));
        lblTK.setBackground(new Color(255, 255, 255));
        lblThongKe.setBackground(new Color(255, 255, 255));
        lblQLNhanVien.setBackground(new Color(255, 255, 255));
        lblQLDonHang.setBackground(new Color(255, 255, 255));
        lblTacGia.setBackground(new Color(255, 255, 255));
        lblTheLoai.setBackground(new Color(255, 255, 255));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel11 = new javax.swing.JPanel();
        lblThe = new javax.swing.JLabel();
        sideBar = new javax.swing.JPanel();
        lblmuonSach = new javax.swing.JLabel();
        lblKhachHang = new javax.swing.JLabel();
        lblThe1 = new javax.swing.JLabel();
        lblQLSach = new javax.swing.JLabel();
        lblTacGia = new javax.swing.JLabel();
        lblTheLoai = new javax.swing.JLabel();
        lblQLDonHang = new javax.swing.JLabel();
        lblQLNhanVien = new javax.swing.JLabel();
        lblThongKe = new javax.swing.JLabel();
        lblTK = new javax.swing.JLabel();
        contentPanel = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        lblThe.setBackground(new java.awt.Color(255, 255, 255));
        lblThe.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblThe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThe.setText("Quản lý thẻ");
        lblThe.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblThe.setOpaque(true);
        lblThe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTheMouseClicked(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Trang chủ");
        setResizable(false);

        sideBar.setLayout(new java.awt.GridLayout(0, 1));

        lblmuonSach.setBackground(new java.awt.Color(236, 236, 236));
        lblmuonSach.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblmuonSach.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblmuonSach.setText("Trang mượn sách");
        lblmuonSach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblmuonSach.setOpaque(true);
        lblmuonSach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblmuonSachMouseClicked(evt);
            }
        });
        sideBar.add(lblmuonSach);

        lblKhachHang.setBackground(new java.awt.Color(236, 236, 236));
        lblKhachHang.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblKhachHang.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblKhachHang.setText("Quản lý khách hàng");
        lblKhachHang.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblKhachHang.setOpaque(true);
        lblKhachHang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblKhachHangMouseClicked(evt);
            }
        });
        sideBar.add(lblKhachHang);

        lblThe1.setBackground(new java.awt.Color(236, 236, 236));
        lblThe1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblThe1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThe1.setText("Quản lý Thẻ");
        lblThe1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblThe1.setOpaque(true);
        lblThe1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblThe1MouseClicked(evt);
            }
        });
        sideBar.add(lblThe1);

        lblQLSach.setBackground(new java.awt.Color(236, 236, 236));
        lblQLSach.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLSach.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLSach.setText("Quản lý sách");
        lblQLSach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLSach.setOpaque(true);
        lblQLSach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLSachMouseClicked(evt);
            }
        });
        sideBar.add(lblQLSach);

        lblTacGia.setBackground(new java.awt.Color(236, 236, 236));
        lblTacGia.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblTacGia.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTacGia.setText("Quản lý tác giả");
        lblTacGia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblTacGia.setOpaque(true);
        lblTacGia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTacGiaMouseClicked(evt);
            }
        });
        sideBar.add(lblTacGia);

        lblTheLoai.setBackground(new java.awt.Color(236, 236, 236));
        lblTheLoai.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblTheLoai.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTheLoai.setText("Quản lý thể loại");
        lblTheLoai.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblTheLoai.setOpaque(true);
        lblTheLoai.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTheLoaiMouseClicked(evt);
            }
        });
        sideBar.add(lblTheLoai);

        lblQLDonHang.setBackground(new java.awt.Color(236, 236, 236));
        lblQLDonHang.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLDonHang.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLDonHang.setText("Quản lý mượn trả");
        lblQLDonHang.setToolTipText("");
        lblQLDonHang.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLDonHang.setOpaque(true);
        lblQLDonHang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLDonHangMouseClicked(evt);
            }
        });
        sideBar.add(lblQLDonHang);

        lblQLNhanVien.setBackground(new java.awt.Color(236, 236, 236));
        lblQLNhanVien.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblQLNhanVien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQLNhanVien.setText("Quản lý nhân viên");
        lblQLNhanVien.setToolTipText("");
        lblQLNhanVien.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQLNhanVien.setOpaque(true);
        lblQLNhanVien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQLNhanVienMouseClicked(evt);
            }
        });
        sideBar.add(lblQLNhanVien);

        lblThongKe.setBackground(new java.awt.Color(236, 236, 236));
        lblThongKe.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblThongKe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThongKe.setText("Thống kê");
        lblThongKe.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblThongKe.setOpaque(true);
        lblThongKe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblThongKeMouseClicked(evt);
            }
        });
        sideBar.add(lblThongKe);

        lblTK.setBackground(new java.awt.Color(236, 236, 236));
        lblTK.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lblTK.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTK.setText("Tài khoản");
        lblTK.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblTK.setOpaque(true);
        lblTK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTKMouseClicked(evt);
            }
        });
        sideBar.add(lblTK);

        contentPanel.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(sideBar, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sideBar, javax.swing.GroupLayout.DEFAULT_SIZE, 720, Short.MAX_VALUE)
                    .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        setSize(new java.awt.Dimension(1419, 757));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lblmuonSachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblmuonSachMouseClicked
        showPage(1);
    }//GEN-LAST:event_lblmuonSachMouseClicked

    private void lblTheMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTheMouseClicked
        showPage(3);
    }//GEN-LAST:event_lblTheMouseClicked

    private void lblKhachHangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblKhachHangMouseClicked
        showPage(2);
    }//GEN-LAST:event_lblKhachHangMouseClicked

    private void lblQLSachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLSachMouseClicked
        showPage(4);
    }//GEN-LAST:event_lblQLSachMouseClicked

    private void lblTKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTKMouseClicked
        showPage(10);
    }//GEN-LAST:event_lblTKMouseClicked

    private void lblQLNhanVienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLNhanVienMouseClicked
        showPage(8);
    }//GEN-LAST:event_lblQLNhanVienMouseClicked

    private void lblThongKeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblThongKeMouseClicked
        showPage(9);
    }//GEN-LAST:event_lblThongKeMouseClicked

    private void lblQLDonHangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQLDonHangMouseClicked
        showPage(7);
    }//GEN-LAST:event_lblQLDonHangMouseClicked

    private void lblTacGiaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTacGiaMouseClicked
        showPage(5);
    }//GEN-LAST:event_lblTacGiaMouseClicked

    private void lblTheLoaiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTheLoaiMouseClicked
        showPage(6);
    }//GEN-LAST:event_lblTheLoaiMouseClicked

    private void lblThe1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblThe1MouseClicked
        // TODO add your handling code here:
        showPage(3);
    }//GEN-LAST:event_lblThe1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contentPanel;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JLabel lblKhachHang;
    private javax.swing.JLabel lblQLDonHang;
    private javax.swing.JLabel lblQLNhanVien;
    private javax.swing.JLabel lblQLSach;
    private javax.swing.JLabel lblTK;
    private javax.swing.JLabel lblTacGia;
    private javax.swing.JLabel lblThe;
    private javax.swing.JLabel lblThe1;
    private javax.swing.JLabel lblTheLoai;
    private javax.swing.JLabel lblThongKe;
    private javax.swing.JLabel lblmuonSach;
    private javax.swing.JPanel sideBar;
    // End of variables declaration//GEN-END:variables
}
