package model;

public class Sach {

    private int id;
    private String ten;
    private int maTacGia;
    private int maTheLoai;
    private int maNXB;
    private int namXB;
    private double tienMuon;
    private double tienCoc;
    private int trangThai;
    //so luong muon dung de phuc vu thong ke
    private int soLuongMuon;

    public Sach() {
    }

    public Sach(int id, String ten, int maTacGia, int maTheLoai, int maNXB, int namXB, double tienMuon, double tienCoc, int trangThai, int soLuongMuon) {
        this.id = id;
        this.ten = ten;
        this.maTacGia = maTacGia;
        this.maTheLoai = maTheLoai;
        this.maNXB = maNXB;
        this.namXB = namXB;
        this.tienMuon = tienMuon;
        this.tienCoc = tienCoc;
        this.trangThai = trangThai;
        this.soLuongMuon = soLuongMuon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getMaTacGia() {
        return maTacGia;
    }

    public void setMaTacGia(int maTacGia) {
        this.maTacGia = maTacGia;
    }

    public int getMaTheLoai() {
        return maTheLoai;
    }

    public void setMaTheLoai(int maTheLoai) {
        this.maTheLoai = maTheLoai;
    }

    public int getMaNXB() {
        return maNXB;
    }

    public void setMaNXB(int maNXB) {
        this.maNXB = maNXB;
    }

    public int getNamXB() {
        return namXB;
    }

    public void setNamXB(int namXB) {
        this.namXB = namXB;
    }

    public double getTienMuon() {
        return tienMuon;
    }

    public void setTienMuon(double tienMuon) {
        this.tienMuon = tienMuon;
    }

    public double getTienCoc() {
        return tienCoc;
    }

    public void setTienCoc(double tienCoc) {
        this.tienCoc = tienCoc;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    public int getSoLuongMuon() {
        return soLuongMuon;
    }

    public void setSoLuongMuon(int soLuongMuon) {
        this.soLuongMuon = soLuongMuon;
    }

    @Override
    public String toString() {
        return "Sach{" + "id=" + id + ", ten=" + ten + ", maTacGia=" + maTacGia + ", maTheLoai=" + maTheLoai + ", maNXB=" + maNXB + ", namXB=" + namXB + ", tienMuon=" + tienMuon + ", tienCoc=" + tienCoc + ", trangThai=" + trangThai + ", soLuongMuon=" + soLuongMuon + '}';
    }
}
