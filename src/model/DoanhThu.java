package model;

public class DoanhThu {

    String thang;
    double doanhThu;

    public DoanhThu() {
    }

    public DoanhThu(String thang, double doanhThu) {
        this.thang = thang;
        this.doanhThu = doanhThu;
    }

    public String getThang() {
        return thang;
    }

    public void setThang(String thang) {
        this.thang = thang;
    }

    public double getDoanhThu() {
        return doanhThu;
    }

    public void setDoanhThu(double doanhThu) {
        this.doanhThu = doanhThu;
    }

    @Override
    public String toString() {
        return "DanhThu{" + "thang=" + thang + ", doanhThu=" + doanhThu + '}';
    }
}
