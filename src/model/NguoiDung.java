package model;

//nguoi dung la nhan vien hoac chu cua hang
public class NguoiDung {

    private int maND;
    private String tenND;
    private int maCV;
    private String taiKhoan;
    private String matKhau;
    private String gioiTinh;
    private String ngaySinh;
    private String sdt;
    private int trangThai;

    public NguoiDung() {
    }

    public NguoiDung(int maND, String tenND, int maCV, String taiKhoan, String matKhau, String gioiTinh, String ngaySinh, String sdt, int trangThai) {
        this.maND = maND;
        this.tenND = tenND;
        this.maCV = maCV;
        this.taiKhoan = taiKhoan;
        this.matKhau = matKhau;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.sdt = sdt;
        this.trangThai = trangThai;
    }

    public int getMaND() {
        return maND;
    }

    public void setMaND(int maND) {
        this.maND = maND;
    }

    public String getTenND() {
        return tenND;
    }

    public void setTenND(String tenND) {
        this.tenND = tenND;
    }

    public int getMaCV() {
        return maCV;
    }

    public void setMaCV(int maCV) {
        this.maCV = maCV;
    }

    public String getTaiKhoan() {
        return taiKhoan;
    }

    public void setTaiKhoan(String taiKhoan) {
        this.taiKhoan = taiKhoan;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    @Override
    public String toString() {
        return "NguoiDung{" + "maND=" + maND + ", tenND=" + tenND + ", maCV=" + maCV + ", taiKhoan=" + taiKhoan + ", matKhau=" + matKhau + ", gioiTinh=" + gioiTinh + ", ngaySinh=" + ngaySinh + ", sdt=" + sdt + ", trangThai=" + trangThai + '}';
    }
}
