package model;

public class DonHang {

    private int maDH;
    private String soThe;
    private int maNV;
    private String ngayMuon;

    public DonHang() {
    }

    public DonHang(int maDH, String soThe, int maNV, String ngayMuon) {
        this.maDH = maDH;
        this.soThe = soThe;
        this.maNV = maNV;
        this.ngayMuon = ngayMuon;
    }

    public int getMaDH() {
        return maDH;
    }

    public void setMaDH(int maDH) {
        this.maDH = maDH;
    }

    public String getSoThe() {
        return soThe;
    }

    public void setSoThe(String soThe) {
        this.soThe = soThe;
    }

    public int getMaNV() {
        return maNV;
    }

    public void setMaNV(int maNV) {
        this.maNV = maNV;
    }

    public String getNgayMuon() {
        return ngayMuon;
    }

    public void setNgayMuon(String ngayMuon) {
        this.ngayMuon = ngayMuon;
    }
}
