package model;

public class ChiTietDonHang {

    private int maDH;
    private int idSach;
    private String ngayMuon;
    private String ngayTra;
    private double tienCoc;
    private double tienMuon;
    private double tienThanhToan;
    private int thoiGianMuon;

    public ChiTietDonHang() {
    }

    public ChiTietDonHang(int maDH, int idSach, String ngayMuon, String ngayTra, double tienCoc, double tienMuon, double tienThanhToan, int thoiGianMuon) {
        this.maDH = maDH;
        this.idSach = idSach;
        this.ngayMuon = ngayMuon;
        this.ngayTra = ngayTra;
        this.tienCoc = tienCoc;
        this.tienMuon = tienMuon;
        this.tienThanhToan = tienThanhToan;
        this.thoiGianMuon = thoiGianMuon;
    }

    public int getMaDH() {
        return maDH;
    }

    public void setMaDH(int maDH) {
        this.maDH = maDH;
    }

    public int getIdSach() {
        return idSach;
    }

    public void setIdSach(int idSach) {
        this.idSach = idSach;
    }

    public String getNgayMuon() {
        return ngayMuon;
    }

    public void setNgayMuon(String ngayMuon) {
        this.ngayMuon = ngayMuon;
    }

    public String getNgayTra() {
        return ngayTra;
    }

    public void setNgayTra(String ngayTra) {
        this.ngayTra = ngayTra;
    }

    public double getTienCoc() {
        return tienCoc;
    }

    public void setTienCoc(double tienCoc) {
        this.tienCoc = tienCoc;
    }

    public double getTienMuon() {
        return tienMuon;
    }

    public void setTienMuon(double tienMuon) {
        this.tienMuon = tienMuon;
    }

    public double getTienThanhToan() {
        return tienThanhToan;
    }

    public void setTienThanhToan(double tienThanhToan) {
        this.tienThanhToan = tienThanhToan;
    }

    public int getThoiGianMuon() {
        return thoiGianMuon;
    }

    public void setThoiGianMuon(int thoiGianMuon) {
        this.thoiGianMuon = thoiGianMuon;
    }
}
