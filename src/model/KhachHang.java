package model;

public class KhachHang {

    private int maDocGia;
    private String tenDocGia;
    private String diaChi;
    private String sdt;

    public KhachHang() {
    }

    public KhachHang(int maDocGia, String tenDocGia, String diaChi, String sdt) {
        this.maDocGia = maDocGia;
        this.tenDocGia = tenDocGia;
        this.diaChi = diaChi;
        this.sdt = sdt;
    }

    public KhachHang(String tenDocGia, String diaChi, String sdt) {
        this.tenDocGia = tenDocGia;
        this.diaChi = diaChi;
        this.sdt = sdt;
    }

    public int getMaDocGia() {
        return maDocGia;
    }

    public void setMaDocGia(int maDocGia) {
        this.maDocGia = maDocGia;
    }

    public String getTenDocGia() {
        return tenDocGia;
    }

    public void setTenDocGia(String tenDocGia) {
        this.tenDocGia = tenDocGia;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }
}
