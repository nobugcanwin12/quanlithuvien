package model;

public class BieuDoTron {

    double soLuong;
    String ten;

    public BieuDoTron() {
    }

    public double getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(double soLuong) {
        this.soLuong = soLuong;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Override
    public String toString() {
        return "BieuDoTron{" + "soLuong=" + soLuong + ", ten=" + ten + '}';
    }

}
