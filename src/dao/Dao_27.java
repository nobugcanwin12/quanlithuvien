package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.NguoiDung;

public class Dao_27 {

    public NguoiDung login(String username, String password) throws SQLException {
        NguoiDung user = new NguoiDung();
        Connection conn = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * FROM nguoi_dung where tai_khoan = ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, username);
        ResultSet rs = pstm.executeQuery();
        if (rs.next()) {
            user.setTaiKhoan(username);
            if (rs.getString("mat_khau").equals(password)) {
                user.setTenND(rs.getString("tenND"));
            } else {
                user.setTaiKhoan(null);
            }
        } else {
            return null;
        }
        return user;
    }

    public List<NguoiDung> getAllND() {
        List<NguoiDung> nd = new ArrayList<NguoiDung>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * "
                + "FROM nguoi_dung "
                + "WHERE ma_nd != 1";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                NguoiDung nguoidung = new NguoiDung();

                nguoidung.setMaND(rs.getInt("ma_nd"));
                nguoidung.setTenND(rs.getString("ten_nd"));
                nguoidung.setMaCV(rs.getInt("ma_chuc_vu"));
                nguoidung.setTaiKhoan(rs.getString("tai_khoan"));
                nguoidung.setMatKhau(rs.getString("mat_khau"));
                nguoidung.setGioiTinh(rs.getString("gioi_tinh"));
                nguoidung.setNgaySinh(rs.getString("ngay_sinh"));

                nguoidung.setSdt(rs.getString("sdt"));
                nguoidung.setTrangThai(rs.getInt("trang_thai"));

                nd.add(nguoidung);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_27.class.getName()).log(Level.SEVERE, null, ex);
        }

        return nd;
    }

    public List<NguoiDung> getUserByTitle(String tenND) throws SQLException {
        List<NguoiDung> nd = new ArrayList<NguoiDung>();
        Connection con = JDBCConnection.getJDBCConnection();
        Statement stm = con.createStatement();
        String sql = "SELECT * "
                + "FROM nguoi_dung "
                + "WHERE ma_nd != 1 and (ten_nd like '%" + tenND + "%' or ma_nd like '%" + tenND + "%')";
        ResultSet rs = stm.executeQuery(sql);

        while (rs.next()) {
            NguoiDung nguoidung = new NguoiDung();
            nguoidung.setMaND(rs.getInt("ma_nd"));
            nguoidung.setTenND(rs.getString("ten_nd"));
            nguoidung.setMaCV(rs.getInt("ma_chuc_vu"));
            nguoidung.setTaiKhoan(rs.getString("tai_khoan"));
            nguoidung.setMatKhau(rs.getString("mat_khau"));
            nguoidung.setGioiTinh(rs.getString("gioi_tinh"));
            nguoidung.setNgaySinh(rs.getString("ngay_sinh"));
            nguoidung.setSdt(rs.getString("sdt"));
            nguoidung.setTrangThai(rs.getInt("trang_thai"));

            nd.add(nguoidung);

        }
        con.close();
        return nd;
    }

    public int addUser(NguoiDung nguoidung) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "INSERT INTO nguoi_dung (ten_nd,tai_khoan,mat_khau,ngay_sinh,gioi_tinh,sdt,trang_thai) "
                + "VALUES(?,?,?,?,?,?,?)";

        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, nguoidung.getTenND());
            psmt.setString(2, nguoidung.getTaiKhoan());
            psmt.setString(3, nguoidung.getMatKhau());
            psmt.setString(4, nguoidung.getNgaySinh());
            psmt.setString(5, nguoidung.getGioiTinh());
            psmt.setString(6, nguoidung.getSdt());
            psmt.setInt(7, nguoidung.getTrangThai());
            int rs = psmt.executeUpdate();
            con.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_27.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int updateUser(NguoiDung nguoidung) {
        int rs_16 = 0;
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE nguoi_dung "
                + "SET ten_nd=?, tai_khoan=?, mat_khau=?, ngay_sinh=?, sdt=?, trang_thai=?, gioi_tinh=? "
                + "WHERE ma_nd =?";

        try {
            PreparedStatement psmt = con.prepareStatement(sql);

            psmt.setString(1, nguoidung.getTenND());
            psmt.setString(2, nguoidung.getTaiKhoan());
            psmt.setString(3, nguoidung.getMatKhau());
            psmt.setString(4, nguoidung.getNgaySinh());
            psmt.setString(5, nguoidung.getSdt());
            psmt.setInt(6, nguoidung.getTrangThai());
            psmt.setString(7, nguoidung.getGioiTinh());
            psmt.setInt(8, nguoidung.getMaND());

            rs_16 = psmt.executeUpdate();

            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_27.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs_16;
    }

    public int deleteUser(int ma_nd) {
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "Delete from nguoi_dung where ma_nd =?";

        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setInt(1, ma_nd);
            int rs = psmt.executeUpdate();
            con.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_27.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int changepass(int maND16, String passnew16) {
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE "
                + "nguoi_dung SET mat_khau=? "
                + "WHERE ma_nd =?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, passnew16);
            pstm.setInt(2, maND16);

            int rs = pstm.executeUpdate();

            con.close();

            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_27.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
