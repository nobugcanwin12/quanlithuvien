package dao;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;

public class Dao_16 {

    public List<String> getAllTheLoai() {
        List<String> theLoais = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_the_loai FROM the_loai";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String theloai = rs.getString("ten_the_loai");
                theLoais.add(theloai);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return theLoais;
    }

    public List<String> getAllTrangThai() {
        List<String> trangThais = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_tt FROM trang_thai_sach";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String trangThai = rs.getString("ten_tt");
                trangThais.add(trangThai);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return trangThais;
    }

    public List<String> getAllTacGia() {
        List<String> tenTacGias = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_tac_gia FROM tac_gia";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String tacGia = rs.getString("ten_tac_gia");
                tenTacGias.add(tacGia);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tenTacGias;
    }

    public String getTrangThaiByMaTrangThai(int mtt) {
        String tenTrangThai = "";

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_tt FROM trang_thai_sach WHERE ma_tt = ?";

        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, mtt);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tenTrangThai = rs.getString("ten_tt");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tenTrangThai;
    }

    public List<DonHang> getAllDonHang() {
        List<DonHang> donHangs = new ArrayList<DonHang>();

        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * "
                + "FROM muon_tra "
                + "ORDER BY ma_mtr desc ";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                DonHang donHang = new DonHang();

                donHang.setMaDH(rs.getInt("ma_mtr"));
                donHang.setSoThe(rs.getString("so_the"));
                donHang.setMaNV(rs.getInt("ma_nv"));
                donHang.setNgayMuon(rs.getString("ngay_muon"));

                donHangs.add(donHang);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return donHangs;
    }

    public String getTenNhanVien(int maNV) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT ten_nd FROM nguoi_dung where ma_nd = ?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, maNV);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            return rs.getString("ten_nd");
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "";
    }

    public List<ChiTietDonHang> getChiTietDonHang(int maDH) {
        List<ChiTietDonHang> chiTietDonHangs = new ArrayList<ChiTietDonHang>();

        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT chi_tiet_muon_tra.*, ngay_muon "
                + "FROM chi_tiet_muon_tra, muon_tra "
                + "WHERE chi_tiet_muon_tra.ma_mtr = ? and chi_tiet_muon_tra.ma_mtr = muon_tra.ma_mtr ";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, maDH);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                ChiTietDonHang chiTietDonHang = new ChiTietDonHang();

                chiTietDonHang.setMaDH(rs.getInt("ma_mtr"));
                chiTietDonHang.setIdSach(rs.getInt("id_sach"));
                chiTietDonHang.setNgayMuon(rs.getString("ngay_muon"));
                chiTietDonHang.setNgayTra(rs.getString("ngay_tra"));
                chiTietDonHang.setTienMuon(rs.getDouble("tien_muon"));
                chiTietDonHang.setTienCoc(rs.getDouble("tien_coc"));
                chiTietDonHang.setTienThanhToan(rs.getDouble("tien_thanh_toan"));
                chiTietDonHang.setThoiGianMuon(rs.getInt("thoi_gian_muon"));

                chiTietDonHangs.add(chiTietDonHang);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return chiTietDonHangs;
    }

    public Double getTienCocByID(int idSach) {
        Double tienCoc = 0.0;
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT tien_coc FROM sach where id_sach = ?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, idSach);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tienCoc = rs.getDouble("tien_coc");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tienCoc;
    }

    public Double getTienMuonByID(int idSach) {
        Double tienMuon = 0.0;
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT tien_muon"
                + " FROM  sach"
                + " where id_sach = ?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, idSach);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tienMuon = rs.getDouble("tien_muon");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tienMuon;
    }

    public int getMaTrangThai(String trangThai) {
        int tt = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ma_tt "
                + "FROM trang_thai_sach "
                + "WHERE ten_tt = ?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, trangThai);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tt = rs.getInt("ma_tt");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tt;
    }

    public int getMaTacGia(String tacGia) {
        int tg = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ma_tac_gia "
                + "FROM tac_gia "
                + "WHERE ten_tac_gia = ?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, tacGia);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tg = rs.getInt("ma_tac_gia");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tg;
    }

    public int getMaTheLoai(String theLoai) {
        int tl = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ma_the_loai "
                + "FROM the_loai "
                + "WHERE ten_the_loai = ?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, theLoai);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tl = rs.getInt("ma_the_loai");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tl;
    }

    public List<Sach> filterSach(String tThai, String tGia, String tLoai) {
        boolean where = false;
        List<Sach> sachs = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * "
                + "FROM sach ";

        if (!tThai.equals("Tất cả sách")) {
            sql += "WHERE trang_thai = " + getMaTrangThai(tThai);
            where = true;
        }
        if (!tGia.equals("Tất cả tác giả")) {
            if (!where) {
                sql += " WHERE ma_tac_gia = " + getMaTacGia(tGia);
                where = true;
            } else {
                sql += " and ma_tac_gia = " + getMaTacGia(tGia);
            }
        }
        if (!tLoai.equals("Tất cả thể loại")) {
            if (!where) {
                sql += " WHERE ma_the_loai = " + getMaTheLoai(tLoai);
            } else {
                sql += " and ma_the_loai = " + getMaTheLoai(tLoai);
            }
        }

        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Sach sach = new Sach();

                sach.setId(rs.getInt("id_sach"));
                sach.setTen(rs.getString("ten_sach"));
                sach.setMaTacGia(rs.getInt("ma_tac_gia"));
                sach.setMaTheLoai(rs.getInt("ma_the_loai"));
                sach.setMaNXB(rs.getInt("ma_nxb"));
                sach.setNamXB(rs.getInt("nam_xuat_ban"));
                sach.setTienMuon(rs.getDouble("tien_muon"));
                sach.setTienCoc(rs.getDouble("tien_coc"));
                sach.setTrangThai(rs.getInt("trang_thai"));

                sachs.add(sach);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sachs;
    }

    public int checkTheMuonSach(String maThe) {

        //0: the dang muon sach
        //1: the dang khong muon sach
        int rt = 1;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "select * "
                + "FROM chi_tiet_muon_tra, muon_tra "
                + "WHERE chi_tiet_muon_tra.ma_mtr = muon_tra.ma_mtr "
                + "AND tien_thanh_toan is null "
                + "AND so_the = ?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, maThe);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                rt = 0;
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rt;
    }

    public int checkThe(String maThe) {

        //0: khong co ma the
        //1: the nay dang muon sach
        //2: the bi khoa
        //3: the het han
        //4: thanh cong
        int rt = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * "
                + "FROM the "
                + "WHERE so_the = ?";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, maThe);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                if (checkTheMuonSach(maThe) == 0) {
                    rt = 1;
                } else if (rs.getInt("trang_thai_the") == 0) {
                    rt = 2;
                } else if (rs.getInt("trang_thai_the") == -1) {
                    rt = 3;
                } else {
                    rt = 4;
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rt;
    }

    public int getMaDHNew() {
        int maDHMax = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT max(ma_mtr) "
                + "FROM muon_tra ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            maDHMax = rs.getInt("max(ma_mtr)");
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return maDHMax;
    }

    public int addDonHang(DonHang donHang) {
        int maDH = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "INSERT INTO muon_tra(so_the, ma_nv, ngay_muon) "
                + "VALUES(?, ?, CURRENT_DATE()) ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, donHang.getSoThe());
            pstm.setInt(2, donHang.getMaNV());

            int rs = pstm.executeUpdate();
            if (rs == 1) {
                maDH = getMaDHNew();
            }
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return maDH;
    }

    public int editTrangThaiSach(int idSach, int trangThai) {
        int rs = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE sach "
                + "SET trang_thai = ? "
                + "WHERE id_sach = ? ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, trangThai);
            pstm.setInt(2, idSach);

            rs = pstm.executeUpdate();

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public int addChiTietDonHang(ChiTietDonHang ctdh) {
        int rs = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "INSERT INTO chi_tiet_muon_tra "
                + "VALUES(?, ?, null, ?, ?, null, ?) ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, ctdh.getMaDH());
            pstm.setInt(2, ctdh.getIdSach());
            pstm.setDouble(3, ctdh.getTienCoc());
            pstm.setDouble(4, ctdh.getTienMuon());
            pstm.setInt(5, ctdh.getThoiGianMuon());

            rs = pstm.executeUpdate();

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        //thay doi trang thai sach thanh dang cho muon
        if (rs == 1) {
            if (editTrangThaiSach(ctdh.getIdSach(), 2) != 1) {
                rs = 0;
            }
        }

        return rs;
    }

    public int updateTrangThaiToHetHan(int maDH, int idSach) {
        int rs = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE chi_tiet_muon_tra, sach "
                + "SET tien_thanh_toan = chi_tiet_muon_tra.tien_coc, ngay_tra = CURRENT_DATE(), sach.trang_thai = 3 "
                + "WHERE ma_mtr = ? and chi_tiet_muon_tra.id_sach = ? "
                + "and chi_tiet_muon_tra.id_sach = sach.id_sach ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, maDH);
            pstm.setInt(2, idSach);

            rs = pstm.executeUpdate();

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public int updateTrangThaiToDaThanhToan(ChiTietDonHang c) {
        int rs = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE chi_tiet_muon_tra, sach "
                + "SET tien_thanh_toan = chi_tiet_muon_tra.tien_muon * thoi_gian_muon, ngay_tra = ?, sach.trang_thai = 1 "
                + "WHERE ma_mtr = ? "
                + "and chi_tiet_muon_tra.id_sach = ? "
                + "and chi_tiet_muon_tra.id_sach = sach.id_sach ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, c.getNgayTra());
            pstm.setInt(2, c.getMaDH());
            pstm.setInt(3, c.getIdSach());

            rs = pstm.executeUpdate();

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public int checkChiTietDonHang() {

        //return 1: co thay doi thanh het han
        //return 0: khong co sach nao de thay doi
        //return -1: loi
        int canThayDoi = 0, daThayDoi = 0;

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT chi_tiet_muon_tra.ma_mtr, id_sach, "
                + "adddate(ngay_muon, thoi_gian_muon) as 'ngay_het_han', CURRENT_DATE() as 'ngay_hien_tai' "
                + "FROM muon_tra, chi_tiet_muon_tra "
                + "WHERE muon_tra.ma_mtr = chi_tiet_muon_tra.ma_mtr "
                + "and ngay_tra is null ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);

            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                Date ngayHienTai16 = rs.getDate("ngay_hien_tai");
                Date ngayHetHan16 = rs.getDate("ngay_het_han");

                if (ngayHienTai16.compareTo(ngayHetHan16) > 0) {
                    canThayDoi += 1;
                    daThayDoi += updateTrangThaiToHetHan(rs.getInt("ma_mtr"), rs.getInt("id_sach"));
                }
            }

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (canThayDoi == 0 && daThayDoi == 0) {
            return 0;
        } else if (canThayDoi * 2 == daThayDoi) {
            return 1;
        } else {
            return -1;
        }
    }

    public List<String> getAllYearDonHanng() {
        List<String> years = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT year(ngay_muon) as 'nam' "
                + "FROM muon_tra "
                + "GROUP BY nam ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String year = rs.getString("nam");
                years.add(year);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return years;
    }

    public List<String> getMonthDonHangByYear(String year) {
        List<String> months = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "select month(ngay_muon) as 'thang' "
                + "FROM muon_tra "
                + "where year(ngay_muon) = ? "
                + "GROUP BY thang ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, year);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String month = rs.getString("thang");
                months.add(month);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }
        return months;
    }

    public List<DonHang> filterDonHang(String trangThaiDH, String nam, String thang) {
        List<DonHang> donHangs = new ArrayList<DonHang>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT muon_tra.* "
                + "FROM muon_tra, chi_tiet_muon_tra "
                + "WHERE chi_tiet_muon_tra.ma_mtr = muon_tra.ma_mtr ";

        //loc theo trang thai don hang
        if (trangThaiDH.equals("Đã trả hết sách")) {
            sql += "AND chi_tiet_muon_tra.ma_mtr not in (SELECT DISTINCT(ma_mtr) from chi_tiet_muon_tra where tien_thanh_toan = tien_coc or tien_thanh_toan is null) ";
        } else if (trangThaiDH.equals("Có sách đang mượn")) {
            sql += "AND chi_tiet_muon_tra.ma_mtr in (SELECT DISTINCT(ma_mtr) from chi_tiet_muon_tra where tien_thanh_toan is null) ";
        } else if (trangThaiDH.equals("Có sách hết hạn")) {
            sql += "AND chi_tiet_muon_tra.ma_mtr in (SELECT DISTINCT(ma_mtr) from chi_tiet_muon_tra where tien_thanh_toan = tien_coc) ";
        }

        //loc theo nam
        if (!nam.equals("Tất cả các năm")) {
            sql += "AND year(ngay_muon) = " + nam + " ";
        }

        //loc theo thang
        if (!thang.equals("Tất cả các tháng")) {
            sql += "AND month(ngay_muon) = " + thang + " ";
        }

        sql += "group by muon_tra.ma_mtr order by muon_tra.ma_mtr desc";

        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                DonHang donHang = new DonHang();

                donHang.setMaDH(rs.getInt("ma_mtr"));
                donHang.setSoThe(rs.getString("so_the"));
                donHang.setMaNV(rs.getInt("ma_nv"));
                donHang.setNgayMuon(rs.getString("ngay_muon"));

                donHangs.add(donHang);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return donHangs;
    }

    public List<Sach> getSachMuonNhieu() {
        List<Sach> s = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "select chi_tiet_muon_tra.id_sach, ten_sach, count(chi_tiet_muon_tra.id_sach) as 'so_luong' "
                + "from chi_tiet_muon_tra, sach "
                + "WHERE chi_tiet_muon_tra.id_sach = sach.id_sach "
                + "group by chi_tiet_muon_tra.id_sach "
                + "order by so_luong desc, id_sach";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Sach sach = new Sach();

                sach.setId(rs.getInt("id_sach"));
                sach.setTen(rs.getString("ten_sach"));
                sach.setSoLuongMuon(rs.getInt("so_luong"));

                s.add(sach);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return s;
    }

    public List<DonHang> searchDonHang(String text) {
        List<DonHang> donHangs = new ArrayList<DonHang>();

        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * "
                + "FROM muon_tra, nguoi_dung "
                + "WHERE muon_tra.ma_nv = nguoi_dung.ma_nd "
                + "and (so_the = ? or ma_mtr = ? or ten_nd like '%"+ text +"%') ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, text);
            pstm.setString(2, text);

            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                DonHang donHang = new DonHang();

                donHang.setMaDH(rs.getInt("ma_mtr"));
                donHang.setSoThe(rs.getString("so_the"));
                donHang.setMaNV(rs.getInt("ma_nv"));
                donHang.setNgayMuon(rs.getString("ngay_muon"));

                donHangs.add(donHang);

            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return donHangs;
    }
}
