package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Sach;

public class Dao_23 {

    public List<Sach> getDSSach() {
        List<Sach> s = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * FROM sach";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Sach sach = new Sach();

                sach.setId(rs.getInt("id_sach"));
                sach.setTen(rs.getString("ten_sach"));
                sach.setMaTacGia(rs.getInt("ma_tac_gia"));
                sach.setMaTheLoai(rs.getInt("ma_the_loai"));
                sach.setMaNXB(rs.getInt("ma_nxb"));
                sach.setNamXB(rs.getInt("nam_xuat_ban"));
                sach.setTienMuon(rs.getDouble("tien_muon"));
                sach.setTienCoc(rs.getDouble("tien_coc"));
                sach.setTrangThai(rs.getInt("trang_thai"));

                s.add(sach);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return s;
    }

    public String getTacGiaByMaTacGia(int mtg) {
        String tenTacGia = "";
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT ten_tac_gia FROM tac_gia WHERE ma_tac_gia = ?";
        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, mtg);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tenTacGia = rs.getString("ten_tac_gia");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tenTacGia;
    }

    public String getTtheLoaiByMaTheLoai(int mtl) {
        String tenTheLoai = "";
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT ten_the_loai FROM the_loai WHERE ma_the_loai = ?";
        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, mtl);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tenTheLoai = rs.getString("ten_the_loai");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tenTheLoai;
    }

    public String getNXBByMaNXB(int mnxb) {
        String tenNhaXuatBan = "";
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT ten_nxb FROM nha_xuat_ban WHERE ma_nxb = ?";
        PreparedStatement pstm;
        try {
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, mnxb);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            tenNhaXuatBan = rs.getString("ten_nxb");
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tenNhaXuatBan;
    }

    public List<String> getAllNXB() {
        List<String> tenNXBs = new ArrayList<String>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT ten_nxb FROM nha_xuat_ban";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String NXB = rs.getString("ten_nxb");
                tenNXBs.add(NXB);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tenNXBs;
    }

    public List<Sach> searchSach(String tenSach) {
        List<Sach> s = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "SELECT * "
                + "FROM sach "
                + "WHERE ten_sach like '%" + tenSach + "%' or id_sach = ? ";

        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setString(1, tenSach);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Sach sach = new Sach();
                
                sach.setId(rs.getInt("id_sach"));
                sach.setTen(rs.getString("ten_sach"));
                sach.setMaTacGia(rs.getInt("ma_tac_gia"));
                sach.setMaTheLoai(rs.getInt("ma_the_loai"));
                sach.setMaNXB(rs.getInt("ma_nxb"));
                sach.setNamXB(rs.getInt("nam_xuat_ban"));
                sach.setTienMuon(rs.getDouble("tien_muon"));
                sach.setTienCoc(rs.getDouble("tien_coc"));
                sach.setTrangThai(rs.getInt("trang_thai"));

                s.add(sach);

            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }

        return s;
    }

    public List<Sach> setListSach(ResultSet rs) throws SQLException {
        List<Sach> s = new ArrayList<Sach>();
        while (rs.next()) {
            Sach sach = new Sach();
            sach.setId(rs.getInt("id_sach"));
            sach.setTen(rs.getString("ten_sach"));
            sach.setMaTacGia(rs.getInt("ma_tac_gia"));
            sach.setMaTheLoai(rs.getInt("ma_the_loai"));
            sach.setMaNXB(rs.getInt("ma_nxb"));
            sach.setNamXB(rs.getInt("nam_xuat_ban"));
            sach.setTienMuon(rs.getDouble("tien_muon"));
            sach.setTrangThai(rs.getInt("trang_thai"));

            s.add(sach);
        }
        return s;
    }

    public List<Sach> getTheLoaiSach(String type) {
        List<Sach> s = new ArrayList<Sach>();

        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "select * from sach, the_loai where ten_the_loai = ?"
                + " and sach.ma_the_loai = the_loai.ma_the_loai";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, type);
            ResultSet rs = ps.executeQuery();
            s = setListSach(rs);
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return s;
    }

    public int getTacGia(String tacGia) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "select * from tac_gia where ten_tac_gia = ?";
        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, tacGia);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("ma_tac_gia");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    public int getTheLoai(String theLoai) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "select * from the_loai where ten_the_loai = ?";
        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, theLoai);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("ma_the_loai");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    public int getMaNXB(String NXB) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "select * from nha_xuat_ban where ten_nxb = ?";
        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, NXB);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("ma_nxb");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    public int getMaTrangThai(String trangThai) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "select * from trang_thai_sach where ten_tt = ?";
        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, trangThai);
            ResultSet rs = psmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("ma_tt");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    public int addBook(Sach sach) {
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "INSERT INTO sach(ten_sach, ma_tac_gia, ma_the_loai, ma_nxb, nam_xuat_ban, tien_muon, tien_coc, trang_thai) "
                + "VALUES(?,?,?,?,?,?,?,?)";

        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setString(1, sach.getTen());
            psmt.setInt(2, sach.getMaTacGia());
            psmt.setInt(3, sach.getMaTheLoai());
            psmt.setInt(4, sach.getMaNXB());
            psmt.setInt(5, sach.getNamXB());
            psmt.setDouble(6, sach.getTienMuon());
            psmt.setDouble(7, sach.getTienCoc());
            psmt.setInt(8, sach.getTrangThai());
            int rs = psmt.executeUpdate();
            con.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int updateBook(Sach sach) {
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "UPDATE sach SET ten_sach = ?, ma_tac_gia = ?, ma_the_loai = ?, ma_nxb = ?, "
                + " nam_xuat_ban = ?, tien_muon = ?, tien_coc = ?, trang_thai = ? "
                + "WHERE id_sach = ?";

        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
//            System.out.println(sach);
            psmt.setString(1, sach.getTen());
            psmt.setInt(2, sach.getMaTacGia());
            psmt.setInt(3, sach.getMaTheLoai());
            psmt.setInt(4, sach.getMaNXB());
            psmt.setInt(5, sach.getNamXB());
            psmt.setDouble(6, sach.getTienMuon());
            psmt.setDouble(7, sach.getTienCoc());
            psmt.setInt(8, sach.getTrangThai());
            psmt.setInt(9, sach.getId());
            int rs = psmt.executeUpdate();
            con.close();

            return 1;

        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int deleteBook(int id) {
        Connection con = JDBCConnection.getJDBCConnection();

        String sql = "DELETE FROM sach WHERE id_sach = ?";

        PreparedStatement psmt;
        try {
            psmt = con.prepareStatement(sql);
            psmt.setInt(1, id);
            int rs = psmt.executeUpdate();
            con.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

//    public int ranDom() {
//        List<Sach> a = getDSSach();
//        Connection con = JDBCConnection.getJDBCConnection();
//        int min = 800;
//        int max = 1500;
//        String sql = "UPDATE sach SET tien_muon=? where id_sach=?";
//        PreparedStatement psmt;
//        try {
//            psmt = con.prepareStatement(sql);
//            for (Sach s : a) {
//                int random_int = (int) (Math.random() * (max - min + 1) + min);
//                psmt.setDouble(1, random_int);
//                psmt.setInt(2, s.getId());
//                int rs = psmt.executeUpdate();
//
//            }
//            con.close();
//            return 1;
//        } catch (SQLException ex) {
//            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return 0;
//    }
//
//    public static void main(String[] args) throws SQLException {
//        Dao_23 dao = new Dao_23();
//        System.out.println(dao.ranDom());
//
//    }
}
