package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.time.LocalDate;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class Dao_14 {

    //thêm KH
    public int addKH(KhachHang kh) {
        String sql1 = "SELECT * from doc_gia where sdt=" + kh.getSdt();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " INSERT INTO doc_gia(ten_doc_gia,dia_chi,sdt)"
                + " VALUE(?,?,?) ";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql1);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return -1;
            }
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, kh.getTenDocGia());
            preparedStatement.setString(2, kh.getDiaChi());
            preparedStatement.setString(3, kh.getSdt());
            int rs1 = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //lấy tất cả Kh
    public List<KhachHang> getAllKH() {
        List<KhachHang> khachHangs = new ArrayList<KhachHang>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * FROM doc_gia";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                KhachHang kh = new KhachHang();
                kh.setMaDocGia(rs.getInt("ma_doc_gia"));
                kh.setTenDocGia(rs.getString("ten_doc_gia"));
                kh.setDiaChi(rs.getString("dia_chi"));
                kh.setSdt(rs.getString("sdt"));
                khachHangs.add(kh);

            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return khachHangs;
    }

    //xóa KH
    public int deleteKH(int maKH) {
        Connection connect = JDBCConnection.getJDBCConnection();
        PreparedStatement preparedStatement;
        String sql = "delete from doc_gia where ma_doc_gia =?";
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setInt(1, maKH);
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //sửa khách hàng
    public int editKH(KhachHang kh) {
        Connection connect = JDBCConnection.getJDBCConnection();
        PreparedStatement preparedStatement;
        String sql = " UPDATE doc_gia SET ten_doc_gia=?, dia_chi=?, sdt=? WHERE doc_gia.ma_doc_gia=?";
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, kh.getTenDocGia());
            preparedStatement.setString(2, kh.getDiaChi());
            preparedStatement.setString(3, kh.getSdt());
            preparedStatement.setInt(4, kh.getMaDocGia());
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //lấy KH theo SDT hoặc tên
    public List<KhachHang> getKhSdt(String sdt) {
        List<KhachHang> khachHangs = new ArrayList<KhachHang>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * FROM doc_gia WHERE  ten_doc_gia like '%" + sdt + "%' or sdt like '%" + sdt + "%' "
                + "order by ngay_bd desc";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                KhachHang kh = new KhachHang();
                kh.setMaDocGia(rs.getInt("ma_doc_gia"));
                kh.setTenDocGia(rs.getString("ten_doc_gia"));
                kh.setDiaChi(rs.getString("dia_chi"));
                kh.setSdt(rs.getString("sdt"));
                khachHangs.add(kh);
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return khachHangs;
    }

    //Lấy tất cả Thẻ
    public List<The> getAllThe() {
        List<The> thes = new ArrayList<The>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * "
                + "FROM the "
                + "ORDER BY ngay_bd desc";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                The the = new The();
                the.setSoThe(rs.getString("so_the"));                
                the.setMaKhachHang(rs.getInt("ma_doc_gia"));
                the.setNgayBatDau(rs.getString("ngay_bd"));
                the.setNgayKetThuc(rs.getString("ngay_kt"));
                the.setTrangThai(rs.getInt("trang_thai_the"));
                thes.add(the);
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return thes;
    }

    //Kiểm tra người này có thẻ đang hoạt động hay khÔng
    public int checkThe(int maKH) {
//        int theKhoa=0;
        int theTonTai = -1;
        List<The> thes = new ArrayList<The>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Connection connect = JDBCConnection.getJDBCConnection();

        String ht = String.valueOf(java.time.LocalDate.now());
        String sql = "select * from the where ma_doc_gia=" + maKH;
        PreparedStatement preparedStatement;
        try {
            Date hientai = formatter.parse(ht);
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                Date ngayKhoaThe = formatter.parse(rs.getString("ngay_kt"));
                if (ngayKhoaThe.compareTo(hientai) > 0) {
                    return -1;
                }
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 1;
    }

    //Thêm 1 thẻ người dùng
    public int addThe(The the) {
        if (checkThe(the.getMaKhachHang()) != 1) {
            return checkThe(the.getMaKhachHang());
        }
        int themKhongThanhCong = 0;
        int soNgayCuaThe = 90;
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " INSERT INTO the(so_the,ma_doc_gia,ngay_bd,ngay_kt,trang_thai_the)"
                + " VALUE(concat('THE-', LPAD(?, 4, '0')),?,?,?,?) ";
        String sql1 = "select max(MID(so_the, 5)) as 'max' from the ";

        PreparedStatement preparedStatement;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            preparedStatement = connect.prepareStatement(sql1);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            //Tạo mã thẻ theo số thứ tự
            int soThe = rs.getInt("max") + 1;

            LocalDate parsedDate = LocalDate.parse(the.getNgayBatDau()); //Parse date from String

            //Mỗi thẻ có tgian nhất định 
            LocalDate addedDate = parsedDate.plusDays(soNgayCuaThe);
            the.setNgayKetThuc(String.valueOf(addedDate));

            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setInt(1, soThe);
            preparedStatement.setInt(2, the.getMaKhachHang());
            preparedStatement.setString(3, the.getNgayBatDau());
            preparedStatement.setString(4, the.getNgayKetThuc());
            preparedStatement.setString(5, "1");
            int rs1 = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return themKhongThanhCong;
    }

    //Xóa 1 thẻ người dùng
    public int deleteThe(The the) {
        Connection connect = JDBCConnection.getJDBCConnection();
        PreparedStatement preparedStatement;
        String sql = "delete from the where so_the =? and ma_doc_gia=?";
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, the.getSoThe());
            preparedStatement.setInt(2, the.getMaKhachHang());
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

    //Khóa 1 thẻ người dùng
    public int khoaThe(String maThe) {
        Connection connect = JDBCConnection.getJDBCConnection();
        PreparedStatement preparedStatement;
        String sql = "UPDATE the SET trang_thai_the=0 WHERE the.so_the=?";
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, maThe);
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Mở 1 thẻ người dùng
    public int moThe(String maThe) {
        Connection connect = JDBCConnection.getJDBCConnection();
        PreparedStatement preparedStatement;
        String sql = "UPDATE the SET trang_thai_the=1 WHERE so_the=?";
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, maThe);

            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Lấy tên khách hàng bằng mã 
    public String getTenByMa(int maKH) {
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT * FROM doc_gia where ma_doc_gia=" + maKH;
        String ten = "";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            ten = rs.getString("ten_doc_gia");
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return ten;
    }

    //Tìm kiếm thẻ theo tên KH
    public List<The> getTheByTen(String ten) {
        List<The> thes = new ArrayList<The>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT the.so_the,the.ma_doc_gia,the.ngay_bd,the.ngay_kt,the.trang_thai_the "
                + " FROM the,doc_gia "
                + "WHERE the.ma_doc_gia=doc_gia.ma_doc_gia "
                + "And (doc_gia.ten_doc_gia like '%" + ten + "%' or the.so_the like '%" + ten + "%') "
                + "Order by ngay_bd desc";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                The the = new The();
                the.setSoThe(rs.getString("the.so_the"));
                the.setMaKhachHang(rs.getInt("the.ma_doc_gia"));
                the.setNgayBatDau(rs.getString("the.ngay_bd"));
                the.setNgayKetThuc(rs.getString("the.ngay_kt"));
                the.setTrangThai(rs.getInt("the.trang_thai_the"));
                thes.add(the);
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return thes;
    }

    //thêm 1 tác giả
    public int addTacGia(String tenTacGia) {
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " INSERT INTO tac_gia(ten_tac_gia)"
                + " VALUE(?) ";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, tenTacGia);
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Lấy tất cả tác giả
    public List<TacGia> getAllTacGia() {
        List<TacGia> tacGiaS = new ArrayList<TacGia>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "select * from tac_gia";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TacGia tg = new TacGia();
                tg.setMaTacGia(rs.getInt("ma_tac_gia"));
                tg.setTenTacGia(rs.getString("ten_tac_gia"));
                tacGiaS.add(tg);

            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tacGiaS;
    }

    //Sửa tên tác giả
    public int editTacGia(TacGia tg) {
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " UPDATE tac_gia SET ten_tac_gia=? WHERE ma_tac_gia=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, tg.getTenTacGia());
            preparedStatement.setInt(2, tg.getMaTacGia());
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Tìm kiếm tác giả
    public List<TacGia> timKiemTacGia(String tk) {
        List<TacGia> tacGiaS = new ArrayList<TacGia>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "select * from tac_gia "
                + "   where ten_tac_gia like '%" + tk + "%'";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TacGia tg = new TacGia();
                tg.setMaTacGia(rs.getInt("ma_tac_gia"));
                tg.setTenTacGia(rs.getString("ten_tac_gia"));
                tacGiaS.add(tg);

            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tacGiaS;
    }

    //Lấy tất cả thể loại
    public List<TheLoai> getAllTheLoai() {
        List<TheLoai> theLoaiS = new ArrayList<TheLoai>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "select * from the_loai ";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TheLoai tl = new TheLoai();
                tl.setMaTheLoai(rs.getInt("ma_the_loai"));
                tl.setTenTheLoai(rs.getString("ten_the_loai"));
                theLoaiS.add(tl);

            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return theLoaiS;
    }

    //Tìm kiếm thể loại
    public List<TheLoai> timKiemTheLoai(String tk) {
        List<TheLoai> theLoaiS = new ArrayList<TheLoai>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "select * from the_loai where ten_the_loai like '%" + tk + "%'";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TheLoai tl = new TheLoai();
                tl.setMaTheLoai(rs.getInt("ma_the_loai"));
                tl.setTenTheLoai(rs.getString("ten_the_loai"));
                theLoaiS.add(tl);

            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }

        return theLoaiS;
    }

    //Thêm 1 thể loại
    public int addTheLoai(String tenTheLoai) {
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " INSERT INTO the_loai(ten_the_loai)"
                + " VALUE(?) ";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, tenTheLoai);
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Sửa 1 thể loại
    public int editTheLoai(TheLoai tl) {
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = " UPDATE the_loai SET ten_the_loai=? WHERE ma_the_loai=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, tl.getTenTheLoai());
            preparedStatement.setInt(2, tl.getMaTheLoai());
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int setTrangThaiHetHan() {

        Connection connect = JDBCConnection.getJDBCConnection();
        String ngayHienTai = String.valueOf(java.time.LocalDate.now());
        String sql = "UPDATE the"
                + " SET trang_thai_the=-1"
                + " WHERE trang_thai_the=1 and ngay_kt<?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            preparedStatement.setString(1, ngayHienTai);
            int rs = preparedStatement.executeUpdate();
            connect.close();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    //Lấy doanh thu theo từng tháng
    public List<DoanhThu> doanhThu() {
        List<DoanhThu> dThu = new ArrayList<DoanhThu>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT sum(tien_thanh_toan) as tien,concat(MONTH(ngay_tra),\"/\",YEAR(ngay_tra)) as thang "
                + "FROM chi_tiet_muon_tra "
                + " WHERE ngay_tra is NOT null "
                + "GROUP BY YEAR(ngay_tra), MONTH(ngay_tra)";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DoanhThu dt = new DoanhThu();
                dt.setThang(rs.getString("thang"));
                dt.setDoanhThu(rs.getDouble("tien"));
                dThu.add(dt);
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dThu;
    }

    public int demSach() {
        int dem = 0;
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT COUNT(*) as soLuong "
                + " From sach";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            dem = rs.getInt("soLuong");
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dem;
    }

    public List<BieuDoTron> BieuDotron() {

        List<BieuDoTron> bdt = new ArrayList<BieuDoTron>();
        Connection connect = JDBCConnection.getJDBCConnection();
        String sql = "SELECT COUNT(trang_thai) as trangThai ,trang_thai_sach.ten_tt "
                + "FROM sach,trang_thai_sach "
                + "WHERE sach.trang_thai=trang_thai_sach.ma_tt GROUP BY trang_thai";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connect.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BieuDoTron bd = new BieuDoTron();
                bd.setSoLuong(rs.getDouble("trangThai"));
//               Math.round(rs.getDouble("trangThai")/soLuongSach*100.0)
                bd.setTen(rs.getString("ten_tt"));
                bdt.add(bd);
            }
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao_14.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bdt;
    }
//    public int ranDom() {
//        List<The> thes = getAllThe();
//
//        Connection con = JDBCConnection.getJDBCConnection();
//
//        String sql = "UPDATE the SET ngay_kt = ? where so_the = ?";
//
//        try {
//            PreparedStatement psmt = con.prepareStatement(sql);
//            for (The the : thes) {
//                LocalDate ngayBD = LocalDate.parse(the.getNgayBatDau());
//                LocalDate ngayKT = ngayBD.plusDays(90);
//                
//                psmt.setString(1, String.valueOf(ngayKT));
//                psmt.setString(2, the.getSoThe());
//                int rs = psmt.executeUpdate();
//
//            }
//            con.close();
//            return 1;
//        } catch (SQLException ex) {
//            Logger.getLogger(Dao_23.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return 0;
//    }
//    
//    public static void main(String[] args) {
//        Dao_14 tus = new Dao_14();
//        System.out.println(tus.ranDom());
//    }
    public double getChiTietDonHang(int maDH) {
        double tong=0;
        Connection con = JDBCConnection.getJDBCConnection();
        String sql = "SELECT sum(tien_coc) as tong_tien "
                + "FROM chi_tiet_muon_tra "
                + "WHERE ma_mtr = ?";
        try {
            PreparedStatement pstm = con.prepareStatement(sql);
            pstm.setInt(1, maDH);
            ResultSet rs = pstm.executeQuery();
           rs.next();
           tong=rs.getDouble("tong_tien");
        } catch (SQLException ex) {
            Logger.getLogger(Dao_16.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tong;
    }

    public void inHoaDon(int maDH) {
        Hashtable map = new Hashtable();
        map.put("maDH", maDH);
        map.put("tong_tien_coc", getChiTietDonHang(maDH));
        String link = "src\\view\\muonSach\\HoaDon.jrxml";
        Connection connect = JDBCConnection.getJDBCConnection();
        try {
            JasperReport jr = JasperCompileManager.compileReport(link);
            JasperPrint jp = JasperFillManager.fillReport(jr, map, connect);
//            JasperViewer.viewReport(jp);
            JasperViewer.viewReport(jp, false);
           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        Dao_14 a = new Dao_14();
        System.out.println(a.getTheByTen("1"));
//        The the = new The();
//        the.setMaKhachHang(3);
//        the.setNgayBatDau("2022-05-24");
//        System.out.println(a.addThe(the));;
//        the.setMaKhachHang(4);
//        the.setSoThe("btdg.1");
//        the.setNgayBatDau("2022-05-31");
//        System.out.println(a.moThe(the));
//
//        KhachHang kh = new KhachHang();
//        kh.setSdt("012345678988");
//        kh.setTenDocGia("tttt");
//        kh.setDiaChi("ccc");
//        System.out.println(a.addKH(kh));
//        System.out.println(a.setTrangThaiHetHan());
//        String ht = String.valueOf(java.time.LocalDate.now());
//        System.out.println(ht);
//        System.out.println(a.doanhThu());
    }
}
