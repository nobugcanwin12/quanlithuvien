-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 01, 2022 lúc 06:14 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanlithuvien`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chi_tiet_muon_tra`
--

CREATE TABLE `chi_tiet_muon_tra` (
  `ma_mtr` int(11) NOT NULL,
  `id_sach` int(11) NOT NULL,
  `ngay_tra` date DEFAULT NULL,
  `tien_coc` double NOT NULL,
  `tien_muon` double NOT NULL,
  `tien_thanh_toan` double DEFAULT NULL,
  `thoi_gian_muon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chi_tiet_muon_tra`
--

INSERT INTO `chi_tiet_muon_tra` (`ma_mtr`, `id_sach`, `ngay_tra`, `tien_coc`, `tien_muon`, `tien_thanh_toan`, `thoi_gian_muon`) VALUES
(1, 3, '2022-01-05', 68000, 1496, 10472, 7),
(1, 13, '2022-01-19', 56000, 1324, 27804, 21),
(1, 26, '2022-01-21', 79000, 1496, 31416, 21),
(2, 48, '2022-01-15', 93123, 1099, 15386, 14),
(3, 21, '2022-01-20', 56000, 1421, 19894, 14),
(3, 27, '2022-01-13', 63000, 1185, 8295, 7),
(4, 14, '2022-01-28', 75000, 886, 18606, 21),
(5, 8, '2022-02-05', 57000, 1224, 57000, 21),
(5, 28, '2022-01-17', 80000, 972, 6804, 7),
(6, 23, '2022-02-06', 59000, 1480, 31080, 21),
(7, 1, '2022-01-30', 56000, 859, 12026, 14),
(7, 16, '2022-02-06', 66000, 818, 17178, 21),
(7, 40, '2022-01-24', 72310, 1365, 9555, 7),
(8, 6, '2022-02-13', 77000, 1171, 77000, 21),
(8, 25, '2022-02-10', 89000, 1044, 21924, 21),
(9, 4, '2022-02-06', 92000, 1339, 18746, 14),
(9, 7, '2022-02-12', 55000, 1157, 24297, 21),
(9, 34, '2022-01-31', 83000, 1093, 7651, 7),
(10, 39, '2022-02-15', 77000, 1360, 28560, 21),
(11, 18, '2022-02-12', 65000, 1462, 20468, 14),
(11, 30, '2022-02-23', 94000, 804, 16884, 21),
(12, 5, '2022-02-20', 86000, 906, 12684, 14),
(12, 34, '2022-02-13', 83000, 1093, 7651, 7),
(12, 46, '2022-02-20', 67310, 961, 13454, 14),
(13, 4, '2022-03-08', 92000, 1339, 28119, 21),
(13, 13, '2022-02-19', 56000, 1324, 9268, 7),
(13, 25, '2022-02-27', 89000, 1044, 14616, 14),
(14, 26, '2022-03-10', 79000, 1496, 20944, 14),
(14, 28, '2022-02-27', 80000, 972, 6804, 7),
(15, 3, '2022-03-13', 68000, 1496, 20944, 14),
(15, 7, '2022-03-18', 55000, 1157, 24297, 21),
(15, 32, '2022-03-12', 82000, 1023, 14322, 14),
(16, 2, '2022-03-10', 79000, 1012, 79000, 7),
(17, 15, '2022-03-30', 59000, 954, 59000, 21),
(17, 31, '2022-03-13', 90000, 1221, 8547, 7),
(17, 42, '2022-03-20', 75312, 1425, 19950, 14),
(18, 33, '2022-03-26', 61000, 1201, 16814, 14),
(19, 43, '2022-03-25', 98000, 980, 6860, 7),
(19, 44, '2022-04-05', 76301, 1323, 18522, 14),
(20, 4, '2022-04-06', 92000, 1339, 18746, 14),
(21, 22, '2022-04-10', 78000, 1496, 10472, 7),
(21, 39, '2022-04-23', 77000, 1360, 28560, 21),
(21, 40, '2022-04-23', 72310, 1365, 28665, 21),
(22, 25, '2022-04-26', 89000, 1044, 21924, 21),
(22, 31, '2022-04-21', 90000, 1221, 17094, 14),
(23, 20, '2022-05-01', 67000, 946, 13244, 14),
(23, 29, '2022-05-02', 55000, 1397, 19558, 14),
(23, 42, '2022-05-08', 75312, 1425, 29925, 21),
(24, 14, '2022-05-07', 75000, 886, 18606, 21),
(24, 26, '2022-05-08', 79000, 1496, 31416, 21),
(24, 27, '2022-05-01', 63000, 1185, 16590, 14),
(25, 45, '2022-05-15', 76010, 1296, 27216, 21),
(26, 18, '2022-05-14', 65000, 1462, 20468, 14),
(26, 30, '2022-05-03', 94000, 804, 5628, 7),
(27, 3, '2022-05-20', 68000, 1496, 20944, 14),
(28, 7, '2022-05-22', 55000, 1157, 8099, 7),
(28, 9, '2022-05-29', 55000, 861, 55000, 7),
(28, 36, NULL, 97000, 1335, NULL, 21),
(29, 43, NULL, 98000, 980, NULL, 21),
(30, 17, NULL, 60000, 1093, NULL, 7),
(30, 35, NULL, 99000, 1192, NULL, 7),
(31, 1, NULL, 56000, 859, NULL, 30);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuc_vu`
--

CREATE TABLE `chuc_vu` (
  `ma_chuc_vu` int(11) NOT NULL,
  `ten_chuc_vu` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chuc_vu`
--

INSERT INTO `chuc_vu` (`ma_chuc_vu`, `ten_chuc_vu`) VALUES
(1, 'Nhân viên'),
(2, 'Chủ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doc_gia`
--

CREATE TABLE `doc_gia` (
  `ma_doc_gia` int(11) NOT NULL,
  `ten_doc_gia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `doc_gia`
--

INSERT INTO `doc_gia` (`ma_doc_gia`, `ten_doc_gia`, `dia_chi`, `sdt`) VALUES
(1, 'Nguyễn Văn 1', 'Nhà', '0900448265'),
(2, 'Nguyễn Văn 2', 'Nhà', '0908714062'),
(3, 'Nguyễn Văn 3', 'Nhà', '0901267111'),
(4, 'Nguyễn Văn 4', 'Nhà', '0906663510'),
(5, 'Nguyễn Văn 5', 'Nhà', '0904337108'),
(6, 'Nguyễn Văn 6', 'Nhà', '0906571665'),
(7, 'Nguyễn Văn 7', 'Nhà', '0908727356'),
(8, 'Nguyễn Văn 8', 'Nhà', '0907044112'),
(9, 'Nguyễn Văn 9', 'Nhà', '0908020641'),
(10, 'Nguyễn Văn 10', 'Nhà', '0901550420');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `muon_tra`
--

CREATE TABLE `muon_tra` (
  `ma_mtr` int(11) NOT NULL,
  `so_the` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ma_nv` int(11) NOT NULL,
  `ngay_muon` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `muon_tra`
--

INSERT INTO `muon_tra` (`ma_mtr`, `so_the`, `ma_nv`, `ngay_muon`) VALUES
(1, 'THE-0001', 3, '2022-01-01'),
(2, 'THE-0002', 6, '2022-01-06'),
(3, 'THE-0003', 6, '2022-01-07'),
(4, 'THE-0004', 2, '2022-01-10'),
(5, 'THE-0005', 3, '2022-01-14'),
(6, 'THE-0006', 3, '2022-01-18'),
(7, 'THE-0007', 4, '2022-01-19'),
(8, 'THE-0008', 5, '2022-01-22'),
(9, 'THE-0009', 2, '2022-01-25'),
(10, 'THE-0010', 4, '2022-01-29'),
(11, 'THE-0003', 5, '2022-02-03'),
(12, 'THE-0002', 2, '2022-02-11'),
(13, 'THE-0004', 6, '2022-02-16'),
(14, 'THE-0001', 4, '2022-02-25'),
(15, 'THE-0005', 4, '2022-02-28'),
(16, 'THE-0009', 4, '2022-03-02'),
(17, 'THE-0010', 2, '2022-03-08'),
(18, 'THE-0007', 2, '2022-03-14'),
(19, 'THE-0008', 2, '2022-03-23'),
(20, 'THE-0006', 3, '2022-03-27'),
(21, 'THE-0004', 3, '2022-04-04'),
(22, 'THE-0005', 4, '2022-04-09'),
(23, 'THE-0009', 4, '2022-04-20'),
(24, 'THE-0008', 2, '2022-04-20'),
(25, 'THE-0010', 5, '2022-04-27'),
(26, 'THE-0014', 6, '2022-05-01'),
(27, 'THE-0013', 3, '2022-05-10'),
(28, 'THE-0015', 5, '2022-05-18'),
(29, 'THE-0012', 5, '2022-05-20'),
(30, 'THE-0011', 6, '2022-05-29'),
(31, 'THE-0016', 6, '2022-05-29'),
(34, 'THE-0013', 1, '2022-05-31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoi_dung`
--

CREATE TABLE `nguoi_dung` (
  `ma_nd` int(11) NOT NULL,
  `ten_nd` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ma_chuc_vu` int(11) NOT NULL,
  `tai_khoan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` date DEFAULT NULL,
  `sdt` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trang_thai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`ma_nd`, `ten_nd`, `ma_chuc_vu`, `tai_khoan`, `mat_khau`, `gioi_tinh`, `ngay_sinh`, `sdt`, `trang_thai`) VALUES
(1, 'Nguyễn Quản Trị', 2, 'admin', 'admin', 'Nam', '1996-03-13', '0902373133', 1),
(2, 'Nguyễn Nhân Viên', 1, 'nhanvien1', 'admin', 'Nam', '1997-02-09', '0906547747', 1),
(3, 'Đặng Nhân Viên', 1, 'nhanvien2', 'admin', 'Nam', '1998-01-13', '0906288121', 1),
(4, 'Lưu Nhân Viên', 1, 'nhanvien3', 'admin', 'Nam', '1998-07-01', '0907627516', 1),
(5, 'Bùi Nhân Viên', 1, 'nhanvien4', 'admin', 'Nam', '1995-03-09', '0905442381', 1),
(6, 'Đinh Thị Nhân Viên', 1, 'nhanvien5', 'admin', 'Nữ', '1998-08-22', '0905642481', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nha_xuat_ban`
--

CREATE TABLE `nha_xuat_ban` (
  `ma_nxb` int(11) NOT NULL,
  `ten_nxb` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nha_xuat_ban`
--

INSERT INTO `nha_xuat_ban` (`ma_nxb`, `ten_nxb`) VALUES
(1, 'Kim Đồng'),
(2, 'Tuổi trẻ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sach`
--

CREATE TABLE `sach` (
  `id_sach` int(11) NOT NULL,
  `ten_sach` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ma_tac_gia` int(11) NOT NULL,
  `ma_the_loai` int(11) NOT NULL,
  `ma_nxb` int(11) NOT NULL,
  `nam_xuat_ban` int(5) NOT NULL,
  `tien_muon` double NOT NULL,
  `tien_coc` double NOT NULL,
  `trang_thai` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sach`
--

INSERT INTO `sach` (`id_sach`, `ten_sach`, `ma_tac_gia`, `ma_the_loai`, `ma_nxb`, `nam_xuat_ban`, `tien_muon`, `tien_coc`, `trang_thai`) VALUES
(1, 'Tôi Đã Kiếm Được 2.000.000 Đô-La Từ Thị Trường Chứng Khoán Như Thế Nào?', 20, 4, 1, 2000, 859, 56000, 2),
(2, 'Bách Thủ Thư Sinh', 12, 1, 1, 2008, 1012, 79000, 3),
(3, 'Phi Đao Túy Nguyệt', 2, 1, 2, 2005, 1496, 68000, 1),
(4, 'Machine Learning Cơ Bản', 3, 2, 2, 2003, 1339, 92000, 1),
(5, 'Laravel 5 Cookbook Enhance Your Amazing Applications', 4, 2, 2, 2007, 906, 86000, 1),
(6, 'Pro ASP.NET MVC 5', 5, 2, 1, 2002, 1171, 77000, 3),
(7, 'Linux All-In-One For Dummies – 5Th Edition', 6, 2, 1, 2008, 1157, 55000, 1),
(8, 'Cách Học Ngoại Ngữ Nhanh Và Không Bao Giờ Quên', 10, 3, 2, 2005, 1224, 57000, 3),
(9, 'Phàm Nhân Tu Tiên Chi Tiên Giới Thiên (Phàm Nhân Tu Tiên 2)', 13, 1, 2, 2003, 861, 55000, 3),
(10, 'Hồ Nữ', 14, 1, 1, 2002, 934, 82000, 1),
(11, 'Văn Thuyết', 15, 1, 2, 2000, 1036, 59000, 1),
(12, 'Đấu La Đại Lục', 16, 1, 2, 2003, 917, 54000, 1),
(13, 'Âm Dương Miện', 16, 1, 1, 2006, 1324, 56000, 1),
(14, 'Để xây dựng doanh nghiệp hiệu quả', 17, 4, 1, 2007, 886, 75000, 1),
(15, '1001 Cách Giữ Chân Khách Hàng', 18, 4, 1, 2005, 954, 59000, 3),
(16, 'Cẩm Nang Sale Bất Động Sản Alibaba – Nguyễn Thái Luyện', 19, 4, 2, 2007, 818, 66000, 1),
(17, 'Giáo Trình Thiết Kế Mạng', 21, 2, 1, 2005, 1093, 60000, 2),
(18, 'Tuổi Trẻ Hoàng Văn Thụ', 22, 5, 2, 1999, 1462, 65000, 1),
(19, 'Điện Biên Phủ - Điểm hẹn lịch sử', 23, 5, 1, 1998, 803, 70000, 1),
(20, 'Đường tới Điện Biên Phủ', 23, 5, 2, 1997, 946, 67000, 1),
(21, 'Lịch Sử Hoa Kỳ', 24, 5, 1, 1990, 1421, 56000, 1),
(22, 'Cách Mạng Pháp', 24, 5, 2, 2000, 1496, 78000, 1),
(23, 'Đêm trường Trung Cổ', 24, 5, 1, 1991, 1480, 59000, 1),
(24, 'Từ Câu Sai Đến Câu Hay', 25, 6, 1, 2009, 1333, 90000, 1),
(25, 'Thuật Quản Lý Thời Gian', 26, 6, 2, 2005, 1044, 89000, 1),
(26, 'Tư Duy Tích Cực Đánh Thức Tiềm Năng', 26, 6, 1, 2010, 1496, 79000, 1),
(27, 'Tư Duy Vượt Giới Hạn', 27, 6, 2, 2003, 1185, 63000, 1),
(28, 'Vietnamese Tiếng Việt Không Son Phấn', 28, 3, 1, 2007, 972, 80000, 1),
(29, 'Một Số Di Tích Tiêu Biểu Ở Việt Nam', 1, 7, 1, 1998, 1397, 55000, 1),
(30, 'Hỏi Và Đáp Về 54 Dân Tộc Việt Nam', 1, 7, 2, 2000, 804, 94000, 1),
(31, 'Những Con Đường Tơ Lụa - Một Lịch Sử Mới Về Thế Giới', 29, 7, 1, 2015, 1221, 90000, 1),
(32, 'Non nước Xứ Quảng', 30, 7, 2, 2004, 1023, 82000, 1),
(33, 'Chủ quyền Việt Nam trên biển Đông và Hoàng Sa - Trường Sa', 31, 7, 2, 1996, 1201, 61000, 1),
(34, 'Khảo cổ học ở Thành phố Hồ Chí Minh Khảo cổ học ở Thành phố Hồ Chí Minh', 32, 7, 2, 2006, 1093, 83000, 1),
(35, 'Programming Bitcoin - Learn How to Program Bitcoin from Scratch', 33, 2, 1, 2019, 1192, 99000, 2),
(36, 'Software Engineering at Google', 34, 2, 2, 2020, 1335, 97000, 2),
(37, 'Du Hành Trong Thế Giới Sáng Tạo', 35, 8, 1, 2009, 976, 80000, 1),
(38, 'Thuật Marketing', 26, 8, 2, 2010, 1021, 89000, 1),
(39, 'Thuật Quản Lý Bán Hàng', 26, 8, 1, 2014, 1360, 77000, 1),
(40, 'Đột Phá Marketing Đột Phá', 1, 8, 2, 2005, 1365, 72310, 1),
(41, 'Từ Bản Năng Đến Nghệ Thuật Bán Hàng', 36, 8, 1, 2013, 1020, 65421, 1),
(42, 'Khách Hàng Là Số 1', 37, 8, 2, 2001, 1425, 75312, 1),
(43, 'Tesla, SpaceX Và Sứ Mệnh Tìm Kiếm Một Tương Lai Ngoài Sức Tưởng Tượng', 38, 9, 1, 2021, 980, 98000, 2),
(44, 'Chìa Khóa Vũ Trụ Của George', 39, 9, 2, 2018, 1323, 76301, 1),
(45, 'Phiêu Bước Cùng Einstein', 40, 9, 2, 2003, 1296, 76010, 1),
(46, 'Israel – Mảnh Đất Của Những Phát Minh Vì Con Người', 41, 9, 2, 2009, 961, 67310, 1),
(47, 'Nhân chủng học - Khoa học về con người', 42, 9, 2, 2010, 1457, 78200, 1),
(48, 'Huyền Thoại Và Lịch Sử Các Khoa Học Nhân Văn', 43, 9, 1, 2008, 1099, 93123, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tac_gia`
--

CREATE TABLE `tac_gia` (
  `ma_tac_gia` int(11) NOT NULL,
  `ten_tac_gia` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tac_gia`
--

INSERT INTO `tac_gia` (`ma_tac_gia`, `ten_tac_gia`) VALUES
(1, 'Nhiều tác giả'),
(2, 'Ức Vân'),
(3, 'Vũ Hữu Tiệp'),
(4, 'Nathan Wu'),
(5, 'Adam Freeman'),
(6, 'Emmett Dulaney'),
(7, 'Nguyễn Thị Hà Bắc'),
(8, 'Jung min kyung'),
(9, 'Lê Huy Khoa'),
(10, 'Gabriel Wyner'),
(11, 'Đặng Trung Ngọc'),
(12, 'Ngọa Long Sinh'),
(13, 'Vong Ngữ'),
(14, 'Dịch Ngũ'),
(15, 'Hạnh Dao Vị Vãn'),
(16, 'Đường Gia Tam Thiếu'),
(17, 'Michael E. Gerber'),
(18, 'Nhất Ly'),
(19, 'Nguyễn Thái Luyện'),
(20, 'Nicolas Darvas'),
(21, 'Nguyễn Gia Như'),
(22, 'Tô Hoài'),
(23, 'Hữu Mai'),
(24, 'Zhang Wu Shun'),
(25, 'Nguyễn Đức Dân'),
(26, 'Brian Tracy'),
(27, 'Andy Andrews'),
(28, 'Nguyễn Đình Hòa'),
(29, 'Peter Frankopan'),
(30, 'Phạm Trung Việt'),
(31, 'Nguyễn Đình Dầu'),
(32, 'Lê Xuân Diệm'),
(33, 'Jimmy Song'),
(34, 'Titus Winters'),
(35, 'Michael de Kretser'),
(36, 'Daniel H. Pink'),
(37, 'Ken Blanchard'),
(38, 'Ashlee Vance'),
(39, 'Lucy – Stephen Hawking'),
(40, 'Joshua Foer'),
(41, 'Avi Jorisch'),
(42, 'E. Adamson Hoebel'),
(43, 'Laurent Mucchielli');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `the`
--

CREATE TABLE `the` (
  `so_the` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ma_doc_gia` int(11) NOT NULL,
  `ngay_bd` date NOT NULL DEFAULT current_timestamp(),
  `ngay_kt` date NOT NULL,
  `trang_thai_the` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `the`
--

INSERT INTO `the` (`so_the`, `ma_doc_gia`, `ngay_bd`, `ngay_kt`, `trang_thai_the`) VALUES
('THE-0001', 1, '2022-01-01', '2022-04-01', -1),
('THE-0002', 2, '2022-01-06', '2022-04-06', -1),
('THE-0003', 3, '2022-01-07', '2022-04-07', -1),
('THE-0004', 4, '2022-01-10', '2022-04-10', -1),
('THE-0005', 5, '2022-01-14', '2022-04-14', -1),
('THE-0006', 6, '2022-01-18', '2022-04-18', -1),
('THE-0007', 7, '2022-01-19', '2022-04-19', -1),
('THE-0008', 8, '2022-01-22', '2022-04-22', -1),
('THE-0009', 9, '2022-01-25', '2022-04-25', -1),
('THE-0010', 10, '2022-01-29', '2022-04-29', -1),
('THE-0011', 8, '2022-05-02', '2022-07-31', 1),
('THE-0012', 5, '2022-05-05', '2022-08-03', 1),
('THE-0013', 3, '2022-05-11', '2022-08-09', 1),
('THE-0014', 7, '2022-05-11', '2022-08-09', 1),
('THE-0015', 10, '2022-05-16', '2022-08-14', 1),
('THE-0016', 6, '2022-05-29', '2022-08-27', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `the_loai`
--

CREATE TABLE `the_loai` (
  `ma_the_loai` int(11) NOT NULL,
  `ten_the_loai` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `the_loai`
--

INSERT INTO `the_loai` (`ma_the_loai`, `ten_the_loai`) VALUES
(1, 'Kiếm hiệp - Tiên hiệp'),
(2, 'Công nghệ thông tin'),
(3, 'Học Ngoại Ngữ'),
(4, 'Kinh Tế - Quản Lý'),
(5, 'Lịch Sử'),
(6, 'Kỹ Năng Học Tập - Làm Việc'),
(7, 'Địa Lý'),
(8, 'Marketing – Bán Hàng'),
(9, 'Khoa Học – Kỹ Thuật');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tien`
--

CREATE TABLE `tien` (
  `ma_tien_muon` int(11) NOT NULL,
  `ten_tien_muon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `so_tien` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tien`
--

INSERT INTO `tien` (`ma_tien_muon`, `ten_tien_muon`, `so_tien`) VALUES
(1, 'Tiền tạo thẻ', 50000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trang_thai_sach`
--

CREATE TABLE `trang_thai_sach` (
  `ma_tt` int(2) NOT NULL,
  `ten_tt` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `trang_thai_sach`
--

INSERT INTO `trang_thai_sach` (`ma_tt`, `ten_tt`) VALUES
(1, 'Đang rảnh'),
(2, 'Đang cho mượn'),
(3, 'Đang bảo trì');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `chi_tiet_muon_tra`
--
ALTER TABLE `chi_tiet_muon_tra`
  ADD PRIMARY KEY (`ma_mtr`,`id_sach`),
  ADD KEY `ma_mtr` (`ma_mtr`),
  ADD KEY `id_sach` (`id_sach`);

--
-- Chỉ mục cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  ADD PRIMARY KEY (`ma_chuc_vu`);

--
-- Chỉ mục cho bảng `doc_gia`
--
ALTER TABLE `doc_gia`
  ADD PRIMARY KEY (`ma_doc_gia`),
  ADD UNIQUE KEY `sdt` (`sdt`);

--
-- Chỉ mục cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  ADD PRIMARY KEY (`ma_mtr`),
  ADD KEY `so_the` (`so_the`),
  ADD KEY `ma_nv` (`ma_nv`);

--
-- Chỉ mục cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`ma_nd`),
  ADD KEY `ma_chuc_vu` (`ma_chuc_vu`);

--
-- Chỉ mục cho bảng `nha_xuat_ban`
--
ALTER TABLE `nha_xuat_ban`
  ADD PRIMARY KEY (`ma_nxb`);

--
-- Chỉ mục cho bảng `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`id_sach`),
  ADD KEY `ma_tac_gia` (`ma_tac_gia`),
  ADD KEY `ma_the_loai` (`ma_the_loai`),
  ADD KEY `ma_nxb` (`ma_nxb`),
  ADD KEY `trang_thai` (`trang_thai`);

--
-- Chỉ mục cho bảng `tac_gia`
--
ALTER TABLE `tac_gia`
  ADD PRIMARY KEY (`ma_tac_gia`);

--
-- Chỉ mục cho bảng `the`
--
ALTER TABLE `the`
  ADD PRIMARY KEY (`so_the`),
  ADD KEY `ma_doc_gia` (`ma_doc_gia`);

--
-- Chỉ mục cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  ADD PRIMARY KEY (`ma_the_loai`);

--
-- Chỉ mục cho bảng `tien`
--
ALTER TABLE `tien`
  ADD PRIMARY KEY (`ma_tien_muon`);

--
-- Chỉ mục cho bảng `trang_thai_sach`
--
ALTER TABLE `trang_thai_sach`
  ADD PRIMARY KEY (`ma_tt`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  MODIFY `ma_chuc_vu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `doc_gia`
--
ALTER TABLE `doc_gia`
  MODIFY `ma_doc_gia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  MODIFY `ma_mtr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  MODIFY `ma_nd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `nha_xuat_ban`
--
ALTER TABLE `nha_xuat_ban`
  MODIFY `ma_nxb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `sach`
--
ALTER TABLE `sach`
  MODIFY `id_sach` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT cho bảng `tac_gia`
--
ALTER TABLE `tac_gia`
  MODIFY `ma_tac_gia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT cho bảng `the_loai`
--
ALTER TABLE `the_loai`
  MODIFY `ma_the_loai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tien`
--
ALTER TABLE `tien`
  MODIFY `ma_tien_muon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `trang_thai_sach`
--
ALTER TABLE `trang_thai_sach`
  MODIFY `ma_tt` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `chi_tiet_muon_tra`
--
ALTER TABLE `chi_tiet_muon_tra`
  ADD CONSTRAINT `chi_tiet_muon_tra_ibfk_1` FOREIGN KEY (`ma_mtr`) REFERENCES `muon_tra` (`ma_mtr`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chi_tiet_muon_tra_ibfk_2` FOREIGN KEY (`id_sach`) REFERENCES `sach` (`id_sach`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `muon_tra`
--
ALTER TABLE `muon_tra`
  ADD CONSTRAINT `muon_tra_ibfk_1` FOREIGN KEY (`so_the`) REFERENCES `the` (`so_the`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `muon_tra_ibfk_2` FOREIGN KEY (`ma_nv`) REFERENCES `nguoi_dung` (`ma_nd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD CONSTRAINT `nguoi_dung_ibfk_1` FOREIGN KEY (`ma_chuc_vu`) REFERENCES `chuc_vu` (`ma_chuc_vu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `sach`
--
ALTER TABLE `sach`
  ADD CONSTRAINT `sach_ibfk_1` FOREIGN KEY (`ma_the_loai`) REFERENCES `the_loai` (`ma_the_loai`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_2` FOREIGN KEY (`ma_tac_gia`) REFERENCES `tac_gia` (`ma_tac_gia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_3` FOREIGN KEY (`ma_nxb`) REFERENCES `nha_xuat_ban` (`ma_nxb`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sach_ibfk_5` FOREIGN KEY (`trang_thai`) REFERENCES `trang_thai_sach` (`ma_tt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `the`
--
ALTER TABLE `the`
  ADD CONSTRAINT `the_ibfk_1` FOREIGN KEY (`ma_doc_gia`) REFERENCES `doc_gia` (`ma_doc_gia`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
